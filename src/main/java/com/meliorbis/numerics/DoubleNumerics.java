package com.meliorbis.numerics;

/**
 * Double-specific numerics implementation
 * 
 * This is a somewhat smelly hack between dependency injection and singleton 
 * pattern which is mainly due to the question 'WTF, dude?' when it comes to
 * how arrays are created etc. Refactor!
 */
public class DoubleNumerics extends Numerics<Double>
{
	private static DoubleNumerics _instance;
	
	/**
	 * The constructor should only be call by an IOC container, if at all
	 */
    private DoubleNumerics()
    {
        super(Double.class);
        
        synchronized(DoubleNumerics.class)
        {
            if(_instance != null)
            {
            	throw new RuntimeException("Multiple instances of DoubleNumerics have been created");
            }
        }
    }
    
    /**
     * @return The singleton instance
     */
    public static DoubleNumerics instance()
    {
    	synchronized(DoubleNumerics.class)
    	{
    		if(_instance == null)
    		{
    			_instance = new DoubleNumerics();
    		}
    	}
    	
    	return _instance;
    }
}

