package com.meliorbis.numerics;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.meliorbis.numerics.generic.IntegerArray;

public final class IntArrayFactories
{
	// Prevent construction
	private IntArrayFactories(){}

	private static final DoubleNumerics _numerics = DoubleNumerics.instance();
	
	/**
	 * Creates an integer array with the provided dimensions
	 * 
	 * @param size_ The dimensions of the array to be created
	 * 
	 * @return An array with the give size and all values at the default
	 */
	static public IntegerArray<?> createIntArrayOfSize(List<Integer> size_)
	{
		return _numerics.newIntArray(size_);
	}

	/**
	 * Creates an integer array with the provided dimensions
	 * 
	 * @param size_ The dimensions of the array to be created
	 * 
	 * @return An array with the give size and all values at the default
	 */
	static public IntegerArray<?> createIntArrayOfSize(int... size_)
	{
		return _numerics.newIntArray(size_);
	}

	/**
	 * Creates a one dimensional array of the same length as the input 
	 * arguments, and which is initialised to contain those arguments
	 * 
	 * @param values_ The values to store in the array
	 * 
	 * @return The newly created and filled array
	 */
	static public IntegerArray<?> createIntArray(int... values_)
	{
		return _numerics.newIntArray(values_.length).fill(ArrayUtils.toObject(values_));
	}
}
