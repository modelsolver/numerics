package com.meliorbis.numerics;

public class NumericsException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public NumericsException()
	{
		super();
	}

	public NumericsException(String arg0_, Throwable arg1_)
	{
		super(arg0_, arg1_);
	}

	public NumericsException(String arg0_)
	{
		super(arg0_);
	}

	public NumericsException(Throwable arg0_)
	{
		super(arg0_);
	}

}
