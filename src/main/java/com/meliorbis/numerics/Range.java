/**
 * 
 */
package com.meliorbis.numerics;

import com.meliorbis.utils.Pair;

/**
 * Specifies a range of integers between two values (inclusive)
 * 
 * @author toby
 */
public final class Range extends Pair<Integer, Integer> 
{
	private static final long serialVersionUID = 1L;

	/**
	 * Use this one to indicate all possible values
	 */
	public final static Range FULL = new Range(Integer.MIN_VALUE, Integer.MAX_VALUE);

	public Range(int from_, int to_)
	{
		super(from_,to_);
	}
	
	public int from()
	{
		return getLeft();
	}
	
	public int to()
	{
		return getRight();
	}
}
