/**
 * 
 */
package com.meliorbis.numerics.convergence;

import com.meliorbis.utils.Pair;

/**
 * Interface for numerical methods which are expected to converge and can
 * hence be solver by the converger
 * 
 * @param <S> The state used as both input and output
 * 
 * @author Tobias Grasl
 */
public interface Convergable<S, E extends Exception>
{
	/**
	 * Performs an interation of the process and returns the latest value
	 * of the metric
	 * 
	 * @param input_ The input of the iteration
	 * 
	 * @return A pair containing the new state and also the convergence metric
	 * 
	 * @throws E If an error occurs
	 */
	Pair<S, Criterion> performIteration(S input_) throws E;
}
