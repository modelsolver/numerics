/**
 * 
 */
package com.meliorbis.numerics.convergence;

import java.util.LinkedList;
import java.util.List;

import com.meliorbis.utils.Pair;

/**
 * @author Tobias Grasl
 */
public class Converger
{

	public interface Callback {
		void notify(Pair<?,Criterion> state_, int period_);
	}

	private final List<Callback> _callbacks = new LinkedList<Callback>();
	
	public <S, E extends Exception> S converge(Convergable<S, E> convergable_, 
			S initial_, double target_) throws E
	{
		Pair<S, Criterion> state = new Pair<S, Criterion>(initial_, Criterion.INITIAL);
		
		int period = 0;
		
		while(state.getRight().getValue() > target_)
		{
			final Pair<S,Criterion> newState = convergable_.performIteration(state.getLeft());
			
			state = newState;
			
			final int periodForCallbacks = ++period; 
			
			_callbacks.forEach(c -> c.notify(newState, periodForCallbacks));
		}
		
		return state.getLeft();
	}

	public void addCallback(Callback cb_)
	{
		assert cb_ != null : "Callback must not be null";
		
		_callbacks.add(cb_);
	}

}
