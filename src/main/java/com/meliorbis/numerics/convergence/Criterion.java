/**
 * 
 */
package com.meliorbis.numerics.convergence;

/**
 * @author Tobias Grasl
 *
 */
public interface Criterion
{
	/**
	 * Gets the criterion Value
	 * 
	 * @return A double giving the value of the criterion
	 */
	public double getValue();
	
	static final Criterion INITIAL = new Criterion() {

		@Override
		public double getValue()
		{
			return Double.MAX_VALUE;
		}

		@Override
		public String toString()
		{
			return "INITIAL";
		}
		
		
	};
}
