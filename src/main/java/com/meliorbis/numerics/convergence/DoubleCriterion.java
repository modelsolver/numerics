/**
 * 
 */
package com.meliorbis.numerics.convergence;

/**
 * @author Tobias Grasl
 *
 */
public class DoubleCriterion implements Criterion
{
	private final double _val;
	private final String _label;
	private final String _formatString;
	
	public DoubleCriterion(double val_)
	{
		this(null,val_);
	}
	
	public DoubleCriterion(String label_, double val_)
	{
		_label = label_;
		_val = val_;
		
		if(_label != null) {
			_formatString = "%s: %.2e";
		} else {
			_formatString = "%2$.2e";
		}
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.numerics.convergence.Criterion#getValue()
	 */
	@Override
	public double getValue()
	{
		return _val;
	}

	@Override
	public String toString()
	{
		
		return String.format(_formatString,_label, _val);
	}
	
	static public DoubleCriterion of(double val_) 
	{
		return new DoubleCriterion(val_);
	}
}
