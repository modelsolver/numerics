package com.meliorbis.numerics.convergence;

import java.util.Arrays;
import org.apache.commons.lang.StringUtils;

/**
 * Combines multiple criteria by returning their maximum value
 * 
 * @author Tobias Grasl
 */
public class MaxCriterion implements Criterion
{
	final private Criterion[] _criteria;

	public MaxCriterion(Criterion... criteria_)
	{
		_criteria = criteria_;
	}

	@Override
	public double getValue()
	{
		return Arrays.stream(_criteria).reduce(Double.NEGATIVE_INFINITY,(running,crit) -> Double.max(running,crit.getValue()), Double::max);
	}
	
	@Override
	public String toString()
	{
		return StringUtils.join(_criteria,", ");
	}
}
