package com.meliorbis.numerics.convergence;

import java.io.PrintWriter;

import com.meliorbis.utils.Pair;

public class ProgressDisplayingCallback implements Converger.Callback
{
	private final PrintWriter _writer;

	public ProgressDisplayingCallback()
	{
		_writer = new PrintWriter(System.out);
	}
	
	public ProgressDisplayingCallback(PrintWriter writer_)
	{
		_writer = writer_;
	}
	
	@Override
	public void notify(Pair<?, Criterion> state_, int period_)
	{
		if (period_ % 10 == 0)
		{
			_writer.print(".");

			if (period_ % 100 == 0)
			{
				_writer.print(String.format("[%s]: %s\n", period_ / 100, state_.getRight()));
			}
			
			_writer.flush();
		}
	}

}
