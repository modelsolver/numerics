package com.meliorbis.numerics.demo;

import static com.meliorbis.numerics.DoubleArrayFactories.*;

import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleBinaryOp;

public class ArrayDemo
{
	public static void main(String[] args_) {
		new ArrayDemo().run();
	}

	@SuppressWarnings("unchecked")
	private void run()
	{
		DoubleArray<?> array = createArrayOfSize(2,3);

		array.fillDimensions(new double[] {.1,.2},0);

		// array contains:
		// .1, .1, .1
		// .2, .2, .1
		System.out.println(array);

		array.modifying().across(1).multiply(createArray(1,4,9));

		// array contains:
		// .1, .4, .9
		// .2, .8, 1.8
		System.out.println(array);
		
		array.modifying().across(0).with(createArray(.5,.5)).map(
				(DoubleBinaryOp<RuntimeException>)((double a, double b)-> a>b?a:0));
		
		// array contains:
		// 0,  0, .9
		// 0, .8, 1.8
		System.out.println(array);
	}
}
