/**
 * 
 */
package com.meliorbis.numerics.fixedpoint;

/**
 * When fixed-point strategies fail
 * 
 * @author Tobias Grasl
 */
public class FPStrategyException extends Exception
{
    public FPStrategyException(String arg0_)
    {
        super(arg0_);
    }

	public FPStrategyException(String arg0_, Throwable arg1_)
	{
		super(arg0_, arg1_);
	}
}
