package com.meliorbis.numerics.fixedpoint;

import com.meliorbis.numerics.function.MultiVariateVectorFunction;

/**
 * Created by toby on 28/01/2014.
 */
public interface FixedPointFinder
{
    double[] findFixedPoint(MultiVariateVectorFunction<Double> function_, double[] initialVals_);

    public void setDamping(double damping_);
}
