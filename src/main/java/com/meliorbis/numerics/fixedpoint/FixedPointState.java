package com.meliorbis.numerics.fixedpoint;

/**
 * A marker interface to mark an object as a state object for fixed point
 * calculations.
 * 
 * @author Tobias Grasl
 */
public interface FixedPointState
{

}
