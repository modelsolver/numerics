/**
 * 
 */
package com.meliorbis.numerics.fixedpoint;


/**
 * Implements a mechanism for successively adjusting the configuration of a model with no aggregate risk
 * until the solution found reproduces the aggregates assumed by individual agents
 * 
 * @param <T> The state type to be used
 * 
 * @author Tobias Grasl
 */
public interface FixedPointStrategy<T extends FixedPointState>
{
	/**
	 * Calculate the accuraccy of the match between assumed and implied outcomes, and adjusts the assumed values
	 * according to this strategy, ready for the next attempt.
	 * 
	 * @param state_ The state for which the calculation should be performed
	 * 
	 * @return The accuracy of the match between inputs and outputs
	 * 
	 * @throws FPStrategyException If there is a problem performing the calculation
	 */
	double calculateCriterionAndAdjust(T state_) throws FPStrategyException;

	/**
	 * @param delegate The delegate to be used to perform the actual calculations
	 */
	public abstract void setDelegate(FixedPointValueDelegate<T> delegate);
}
