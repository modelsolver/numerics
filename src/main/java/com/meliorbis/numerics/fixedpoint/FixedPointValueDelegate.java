package com.meliorbis.numerics.fixedpoint;

/**
 * A delegate interface that gives a fixed-point strategy access to read inputs and ouputs
 * of the function, and also to adjust the inputs for the next run.
 * 
 * @author Tobias Grasl
 */
public interface FixedPointValueDelegate<T extends FixedPointState>
{
	/**
	 * Set the provided inputs
	 * 
	 * @param inputs_ The input values to use
	 */
	public void setInputs(double[] inputs_);
	
	/**
	 * Returns the implied values, i.e. the function outputs
	 *
	 * @param state_ The state to use to perform the calculation
	 * 
	 * @return The function Outputs
	 */
	public double[] getOutputs(T state_);

    /**
     * Returns the implied values, i.e. the function outputs
     *
     * @return The function Outputs
     */
    public double[] getInitialInputs();

    /**
     * Notifies the delegate that fixed point has been identified to sufficient precision
     * 
     * @param state_ The state of the calculation
     */
    default public void solutionFound(T state_) {};
    
    /**
     * @return The bounds within which inputs should be kept
     */
	default public double[][] getBounds() {
		return null;
	};
}
