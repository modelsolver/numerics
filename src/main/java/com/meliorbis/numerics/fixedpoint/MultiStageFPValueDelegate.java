package com.meliorbis.numerics.fixedpoint;

import java.util.List;

/**
 * Provides additional information about the stages
 */
public interface MultiStageFPValueDelegate<T extends FixedPointState> extends FixedPointValueDelegate<T>
{
    /**
     * Returns a list of integer arrays. Each integer array lists the dimensions along which a fixed
     * point is to be found, conditional on the other dimension, at that stage. Stage 0 is solved
     * repeatedly, adjusting stage 1 according to the Newton Method at each step, and so on.
     *
     * @return The list of stages
     */
    public List<int[]> getStages();
}
