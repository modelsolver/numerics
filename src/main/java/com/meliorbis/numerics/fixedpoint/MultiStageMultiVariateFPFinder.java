package com.meliorbis.numerics.fixedpoint;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.meliorbis.numerics.Numerics;
import com.meliorbis.numerics.function.MultiVariateVectorFunction;

/**
 * A fixed point finder which iterates in multiple stages
 */
public class MultiStageMultiVariateFPFinder implements FixedPointFinder
{
    final FixedPointFinder[] _stageFinders;
    private final List<int[]> _stages;
    private double[] _currentValues;
    private double[] _lastOutput;

    /**
     * This constructor creates Newton-Method finders for the individual stages
     *
     * @param numerics_ The numerics instance this fixed point finder uses
     * @param precision_ Th desired precision
     * @param adjust_ The adjustment parameter
     * @param stages_ A list of integer arrays, each of which holds the dimensions for a stage
     */
    public MultiStageMultiVariateFPFinder(Numerics<Double> numerics_, Double precision_, Double adjust_, List<int[]> stages_)
    {
        _stages = stages_;
        _stageFinders = new MultiVariateNewtonFixedPointFinder[stages_.size()];

        for(int stage = 0; stage < stages_.size(); stage++)
        {
            _stageFinders[stage] = new MultiVariateNewtonFixedPointFinder(precision_, adjust_);
        }
    }

    /**
     * This constructor creates Newton-Method finders for the individual stages
     *
     * @param stages_ A list of integer arrays, each of which holds the dimensions for a stage
     * @param stageFinders_ The fixed point finder to use for each stage
     */
    public MultiStageMultiVariateFPFinder(List<int[]> stages_,
                                          FixedPointFinder[] stageFinders_)
    {
        _stages = stages_;
        _stageFinders = stageFinders_;
    }

    /**
     * Sets the damping for each stage individually
     *
     * @param damping_ Array of dampings of same length as the stages
     */
    public void setDamping(double... damping_)
    {
        for(int i = 0; i < damping_.length;i++)
        {
            _stageFinders[i].setDamping(damping_[i]);
        }
    }

    /**
     * Sets the damping for all stages to the same value
     * 
     * @param damping_ A value of damping to apply to updates in all stages
     */
    public void setDamping(double damping_)
    {
        for(int i = 0; i < _stageFinders.length;i++)
        {
            _stageFinders[i].setDamping(damping_);
        }
    }

    @Override
    public double[] findFixedPoint(MultiVariateVectorFunction<Double> function_,
                                   double[] initialVals_)
    {
        _currentValues = initialVals_.clone();

        _lastOutput = initialVals_.clone();

        StagedFunction[] functions = new StagedFunction[stageCount()];

        // Create the array of functions to be called at each state
        for(int stage = 0; stage < stageCount(); stage++)
        {
            functions[stage] = new StagedFunction(stage, stage == 0 ? function_ : functions[stage-1]);
        }

        _stageFinders[stageCount()-1].findFixedPoint(functions[stageCount()-1], extractStageVals(_currentValues, stageCount() - 1));

        return _lastOutput;
    }

    private double[] extractStageVals(double[] vals_, int stage_)
    {
        // Get the array of indexes for the stage
        final int[] stageIndices = _stages.get(stage_);
        double[] stageVals = new double[stageIndices.length];

        // Copy the values at those indexes to a new array
        for(int i = 0; i < stageVals.length; i++)
        {
            stageVals[i] = vals_[stageIndices[i]];
        }

        // Return that array
        return stageVals;
    }

    private void updateStageVals(double[] target_, Double[] newValues_, int stage_)
    {
        final int[] stageIndices = _stages.get(stage_);

        // Copy the values at those indexes to the target array
        for(int i = 0; i < stageIndices.length; i++)
        {
            target_[stageIndices[i]] = newValues_[i];
        }
    }

    private int stageCount()
    {
        return _stages.size();
    }

    private final class StagedFunction implements MultiVariateVectorFunction<Double>
    {
        private final int _stage;
        private final MultiVariateVectorFunction<Double> _nextFn;

        @Override
        public Double[] call(Double... args_)
        {
            // Update the current stage's values in the full array
            updateStageVals(_currentValues,args_,_stage);

            // If this is not the innermost stage...
            if(_stage > 0)
            {
                //...solve the next stage
                // NOTE: Using currentValues here ensures that for successive loops of the outer iteration, the inner
                // iteration reuses the fixed point from the last loop, thus hopefully allowing quicker convergence in
                // later stages
                _stageFinders[_stage-1].findFixedPoint(_nextFn,extractStageVals(_currentValues,_stage-1));
            }
            else
            {
                // Just pass on the call to the overall function being solved
                // Note we can't just replace this instance with the outside fn because we do need to update the
                // stage values
                System.arraycopy(ArrayUtils.toPrimitive(_nextFn.call(ArrayUtils.toObject(_currentValues))),0, _lastOutput,0, _lastOutput.length);
            }

            return ArrayUtils.toObject(extractStageVals(_lastOutput,_stage));
        }


        private StagedFunction(int stage_, MultiVariateVectorFunction<Double> nextFn_)
        {
            _stage = stage_;
            _nextFn = nextFn_;
        }
    }
}
