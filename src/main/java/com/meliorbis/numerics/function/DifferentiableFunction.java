package com.meliorbis.numerics.function;

/**
 * Interface for functions that are differentiable
 *
 * @author Tobias Grasl
 */
public interface DifferentiableFunction<I,O> extends Function<I,O>
{
    /**
     * Returns partial derivative of the function along the given dimension as a function
     * across the range of this function
     *
     * @param dimension_ The dimension along which to evaluate the derivative
     *
     * @return A function that is the derivative of this one, with the same domain
     */
    Function<I,O> partialDerivative(int dimension_);

    /**
     * Returns partial derivative of the function along the given dimension at the
     * point specified, if any.
     *
     * Not that the array at may include up to the number of members as there are dimensions
     * in the function. Any value that is NaN (or similar) indicates not to evaluate in that
     * dimension but to include that dimension in the returned function. If at_ is shorter than
     * the bnumber of dimensions then the additional dimensions are treated as though NaN had
     * been passed
     *
     * @param dimension_ The dimension along which to evaluate the derivative
     * @param at_ The (perhaps partially defined, or not at all) point at which to evaluate the
     *            derivative.
     *
     * @return A function that is the derivative of this one in the requested dimension at the
     * requested point. The number of dimensions of the function is equal to the number of points
     * dimensions not given a valid value in at_. The output dimension is the same as the output of
     * this function.
     */
    Function<I,O> partialDerivative(int dimension_, @SuppressWarnings("unchecked") I... at_);
}
