/**
 * 
 */
package com.meliorbis.numerics.function;

/**
 * The domain over which a function is defined
 * 
 * @param <I> The type of number of the domain 
 * 
 * @author Tobias Grasl
 */
public interface Domain<I>
{
	/**
	 * @return The number of dimensions
	 */
	int getNumberOfDimensions();
}
