package com.meliorbis.numerics.function;

/**
 * A generic base interface for functions
 *
 * @param <I> The input type
 * @param <O> The output type
 * 
 * @author Tobias Grasl
 */
public interface Function<I, O>
{
    /**
     * Calls the function for the provided inputs and returns the output
     *
     * @param inputs_ The function inputs
     *
     * @return The function output for the inputs provided
     */
    O call(@SuppressWarnings("unchecked") I... inputs_);

    /**
     * Restricts the function to have the values specified in partialInputs in those dimensions
     * where partialInputs provides a valid value. The invalid values are implementation dependent.
     * If partialInputs is shorter than the number of dimensions of the function, then the remaining
     * dimensions are assumed to be specified as invalid.
     *
     * The returned function will have as many dimensions as invalid values were specified for.
     *
     * For for, in a 3-dimensional function, if a valid value V were passed for dimension 1 and not 0 and 2,
     * then the returned function would be two dimensional and for values (V1, V2) passed would return
     * the same as this function does for (V1, V, V2).
     *
     * @param partialInputs_ The array of values to which to restrict the function in each dimension, some
     *                       of which may be invalid
     *
     * @return The restricted function
     */
    Function<I,O> restrict(@SuppressWarnings("unchecked") I... partialInputs_);
}
