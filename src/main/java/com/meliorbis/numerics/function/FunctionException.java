package com.meliorbis.numerics.function;

import com.meliorbis.numerics.NumericsException;

/**
 * @author Tobias Grasl
 */
public class FunctionException extends NumericsException
{
    public FunctionException(String arg0_, Throwable arg1_)
    {
        super(arg0_, arg1_);
    }

    public FunctionException(String arg0_)
    {
        super(arg0_);
    }
}
