package com.meliorbis.numerics.function;

/**
 * Represents a mathematical function that accepts and returns multiple values
 */
public interface MultiVariateVectorFunction<T>
{
    /**
     * Execute the function
     *
     * @param args_ The arguments to the function
     *
     * @return The value returned by the function
     */
    public T[] call(@SuppressWarnings("unchecked") T... args_);
}
