/**
 * 
 */
package com.meliorbis.numerics.function.grid;

import com.meliorbis.numerics.function.Domain;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * A domain defined over a rectangular grid of points
 * 
 * @param <I> The type of number of the domain
 * 
 * @author Tobias Grasl
 */
public interface RectangularGridDomain<I, A extends MultiDimensionalArray<I, ?>> extends Domain<I>
{
	
	/**
	 * @param dimension_ The dimension to retrieve the 
	 * 
	 * @return The coordinates along the axis in the requested dimension
	 */
	public A getAxis(int dimension_);
	
	/**
	 * Creates an output grid of values of the appropriate type with the requested number
	 * of output dimensions.
	 * 
	 * The grid will be of size &lt;domainSize&gt; x &lt;outputDims&gt;
	 * 
	 * @param outputDims The number of output dimensions, which will form the size of the final	dimension 
	 * @return An array of the appropriate type and size
	 */
	public A createValueGrid(int outputDims);

	/**
	 * Creates an output grid of values of the appropriate type with a single output dimension
	 * 
	 * The grid will be of size &lt;domainSize&gt; x 1
	 * 
	 * @return An array of the appropriate type and size
	 */
	DoubleArray<?> createValueGrid();
	
	/**
	 * Given a function with output values {@code values_} and input values on the grid points in this domain, <i>except</i> in dimension {@code axis_}, where
	 * the inputs are assumed non-rectangular and given by {@code nonRectangularDomainValues_}, this method will interpolate that exceptional dimension 
	 * onto the grid and return the resulting rectangular grid function.
	 * 
	 * The interpolation is unconstrained, so that y values for target x values lying before the first source x are found by extrapolation.
	 * 
	 * @param axis_ The dimension along which to perform the mapping
	 * @param nonRectangularDomainValues_ The input values along dimension {@code axis_} for each point orthogonal to that axis.
	 * @param values_ The values at the points defined by this domain's grid except in dimension {@code axis_}, where the input values are in
	 * {@code nonRectangularDomainValues_}
	 * 
	 * @return A function on this domain that corresponds to the mapping defined by the two arrays passed
	 * 
	 * @param <O> The output value type
	 * @param <OA> The array type to use to hold the outputs
	 */
	<O, OA extends MultiDimensionalArray<?, ?>> RectangularGridFunction<I, O, OA> interpolateToGrid(int axis_, A nonRectangularDomainValues_, OA values_); 
	
	/**
	 * Given a function with output values {@code values_} and input values on the grid points in this domain, <i>except</i> in dimension {@code axis_}, where
	 * the inputs are assumed non-rectangular and given by {@code nonRectangularDomainValues_}, this method will interpolate that exceptional dimension 
	 * onto the grid and return the resulting rectangular grid function.
	 * 
	 * The function can be constrained in the sense that any the first value of {@code sourceX_} along each row is assumed to be the lowest acceptable one
	 * 
	 * @param axis_ The dimension along which to perform the mapping
	 * @param nonRectangularDomainValues_ The input values along dimension {@code axis_} for each point orthogonal to that axis.
	 * @param values_ The values at the points defined by this domain's grid except in dimension {@code axis_}, where the input values are in
	 * {@code nonRectangularDomainValues_}
	 * @param constrained_ If true, the constraint mechanism is applied.
	 * 
	 * @return A function on this domain that corresponds to the mapping defined by the two arrays passed
	 * 
	 * @param <O> The output value type
	 * @param <OA> The array type to use to hold the outputs
	 */
	public <O, OA extends MultiDimensionalArray<?, ?>> RectangularGridFunction<Double, O, OA> interpolateToGrid(int axis_, DoubleArray<?> nonRectangularDomainValues_, OA values_, boolean constrained_);
}
