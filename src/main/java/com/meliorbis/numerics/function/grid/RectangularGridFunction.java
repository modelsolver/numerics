/**
 * 
 */
package com.meliorbis.numerics.function.grid;

import com.meliorbis.numerics.function.Function;

/**
 * A function defined on a rectangular grid of points, which is interpolated
 * to find values at points not lying on the grid
 * 
 * @author Tobias Grasl
 * 
 * @param <I> The input value type
 * @param <O> The output value type
 * @param <OA> The type of array used to hold multiple output values
 */
public interface RectangularGridFunction<I, O, OA> extends Function<I, O>
{
	OA getValues();
}
