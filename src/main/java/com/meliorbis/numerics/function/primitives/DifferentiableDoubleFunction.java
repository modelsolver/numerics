/**
 * 
 */
package com.meliorbis.numerics.function.primitives;

import org.apache.commons.lang.ArrayUtils;

import com.meliorbis.numerics.function.DifferentiableFunction;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Primitive double specialisation of IDifferentiableFunction
 * 
 * @author Tobias Grasl
 */
public interface DifferentiableDoubleFunction extends DifferentiableFunction<Double, DoubleArray<?>>
{
	/**
     * Specialise return type
     * 
     * @see DifferentiableFunction#partialDerivative(int)
     */
	DifferentiableDoubleFunction partialDerivative(int dimension_);

    /**
     * Specialise return type and implement default to delegate to primitive version
     * 
     * @see DifferentiableFunction#partialDerivative(int, Object...)
     */
	default DifferentiableDoubleFunction partialDerivative(int dimension_, Double... at_)
	{
		return partialDerivative(dimension_, ArrayUtils.toPrimitive(at_));
	}
	
    /**
     * Primitive version of partialDerivative
     * 
     * @param dimension_ The dimension along which to find the derivative
     * @param at_ The point at which to find the derivative
     * 
     * @return The derived function
     */
	DifferentiableDoubleFunction partialDerivative(int dimension_, double... at_);
}
