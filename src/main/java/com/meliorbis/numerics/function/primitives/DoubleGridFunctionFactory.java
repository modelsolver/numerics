/**
 * 
 */
package com.meliorbis.numerics.function.primitives;

import java.util.List;

import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Factory class for creating functions defined on grids of doubles
 * 
 * @author Tobias Grasl
 */
public class DoubleGridFunctionFactory
{
	/**
	 * Creates a new domain object
	 * 
	 * @param gridPoints_ The points along the axes that define the rectangular grid
	 * 
	 * @return The new domain
	 */
	public DoubleRectangularGridDomain createDomain(DoubleArray<?>[] gridPoints_)
	{
		return new DoubleRectangularGridDomain(gridPoints_, this);
	}
	
	/**
	 * Creates a new domain object
	 * 
	 * @param gridPoints_ The points along the axes that define the rectangular grid
	 * 
	 * @return The new domain
	 */
	public DoubleRectangularGridDomain createDomain(List<DoubleArray<?>> gridPoints_)
	{
		return new DoubleRectangularGridDomain(gridPoints_, this);
	}
	
	/**
	 * Creates a new function defined over domain_ and having values values_
	 * 
	 * @param domain_ The domain over which the function is defined 
	 * @param values_ The values the function takes at the grid points of domain_
	 * 
	 * @return The newly constructed function
	 */
	public DoubleGridFunction createFunction(DoubleRectangularGridDomain domain_, DoubleArray<?> values_) 
	{
		return new DoubleGridFunction(domain_, values_, this);
	}
	
	/**
	 * Creates a new function defined over domain_ and having values values_
	 * 
	 * @param gridPoints_ The domain over which the function is defined 
	 * @param values_ The values the function takes at the grid points of domain_
	 * 
	 * @return The newly constructed function
	 */
	public DoubleGridFunction createFunction(List<DoubleArray<?>> gridPoints_, DoubleArray<?> values_) 
	{
		return new DoubleGridFunction(createDomain(gridPoints_), values_, this);
	}
	
	/**
	 * Creates a new function defined over domain_ and having values values_, and which only has one output
	 * value.
	 * 
	 * @param gridPoints_ The domain over which the function is defined 
	 * @param values_ The values the function takes at the grid points of domain_
	 * 
	 * @return The newly constructed function
	 */
	public SingleValuedDoubleGridFunction createSingleValuedFunction(List<DoubleArray<?>> gridPoints_, DoubleArray<?> values_)
	{
		return new SingleValuedDoubleGridFunction(createFunction(gridPoints_, values_));
	}
}
