package com.meliorbis.numerics.function.primitives;

import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * @author Tobias Grasl
 */
public interface MultiValuedDoubleFunction extends DoubleFunction<DoubleArray<?>>
{
}
