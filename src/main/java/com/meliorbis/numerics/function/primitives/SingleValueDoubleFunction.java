package com.meliorbis.numerics.function.primitives;


/**
 * @author Tobias Grasl
 */
public interface SingleValueDoubleFunction extends DoubleFunction<Double>
{
	/**
	 * Provides a default that redirects to the version returning a primitive
	 * 
	 * @param inputs_ The inputs to the function
	 * 
	 * @return The result of the function call
	 */
	default Double call(double... inputs_)
	{
		return callDouble(inputs_);
	}
	
	/**
	 * Calls the function and returns a primitive double
	 * 
	 * @param inputs_ The inputs to the function
	 * 
	 * @return The primitive double result
	 */
	double callDouble(double... inputs_);
}
