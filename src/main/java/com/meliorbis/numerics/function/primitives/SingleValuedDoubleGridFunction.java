package com.meliorbis.numerics.function.primitives;

import com.meliorbis.numerics.function.FunctionException;


/**
 * Defines over a grid of possible input values a single valued function. Values between the grid points will be
 * obtained by interpolation
 *
 * @author Tobias Grasl
 */
public class SingleValuedDoubleGridFunction implements SingleValueDoubleFunction
{

    private final DoubleGridFunction _delegate;
    
    public SingleValuedDoubleGridFunction(DoubleGridFunction delegate_)
    {
        _delegate = delegate_;
        
        validate();
    }
    
    private void validate()
    {
    	if(_delegate.getValues().numberOfDimensions() > _delegate.getDomain().getNumberOfDimensions() + 1)
    	{
	    	throw new FunctionException("The values array of a single-valued function can have at most one dimension in excess of the number of dimensions of the domain");
    	}

    	if(_delegate.getValues().numberOfDimensions() == _delegate.getDomain().getNumberOfDimensions() + 1 && 
    			_delegate.getValues().size()[_delegate.getDomain().getNumberOfDimensions()] != 1)
    	{
    		throw new FunctionException("The value-dimension must be of size 1 in a single-valued function");
    	}

    }

    @Override
    public double callDouble(double... inputs_)
    {
        return _delegate.callWithDouble(inputs_).get(0);
    }
    
    @Override
    public Double callWithDouble(double... inputs_)
    {
        return _delegate.callWithDouble(inputs_).get(0);
    }

    @Override
    public SingleValueDoubleFunction restrict(double... partialInputs_)
    {
        return new SingleValuedDoubleGridFunction(_delegate.restrict(partialInputs_));
    }
}
