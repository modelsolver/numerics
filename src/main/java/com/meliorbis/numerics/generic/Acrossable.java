/**
 * 
 */
package com.meliorbis.numerics.generic;

/**
 * Interface for objects that have multiple dimensions and can have operations applied across those dimensions
 * 
 * @author Tobias Grasl
 *
 * @param <T> The underlying type and also the return type of reductions
 * @param <RM> The return type of mappings
 */
public interface Acrossable<T, RM extends Acrossable<T, RM>>
        extends MappableReducible<T,RM,T>
{
	
	/**
     * @return The number of dimensions in this object
     */
    int numberOfDimensions();
    
    /**
     * Returns an operand that will cause operations applied to it to be performed separately for each orthogonal point
     * across the given dimensions.
     * 
     * Note that the return type of reductions after across is the same as the return type of mappings
     *
     * @param dimensions_ The dimensions across which to apply the operation
     *
     * @return The operand which will apply the operation across dimensions
     */
    MappableReducible<T, RM, RM> across(int... dimensions_);
}
