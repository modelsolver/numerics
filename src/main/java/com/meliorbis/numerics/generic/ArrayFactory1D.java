package com.meliorbis.numerics.generic;

/**
 * Factory interface for creating 1-dimensional arrays of generic types
 */
@FunctionalInterface
public interface ArrayFactory1D<T>
{
    T[] createArray(int size_);
}
