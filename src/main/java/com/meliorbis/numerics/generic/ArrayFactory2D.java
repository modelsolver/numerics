package com.meliorbis.numerics.generic;

/**
 * Factory for creating double arrays of generic type
 */
@FunctionalInterface
public interface ArrayFactory2D<T>
{
    T[][] createArray(int size_);
}
