package com.meliorbis.numerics.generic;

/**
 * The specialisation of NaryOp for operations that accept two parameters
 * 
 * @param <T> The numerics type of inputs and outputs
 * @param <E> The exception thrown if the operation fails
 * 
 * @author Tobias Grasl
 */
public interface BinaryOp<T, E extends Exception> extends NaryOp<T, T, E>
{
	/**
	 * Performs the binary op for the two operands passed
	 * 
	 * @param left_ The left operand
	 * @param right_ The right operand
	 * 
	 * @return The result of the operation
	 * 
	 * @throws E If an error occurred performing the operation
	 */
    public T perform(T left_, T right_) throws E;

    /**
     * Overrides the superclass method to delegate to the binary method
     * 
     * @param inputs_ The inputs to the operation, should be of length 2
     * 
     * @return The result of the operation
     * 
     * @throws E If the operation fails.
     */
    @Override
    default T perform(@SuppressWarnings("unchecked") T... inputs_) throws E
    {
        return perform(inputs_[0], inputs_[1]);
    }
}
