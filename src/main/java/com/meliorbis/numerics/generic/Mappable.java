package com.meliorbis.numerics.generic;



/**
 * An interface for collection objects on which operations can be performed. The member type is T, and the resulting
 * object type is R
 *
 * @param <T> The individual element type
 * @param <RM> The return type of mapping operations, which will commonly be the most derived type of this. This parameter is
 *           used to avoid the need to cast
 */
public interface Mappable<T,  RM extends Mappable<T, RM>>
{
    /**
     * An IOperand which adds the otherArrays alongside this one, for n-ary operations
     *
     * @param otherArrays_ The other operands on which an operation is to be performed
     * 
     * @param <S> The type of the array(s) being operated with
     *
     * @return An IMappable, operations performed on which will use both this and otherArrays_
     */
	<S extends T> Mappable<T, RM> with(@SuppressWarnings("unchecked") MultiDimensionalArray<S, ?>... otherArrays_);

    /**
     * Performs the provided operation on each element of this operand, returning the result
     *
     * @param operation_ The operation to perform
     *
     * @param <E> The type of exception thrown by the op
     * 
     * @return The result of the operation
     * 
     * @throws E If the operation fails
     */
    <E extends Exception> RM map(NaryOp<T, T, E> operation_) throws E;
    
    /**
     * Performs the provided operation on each element of this operand, returning the result
     *
     * @param operation_ The operation to perform
     *
     * @param <E> The type of exception thrown by the op
     * 
     * @return The result of the operation
     * 
     * @throws E If the operation fails
     */
    <E extends Exception> RM[] map(MultiValuedNaryOp<T, E> operation_) throws E;
}
