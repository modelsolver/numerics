package com.meliorbis.numerics.generic;

/**
 * Interface that combines IMappable and IReducible
 *
 * @author Tobias Grasl
 */
public interface MappableReducible<T, RM extends MappableReducible<T,RM,?>, RR>
        extends MappableWithSimpleOps<T, RM>, ReducibleWithSimpleOps<T, RR>
{
}
