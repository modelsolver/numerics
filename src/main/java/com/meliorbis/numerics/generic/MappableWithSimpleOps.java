package com.meliorbis.numerics.generic;

/**
 * An extension of IMappable that provides basic arithmetic operations
 *
 * @param <T> The individual element type
 * @param <RM> The return type of mapping operations, which will commonly be the most derived type. This parameter is
 *           used to avoid the need to cast
 */
public interface MappableWithSimpleOps<T, RM extends MappableWithSimpleOps<T, RM>> extends Mappable<T, RM>
{
	/**
	 * @return A reference to this mappable that causes the results of any mappings 
	 * applied to be stored in this object itself
	 */
	MappableWithSimpleOps<T, ? extends MappableWithSimpleOps<T,?>> modifying();
	
	/**
	 * @return A reference to this mappable that <em>does not</em> cause the results of any mappings 
	 * applied to be stored in this object
	 */
	MappableWithSimpleOps<T, ? extends MappableWithSimpleOps<T,?>> nonModifying();
	
    /**
     * Performs a point-by-point multiplication of two equally sized arrays and
     * returns the result
     *
     * @param other_ Array to multiply with
     * 
     * @param <S> The type of the array being multiplied in
     *
     * @return A pointwise product of this and other
     */
    <S extends T> RM multiply(MultiDimensionalArray<S, ?> other_);

    /**
     * The point-wise quotient of this multi-dimensional array and other
     *
     * @param other_ The operand to divide this one by
     * 
     * @param <S> The type of the array being divided by
     *
     * @return The quotient of this operand divided by other
     */
    <S extends T> RM divide(MultiDimensionalArray<S, ?> other_);

    /**
     * The point-wise sum of this multi-dimensional array and other
     *
     * @param other_ The operand to add to this one
     * 
     * @param <S> The type of the array being added
     *
     * @return The sum of the two operands
     */
    <S extends T> RM add(MultiDimensionalArray<S, ?> other_);

    /**
     * The point-wise difference between this multi-dimensional array and other
     *
     * @param other_ The operand to subtract from this one
     * 
     * @param <S> The type of the array being subtracted
     *
     * @return The difference between this operand and other
     */
    <S extends T> RM subtract(MultiDimensionalArray<S, ?> other_);
}
