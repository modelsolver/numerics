/**
 * 
 */
package com.meliorbis.numerics.generic;

/**
 * An interface to check whether a given value matches some criteria
 * 
 * @author toby
 */
public interface Matcher<T>
{
	public boolean matches(T val_);
}
