package com.meliorbis.numerics.generic;

public class MultiDimensionalArrayException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public MultiDimensionalArrayException()
	{}

	public MultiDimensionalArrayException(String arg0_, Throwable arg1_)
	{
		super(arg0_, arg1_);
	}

	public MultiDimensionalArrayException(String arg0_)
	{
		super(arg0_);
	}

	public MultiDimensionalArrayException(Throwable arg0_)
	{
		super(arg0_);
	}
}
