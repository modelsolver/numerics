/**
 * 
 */
package com.meliorbis.numerics.generic;

import java.util.List;

import com.meliorbis.numerics.NumericsException;

/**
 * Factory interface for multi-dimensional arrays
 * 
 * @param <T> The numeric type of the array
 * @param <R> The type of arrays created
 * 
 * @author Tobias Grasl
 */
public interface MultiDimensionalArrayFactory<T, R>
{
	/**
	 * Creates a multi-dimensional array with the given dimensions
	 * 
	 * @param dimensions_ The dimensions of the requested array
	 * 
	 * @return An appropriately sized array
	 */
    R newArray(int... dimensions_);

	/**
	 * Creates a one-dimensional array with the given data
	 * 
	 * @param data_ The data to use
	 * 
	 * @return The created array
	 */
    R newArray(@SuppressWarnings("unchecked") T... data_);
	
	/**
	 * Parses a string to the type of this factory
	 * 
	 * @param valueString_ The string containing a value of this type
	 * @return The value of the appropriate type
	 */
	T fromString(String valueString_) throws NumericsException;

	/**
	 * Creates an array with the specified dimensions
	 * 
	 * @param dimensions_ The dimensions of the new array
	 * 
	 * @return The newly created array
	 */
    R newArray(List<Integer> dimensions_);
}
