/**
 * 
 */
package com.meliorbis.numerics.generic;

/**
 * An operation with n inputs and multiple outputs
 * 
 * @author Tobias Grasl
 * 
 * @param <T> The input type of the operation, and T[] will be its output type
 * @param <E> The exception thrown by the operation
 */
@FunctionalInterface
public interface MultiValuedNaryOp<T, E extends Exception> extends NaryOp<T, T[], E>
{
	 
}
