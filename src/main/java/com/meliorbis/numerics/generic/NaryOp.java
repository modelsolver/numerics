/**
 * 
 */
package com.meliorbis.numerics.generic;

/**
 * An operation with n inputs
 * 
 * @author Tobias Grasl
 * 
 * @param <T> The input type of the operation
 * @param <R> The return type of the operation
 * @param <E> The exception thrown by the operation
 */
public interface NaryOp<T,R, E extends Exception>
{
    /**
     * Perform the operation on the provided inputs and return the result
     *
     * @param inputs_ The inputs to perform the operation on
     *
     * @return The result of the operation
     * 
     * @throws E If an error occurs
     */
    R perform(@SuppressWarnings("unchecked") T... inputs_) throws E;
}
