package com.meliorbis.numerics.generic;

/**
 * Indicates that an iterator can iterate over the same range multiple times
 * 
 * @author Tobias Grasl
 */
public interface ParallelIterator
{
	public boolean hasNextParallel();

	public int nextParallel();
	
	public int parallelCount();
}
