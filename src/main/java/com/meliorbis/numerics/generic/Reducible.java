package com.meliorbis.numerics.generic;

import java.util.Iterator;

import com.meliorbis.numerics.generic.impl.ReductionBase;

/**
 * Interface for objects that contain a number of simpler objects which can be reduced
 *
 * @author Tobias Grasl
 *
 * @param <T> The individual instance type
 * @param <R> The type resulting from reduction
 */
public interface Reducible<T, R>
{
    /**
     * Performs the provided reduction operation across all elements of this object
     *
     * @param reduction_ The reduction to perform
     *
     * @param <E> The type of exception thrown bty the reduction
     * 
     * @return The result of the reduction
     * 
     * @throws E If the reduction fails
     */
    <E extends Exception> R reduce(ReductionBase<T,? extends Iterator<T>, E> reduction_) throws E;
}
