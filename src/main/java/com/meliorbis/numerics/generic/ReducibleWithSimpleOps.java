package com.meliorbis.numerics.generic;

/**
 * Adds a few simple reductions
 *
 * @author Tobias Grasl
 *
 * @param <T> The individual instance type
 * @param <R> The type resulting from reduction
 */
public interface ReducibleWithSimpleOps<T, R> extends Reducible<T,R>
{
    /**
     * Returns the maximum value of elements
     *
     * @return The maximum value of this object
     */
    R max();

    /**
     * Determines the sum of elements
     *
     * @return The sum of elements
     */
    R sum();

    /**
     * Determines the minimum value of elements held in this object
     *
     * @return The minimum element value
     */
    R min();
}
