/**
 * 
 */
package com.meliorbis.numerics.generic;

/**
 * An iterator over a collection, such as a multidimensional array, that is indexed in 
 * multiple dimensions, and which is also settable - that is the, underlying object at each
 * point can be set
 * 
 * @author Tobias Grasl
 */
public interface SettableIndexedIterator<T> extends SettableIterator<T>
{
    /**
	 * Get multi-dimensional index of the current point in the
	 * iteration
	 * 
	 * @return The index of iteration
	 */
	int[] getIndex();
}
