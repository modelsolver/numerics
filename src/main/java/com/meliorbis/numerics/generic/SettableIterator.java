package com.meliorbis.numerics.generic;

import java.util.Iterator;

/**
 * An iterator which allows underlying elements to be set
 */
public interface SettableIterator<T> extends Iterator<T>
{
    /**
     * Get the current value of iteration
     *
     * @return Gets the current value
     */
    T get();

    /**
     * Changes the current value
     *
     * @param val_ The value to set
     */
    void set(T val_);
    
    /**
	 * Resets the iterator to its pre-start position
	 */
	void reset();
}
