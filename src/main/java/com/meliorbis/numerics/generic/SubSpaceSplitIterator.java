package com.meliorbis.numerics.generic;

public interface SubSpaceSplitIterator<T> extends SettableIndexedIterator<T> {
	
	/**
	 * An iterator that iterates orthogonally to this one, i.e.
	 * along the dimensions this one does not iterate over
	 * 
	 * @return The iterator described
	 */
	SubSpaceSplitIterator<T> getOrthogonalIterator();
	
	/**
	 * Gets the index of the current position over the full
	 * space
	 * 
	 * @return The index in the full space
	 */
	int[] getFullIndex();

	/**
	 * Gets the current position of the part of the index not being iterated over
	 * 
	 * @return The value of the index in the dimensions not being iterated by this iterator (orthogonal dimensions!)
	 */
	public int[] getOtherIndex();
	
	
	/**
	 * Gets the sizes of the dimensions this iterator iterates over
	 * 
	 * @return The sizes of the subspace
	 */
	int[] getSizes();
}
