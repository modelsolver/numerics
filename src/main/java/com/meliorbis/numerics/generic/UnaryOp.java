package com.meliorbis.numerics.generic;

/**
 * The specialisation of NaryOp for operations that accept only one parameter
 * 
 * @param <T> The input and output type of the operation
 * @param <E> The exception thrown by the operation
 * 
 * @author Tobias Grasl
 */
public interface UnaryOp<T, E extends Exception> extends NaryOp<T, T, E>
{
	/**
	 * Performs the operation
	 * 
	 * @param operand_ The single input to the operation
	 * 
	 * @return The result of the operation
	 *  
	 * @throws E If the operation fails
	 */
    public T perform(T operand_) throws E;

    /**
     * Overrides the superclass method to delegate to the unary method
     * 
     * @param inputs_ The input to the operation, should be of length 1
     * 
     * @return The result of the operation
     * 
     * @throws E If the operation fails.
     */
    @Override
    default T perform(@SuppressWarnings("unchecked") T... inputs_) throws E
    {
        return perform(inputs_[0]);
    }
}
