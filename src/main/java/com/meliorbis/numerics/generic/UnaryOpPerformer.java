package com.meliorbis.numerics.generic;

/**
 * Implementing classes accept an operation, and perform it in a implementation-defined way,
 * for example for each member of a collection. The result is implementation dependend and may,
 * for example, be a collection of the results of each time the op was performed
 */
public interface UnaryOpPerformer<T, R>
{
    <E extends Exception> R perform(UnaryOp<T, E> op_) throws E;
}
