package com.meliorbis.numerics.generic;

public interface ZeroChecker<V>
{
	boolean isZero(V t_);
}