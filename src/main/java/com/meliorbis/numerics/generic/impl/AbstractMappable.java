package com.meliorbis.numerics.generic.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.ArrayUtils;

import com.meliorbis.numerics.generic.Acrossable;
import com.meliorbis.numerics.generic.BinaryOp;
import com.meliorbis.numerics.generic.Mappable;
import com.meliorbis.numerics.generic.MappableReducible;
import com.meliorbis.numerics.generic.MappableWithSimpleOps;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.MultiValuedNaryOp;
import com.meliorbis.numerics.generic.NaryOp;
import com.meliorbis.numerics.generic.ParallelIterator;
import com.meliorbis.numerics.generic.SettableIterator;
import com.meliorbis.numerics.generic.SubSpaceSplitIterator;
import com.meliorbis.numerics.threading.ComputableRecursiveAction;
import com.meliorbis.numerics.threading.ComputableRecursiveTask;
import com.meliorbis.numerics.threading.CurrentThreadExecutor;
import com.meliorbis.numerics.threading.Executor;
import com.meliorbis.utils.Timer;
import com.meliorbis.utils.Timer.Stoppable;

/**
 * An abstract implementation of IMappableWithSimpleOps that provides common functionality, and in particular enables
 * the modifying() mechanism to work for mapping operations that modify the first operand
 *
 * @param <T> The member type
 * @param <RM> The return type of mapping operations
 */
@SuppressWarnings("unchecked")
public abstract class AbstractMappable<T, RM extends AbstractMappable<T, RM>> 
																	implements Acrossable<T, RM>
{
	public static AtomicInteger callCount = new AtomicInteger(0);
	
    public static final int POINTWISE_BATCH_COUNT = 5;
    protected final Executor _executor;

    public AbstractMappable(Executor executor_)
    {
        super();
        _executor = executor_;
    }

    @Override
    public <E extends Exception> RM map(final NaryOp<T, T,  E> op_) throws E
    {
        // Perform a mapping without any other operands
        return with().map(op_);
    }
    
    @Override
    public <E extends Exception> RM[] map(final MultiValuedNaryOp<T, E> op_) throws E
    {
        // Perform a mapping without any other operands
        return with().map(op_);
    }

    protected boolean isModifying()
    {
        return false;
    }

    @Override
    public abstract MappableWithSimpleOps<T, ? extends MappableWithSimpleOps<T, ?>> modifying();

    @Override
    public <S extends T> RM multiply(MultiDimensionalArray<S, ?> other_)
    {
        return with(other_).map(getMultOp());
    }

    @Override
    public <S extends T> RM divide(MultiDimensionalArray<S, ?> other_)
    {
        return with(other_).map(getDivisionOp());
    }

    @Override
    public <S extends T> RM add(MultiDimensionalArray<S, ?> other_)
    {
        return with(other_).map(getAddOp());
    }

    @Override
    public <S extends T> RM subtract(MultiDimensionalArray<S, ?> other_)
    {
        return with(other_).map(getSubtractionOp());
    }
    
    /**
     * This method creates a callable task that performs the provided operation on each of the elements of the provided
     * of the provided input iterators. Storing the result in the output iterator. All iterators must, therefore, have
     * the same number of remaining elements.
     *
     * @param op_     The operation to perform
     * @param inputs_ The inputs to the operation
     * @param result_ The iterator in which to store the results
     * @param iterateResult_ Indicates whether the result iterator should also be iterated. If it is the same as one of the
     * inputs it is not a good idea to iterate it too
     * @param <E>     The type of exception thrown by the operation
     * @return An NaryOpCallable which, when executed, will perform the operation as described
     */
    protected abstract <E extends Exception> NaryOpCallable<T, E> createNaryOpCallable(
            NaryOp<T,?,E> op_,
            SettableIterator<? extends T>[] inputs_,
            SettableIterator<?> result_, boolean iterateResult_);

    /**
     * @return The binary add operation for the underlying type
     */
    abstract public BinaryOp<T, RuntimeException> getAddOp();

    /**
     * @return The binary subtract operation for the underlying type
     */
    abstract public BinaryOp<T, RuntimeException> getSubtractionOp();

    /**
     * @return The binary multiplication operation for the underlying type
     */
    abstract public BinaryOp<T, RuntimeException> getMultOp();

    /**
     * @return The binary multiplication operation for the underlying type
     */
    abstract public BinaryOp<T, RuntimeException> getDivisionOp();

    /**
     * @return The result target of pointwise operations
     */
    abstract protected RM getPointwiseResult();

    @Override
    final public MappableReducible<T, RM, RM> across(int... dimensions_)
    {
        return new AcrossOperand(dimensions_);
    }
    
    /**
     * This function is actually in IMultiDimensionalArray, which this class does not implement, but it needs to be available
     * 
     * @param dimensions_ The dimensions to iterate over in parallel
     * 
     * @return A list of parallel iterators
     * 
     * @see MultiDimensionalArray#parallelIterators(int[])
     */
    abstract public List<? extends SettableIterator<T>> parallelIterators(int[] dimensions_);

    /**
     * @param dimensions_ The dimensions to iterate over
     * 
     * @return A split iterator that iterates over the requested dimensions
     * 
     * @see MultiDimensionalArray#iteratorAcross(int[])
     */
    abstract public SubSpaceSplitIterator<T> iteratorAcross(int[] dimensions_);
    
    /**
     * @return The max reduction for this mappable
     */
    abstract public Reduction<T, RuntimeException> getMaxOp();
	
    /**
     * @return The min reduction for this mappable
     */
    abstract public Reduction<T, RuntimeException> getMinOp();
	
    /**
     * @return The sum reduction for this mappable
     */
    abstract public Reduction<T, RuntimeException> getSumOp();
	
    /**
     * Create an array that has the dimensions not listed from this array
     * 
     * @param dimensions_ The dimensions to ignore
     * 
     * @return An array with the same dimensions as this array except for the dimensions passed, which are dropped
     */
    abstract protected MultiDimensionalArray<T,?> createOtherDimsArray(int[] dimensions_);
    
    /**
     * Create an array that has the dimensions not listed from this array, and fill it with the provided values
     * 
     * @param dimensions_ The dimensions to ignore
     * @param array_ The values to fill the resulting array with
     * 
     * @return An array with the same dimensions as this array except for the dimensions passed, which are dropped
     */
	abstract protected MultiDimensionalArray<T,?> createOtherDimsArray(int[] dimensions_, T[] array_);
	
	/**
	 * @param reduction_ The reduction to perform
	 * @param currentIterator_ The iterator to perform the reduction over
	 * @param target_ The target_ mappable to fill with the result
	 * 
	 * @param <E> The type of the exception thrown by the reduction
	 * 
	 * @return Creates an action that performs the specified reduction
	 */
	protected abstract <E extends Exception> ComputableRecursiveAction createReduceAction(
			ReductionBase<T, ? extends Iterator<T>, E> reduction_,
			final SettableIterator<T> currentIterator_, AbstractMappable<T,?> target_);
	
    abstract protected SettableIterator<T[]> createMultiSettableIterator(SettableIterator<T>[] iters_);

    abstract protected  <E extends Exception> RM[] createPointWiseMultiValuedResult(MultiValuedNaryOp<T, E> op_,  MultiDimensionalArray<? extends T, ?>[] others_);
    
    /**
     * Manages 'across' operations that apply other arguments only in certain dimensions, but at each point of the
     * dimensions orthogonal to the across dimensions
     */
    private class AcrossOperand implements MappableReducible<T, RM, RM>
    {
        MultiDimensionalArray<? extends T, ?>[] _others;
        final int[] _dimensions;

        private AcrossOperand(int[] dimensions_)
        {
            _dimensions = dimensions_;
            
            // Check that all the across dimensions are in range
            for(int dim : dimensions_) {
            	
            	if( dim >= AbstractMappable.this.numberOfDimensions()) {
            		throw new ArrayIndexOutOfBoundsException("Dimensions " + dim +" requested, "+AbstractMappable.this.numberOfDimensions() +" dimensions available");
            	}
            }
        }


        @Override
        public MappableWithSimpleOps<T, ? extends AbstractMappable<T,?>> modifying()
        {
            return ((Acrossable<T,RM>)AbstractMappable.this.modifying()).across(_dimensions);
        }
        
        @Override
        public MappableWithSimpleOps<T, ? extends AbstractMappable<T,?>> nonModifying()
        {
            return ((Acrossable<T,RM>)AbstractMappable.this.nonModifying()).across(_dimensions);
        }

        @Override
        public <S extends T> RM multiply(MultiDimensionalArray<S, ?> other_)
        {
            _others = new MultiDimensionalArray[]{other_};
            return map(getMultOp());
        }

		@Override
        public <S extends T> RM divide(MultiDimensionalArray<S, ?> other_)
        {
            _others = new MultiDimensionalArray[]{other_};
            return map(getDivisionOp());
        }

        @Override
        public <S extends T> RM add(MultiDimensionalArray<S, ?> other_)
        {
            _others = new MultiDimensionalArray[]{other_};
            return map(getAddOp());
        }

        @Override
        public <S extends T> RM subtract(MultiDimensionalArray<S, ?> other_)
        {
            _others = new MultiDimensionalArray[]{other_};
            return map(getSubtractionOp());
        }
        
        
        @Override
        public <S extends T> Mappable<T, RM> with(MultiDimensionalArray<S, ?>... otherArrays_)
        {
            _others = otherArrays_;

            /* Wrap the AcrossOperand in an IArrayOperand to prevent access to the ISingleOperand functions
             */
            return new Mappable<T, RM>()
            {
                @Override
                public <V extends T> Mappable<T, RM> with(MultiDimensionalArray<V, ?>... otherArrays_)
                {
                    // Add the new arrays to the already added ones
                    _others = (MultiDimensionalArray<? extends T, ?>[]) ArrayUtils.addAll(_others, otherArrays_);

                    // Return self
                    return this;
                }

                @Override
                public <E extends Exception> RM map(NaryOp<T, T, E> operation_) throws E
                {
                    // Call the outer operand's map operation
                    return AbstractMappable.AcrossOperand.this.map(operation_);
                }
                
                @Override
                public <E extends Exception> RM[] map(MultiValuedNaryOp<T, E> operation_) throws E
                {
                    // Call the outer operand's map operation
                    return AbstractMappable.AcrossOperand.this.map(operation_);
                }
            };
        }

        @Override
        final public <E extends Exception> RM map(NaryOp<T,T,E> operation_) throws E
        {
            RM result = getPointwiseResult();

            boolean iterateResult = !result.isModifying();

            // Get parallel iterators over the relevant dimensions
        	List<SettableIterator<T>> sourceIterators = (List<SettableIterator<T>>) AbstractMappable.this.parallelIterators(_dimensions);
            
        	// Some generics hoops to jump through...
			List<SettableIterator<?>> targetIterators = iterateResult ? (List<SettableIterator<?>>) (List<?>) ((MultiDimensionalArray<T,?>)result).parallelIterators(_dimensions) : (List<SettableIterator<?>>)(List<?>) sourceIterators;
        	
        	mapToResult(operation_, sourceIterators, targetIterators, iterateResult);
        	
            return result;
        }
        
        @Override
        final public <E extends Exception> RM[] map(MultiValuedNaryOp<T, E> operation_) throws E
        {
        	RM[] targetArrays = createPointWiseMultiValuedResult(operation_, _others);

            List<SettableIterator<T>>[] resultIters = new List[targetArrays.length];
            
            for (int i = 0; i < targetArrays.length; i++)
			{
            	resultIters[i] = (List<SettableIterator<T>>) targetArrays[i].parallelIterators(_dimensions);
			}
            
            List<SettableIterator<?>> targetIterators = new ArrayList<SettableIterator<?>>();
            
            for(int i = 0; i < resultIters[0].size(); i++) 
            {
            	SettableIterator<T>[] iters = new SettableIterator[targetArrays.length];
            	
            	for(int j = 0; j < resultIters.length; j++)
            	{
            		iters[j] = resultIters[j].get(i);
            	}
            	
            	targetIterators.add(createMultiSettableIterator(iters));
            }
            
            // Get parallel iterators over the relevant dimensions
        	List<SettableIterator<T>> sourceIterators = (List<SettableIterator<T>>) AbstractMappable.this.parallelIterators(_dimensions);
        	
            mapToResult(operation_, sourceIterators, targetIterators, true);
        	
            return  (RM[]) targetArrays;
        }


		protected <E extends Exception> void mapToResult(NaryOp<T, ?, E> operation_, List<SettableIterator<T>> sourceIterators, List<SettableIterator<?>> targetIterators, boolean iterateResult_)
        {
        	
            List<ComputableRecursiveAction> tasks = new ArrayList<ComputableRecursiveAction>(sourceIterators.size());

            // Iterate over the orthogonal dimension, finding the max at each point
            // along the max dimensions
            for (int i = 0; i < sourceIterators.size(); i++)
            {
            	final SettableIterator<? extends T>[] inputs = new SettableIterator[_others.length + 1];
                
            	inputs[0] = sourceIterators.get(i);
            	
                for (int j = 0; j < _others.length; j++)
                {
                    inputs[j + 1] = _others[j].iterator();
                }
                
                final SettableIterator<?> targetIter = targetIterators.get(i);
				final NaryOpCallable<T, E> op = createNaryOpCallable(operation_, inputs, targetIter, iterateResult_);
                
				
                tasks.add(()->
				{
					boolean first = true;
					
					while(((ParallelIterator)inputs[0]).hasNextParallel())
					{
						((ParallelIterator)inputs[0]).nextParallel();
						
						if(iterateResult_)
						{
							((ParallelIterator)targetIter).nextParallel();
						}
						
						// Reset the other inputs if this is not the first call
						if(!first)
						{
							for (int j = 1; j < inputs.length; j++)
			                {
								inputs[j].reset();
			                }
						}
						
						op.compute();
						
						first = false;
					}
				});
                
            }
            
            _executor.executeAndWait(tasks);
        }

		@Override
		public <E extends Exception> RM reduce(ReductionBase<T,? extends Iterator<T>, E> reduction_) throws E
		{
			
			
			if(reduction_ instanceof Reduction)
			{
				final List<ComputableRecursiveAction> tasks;
				List<SettableIterator<T>> sourceIterators = (List<SettableIterator<T>>) AbstractMappable.this.parallelIterators(_dimensions);
	
				tasks = new ArrayList<ComputableRecursiveAction>(sourceIterators.size());
				
				final RM resultArray = (RM) AbstractMappable.this.createOtherDimsArray(_dimensions);
				
				for (int i = 0; i < sourceIterators.size(); i++)
				{
					final SettableIterator<T> currentIterator = sourceIterators.get(i);
					tasks.add(createReduceAction(reduction_, currentIterator, resultArray));
				}	
				Timer timer = new Timer();
				Stoppable stoppable = timer.start("execReduceAcross");
				
				_executor.executeAndWait(tasks);
				 
				stoppable.stop();
				 
				return resultArray;
			}
			else
			{
				ArrayList<ComputableRecursiveTask<T>> tasks = new ArrayList<ComputableRecursiveTask<T>>();
				
				IndexedReduction<T, E> reduction = (IndexedReduction<T, E>)reduction_;
				
				SubSpaceSplitIterator<T> dimIter = iteratorAcross(_dimensions);
				
				SubSpaceSplitIterator<T> orthogonalIterator = dimIter.getOrthogonalIterator();
				
				while(orthogonalIterator.hasNext())
				{
					orthogonalIterator.next();
					
					final SubSpaceSplitIterator<T> currentIterator = orthogonalIterator.getOrthogonalIterator();
					
					tasks.add(()->{
						return reduction.perform(currentIterator);
					});
				}
				
				List<T> results;
				Timer timer = new Timer();
				Stoppable stoppable = timer.start("execReduceAcrossIndexed");
				results = new CurrentThreadExecutor().executeAndGet(tasks);				
				stoppable.stop();
				return (RM) AbstractMappable.this.createOtherDimsArray(_dimensions,(T[])results.toArray());
			}			
		}


		


		@Override
		public RM max()
		{
			return reduce(getMaxOp());
		}


		@Override
		public RM sum()
		{
			return reduce(getSumOp());
		}


		@Override
		public RM min()
		{
			return reduce(getMinOp());
		}
    }
}
