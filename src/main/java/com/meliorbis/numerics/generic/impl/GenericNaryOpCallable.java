package com.meliorbis.numerics.generic.impl;

import com.meliorbis.numerics.generic.MultiValuedNaryOp;
import com.meliorbis.numerics.generic.NaryOp;
import com.meliorbis.numerics.generic.SettableIterator;

/**
 */
public abstract class GenericNaryOpCallable<T, E extends Exception> extends NaryOpCallable<T,E>
{
    private final T[] _currentVals;

    public GenericNaryOpCallable(NaryOp<T,?, E> op_, SettableIterator<? extends T>[] inputs_,
                                 SettableIterator<?> result_, boolean iterateResult_)
    {
        super(op_, inputs_, result_, iterateResult_);

        _currentVals = createTypedArray(inputs_.length);
    }

    abstract protected T[] createTypedArray(int length_);

    @SuppressWarnings("unchecked")
	@Override
    protected void computeNextStep() throws E
    {
        for (int i = 0; i < _iterators.length; i++) {
            _currentVals[i] = _iterators[i].next();
        }

        if(_iterateResult) 
        {
        	_resultIter.next();
        }
        
        if(_op instanceof MultiValuedNaryOp) 
        {
        	((SettableIterator<T[]>)_resultIter).set((T[])_op.perform(_currentVals));
        }
        else
        {
        	((SettableIterator<T>)_resultIter).set((T)_op.perform(_currentVals));	
        }
    }
}
