/**
 * 
 */
package com.meliorbis.numerics.generic.impl;

import com.meliorbis.numerics.generic.SubSpaceSplitIterator;

/**
 * Interface for reduction operations which also require access to the current index
 * 
 * @author Tobias Grasl
 * 
 * @param <T> The type iterated over and returned
 * @param <E> The Exception that may be thrown by the operation
 */
public interface IndexedReduction<T, E extends Exception> extends ReductionBase<T, SubSpaceSplitIterator<T>, E>
{
}
