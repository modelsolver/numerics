package com.meliorbis.numerics.generic.impl;

import com.meliorbis.numerics.generic.SubSpaceSplitIterator;

/**
 * An operation that works across some dimensions, successively
 * performed across the orthogonal ones.
 * 
 * @author Tobias Grasl
 *
 * @param <T> The type of array this operation operates on
 */
public interface IndexedSubSpaceOperation<T, E extends Exception> extends IteratedOperation<T, E>
{
	default void initialise(SubSpaceSplitIterator<T> orthogonal_) throws E
    {
        // Do nothing by default
    }
}