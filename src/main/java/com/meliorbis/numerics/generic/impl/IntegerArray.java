package com.meliorbis.numerics.generic.impl;

import java.util.Comparator;

import com.meliorbis.numerics.generic.ArrayFactory1D;
import com.meliorbis.numerics.generic.ArrayFactory2D;
import com.meliorbis.numerics.generic.BinaryOp;
import com.meliorbis.numerics.index.SubIndex;
import com.meliorbis.numerics.threading.Executor;

/**
 * Specialisation of GenericBlockedArray for Integer objects
 *
 * @author Tobias Grasl
 */
public class IntegerArray extends GenericBlockedArray<Integer, IntegerArray> implements com.meliorbis.numerics.generic.IntegerArray<IntegerArray>
{
    public IntegerArray(Executor _executor, int[] dimensions_)
    {
        super(_executor, dimensions_);
    }

    public IntegerArray(BlockedArrayData<Integer> data_, SubIndex dimensionCounter_, Executor executor_)
    {
        super(data_, dimensionCounter_, executor_);
    }

    @Override
    protected IntegerArray createSub(BlockedArrayData<Integer> data_, SubIndex dimensionCounter_)
    {
        return new IntegerArray(data_, dimensionCounter_, _executor);
    }

    @Override
    protected ArrayFactory2D<Integer> getArrayFactory2D()
    {
        return length -> new Integer[length][];
    }

    @Override
    protected ArrayFactory1D<Integer> getArrayFactory1D()
    {
        return length -> new Integer[length];
    }

    @Override
    protected IntegerArray createNew(int... dimensions_)
    {
        return new IntegerArray(_executor, dimensions_);
    }

    @Override
    protected Integer getZero()
    {
        return 0;
    }

    @Override
    protected Integer getMinusOne()
    {
        return -1;
    }

    @Override
    public BinaryOp<Integer, RuntimeException> getAddOp()
    {
        return (a,b) -> a + b;
    }

    @Override
    public BinaryOp<Integer, RuntimeException> getSubtractionOp()
    {
        return (a,b) -> a - b;
    }

    @Override
    public BinaryOp<Integer, RuntimeException> getMultOp()
    {
        return (a,b) -> a * b;
    }

    @Override
    public BinaryOp<Integer, RuntimeException> getDivisionOp()
    {
        return (a,b) -> a / b;
    }

    @Override
    public Comparator<Integer> getComparator()
    {
        return (a, b) -> a == null ? -1 : (b == null ? 1 : a.compareTo(b)) ;
    }
}
