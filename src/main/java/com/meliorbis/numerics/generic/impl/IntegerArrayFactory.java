package com.meliorbis.numerics.generic.impl;

import java.util.List;

import com.meliorbis.numerics.Numerics;
import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.MultiDimensionalArrayFactory;
import com.meliorbis.utils.Utils;

/**
 * @author Tobias Grasl
 */
public class IntegerArrayFactory implements MultiDimensionalArrayFactory<Integer, IntegerArray>
{
    private final Numerics<?> _numerics;

    public IntegerArrayFactory(Numerics<?> numerics_)
    {
        _numerics = numerics_;
    }

    @Override
    public IntegerArray newArray(int... dimensions_)
    {
        return new IntegerArray(_numerics.getExecutor(), dimensions_);
    }

    @Override
    public IntegerArray newArray(Integer... data_)
    {
        return new IntegerArray(_numerics.getExecutor(), new int[]{data_.length}).fill(data_);
    }

    @Override
    public Integer fromString(String valueString_) throws NumericsException
    {
        return Integer.valueOf(valueString_);
    }

    @Override
    public IntegerArray newArray(List<Integer> dimensions_)
    {
        return newArray(Utils.toIntArray(dimensions_));
    }
}
