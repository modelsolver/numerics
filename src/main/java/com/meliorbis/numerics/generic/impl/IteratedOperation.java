package com.meliorbis.numerics.generic.impl;

import com.meliorbis.numerics.generic.SettableIterator;

/**
 * An operation performed on an iterator
 * 
 * @param <T> The type of values the operation supports
 * @param <E> The type of exception the operation throws
 */
public interface IteratedOperation<T, E extends Exception>
{
    /**
     * Perform the operation in on the provided iterator
     *
     * @param iterator_ The iterator on which to perform the operation
     *
     * @throws E If an exception occurs
     */
    void perform(SettableIterator<T> iterator_) throws E;
}
