package com.meliorbis.numerics.generic.impl;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.NaryOp;
import com.meliorbis.numerics.generic.SettableIterator;
import com.meliorbis.numerics.threading.ComputableRecursiveAction;

/**
 * Abstract Base class for callables that perform an nary op at each point on some iterators
 * 
 * @param T The input type, probably a numeric
 * @param E The type of exception thrown when things vo 
 */
public abstract class NaryOpCallable<T, E extends Exception> implements ComputableRecursiveAction
{
    protected final NaryOp<T, ?, E> _op;

    protected final SettableIterator<?> _resultIter;
    protected final boolean _iterateResult;

    protected final SettableIterator<? extends T>[] _iterators;

    public NaryOpCallable(NaryOp<T, ?, E> op_,
                          SettableIterator<? extends T>[] inputs_,
                          SettableIterator<?> result_,
                          boolean iterateResult_)
    {
        _op = op_;
        _iterators = inputs_;
        _resultIter = result_;
        _iterateResult = iterateResult_;
    }

    @Override
    public void compute()
    {
        try
        {
            while (_iterators[0].hasNext())
            {
                computeNextStep();
            }
        }
        catch(Exception ex_)
        {
        	throw new NumericsException(ex_);
        }
    }

    protected abstract void computeNextStep() throws E;
}
