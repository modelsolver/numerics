package com.meliorbis.numerics.generic.impl;

import com.meliorbis.numerics.generic.SettableIterator;

/**
 * Implementing classes process an iterator and return a value, thereby
 * reducing the values the iterator provides
 * 
 * @author Tobias Grasl
 * 
 * @param <T> The type iterated over and returned
 * @param <E> The Exception that may be thrown by the operation
 */
public interface Reduction<T, E extends Exception> extends ReductionBase<T, SettableIterator<T>, E>
{
    
}
