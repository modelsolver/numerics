/**
 * 
 */
package com.meliorbis.numerics.generic.impl;

import java.util.Iterator;

/**
 * Base interface for operations that reduce an iterator to a single value
 * 
 * @author Tobias Grasl
 *
 * @param <T> The type iterated over and returned
 * @param <I> The type of iterator accepted by the operation
 * @param <E> The Exception that may be thrown by the operation
 */
public interface ReductionBase<T, I extends Iterator<T>, E extends Exception>
{
	T perform(I iterator_) throws E;
}
