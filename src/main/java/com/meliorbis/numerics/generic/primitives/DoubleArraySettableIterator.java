/**
 * 
 */
package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.generic.ParallelIterator;
import com.meliorbis.numerics.generic.SettableIterator;

/**
 * A Settable iterator that gets and retrieves arrays of doubles, using underlying
 * single-double iterators
 * 
 * @author Tobias Grasl
 */
public class DoubleArraySettableIterator implements SettableIterator<Double[]>, ParallelIterator
{
	private final SettableIterator<Double>[] _underlying;

	private double[] _last;
	
	/**
	 * @param underlying_ The underlying iterators to obtain data from and set it to
	 */
	public DoubleArraySettableIterator(SettableIterator<Double>[] underlying_)
	{
		_underlying = underlying_;
	}

	@Override
	public boolean hasNext()
	{
		return _underlying[0].hasNext();
	}

	@Override
	public Double[] next()
	{
		throw new UnsupportedOperationException("Call nextDoubles instead");
	}
	
	public double[] nextDoubles()
	{
		double[] vals = new double[_underlying.length];
		
		for (int i = 0; i < vals.length; i++)
		{
			vals[i] = ((DoubleSettableIterator)_underlying[i]).nextDouble();
		}
		
		_last = vals;
		
		return vals;
	}

	@Override
	public Double[] get()
	{
		throw new UnsupportedOperationException("Call getDoubles instead");
	}

	public double[] getDoubles()
	{
		return _last;
	}

	@Override
	public void set(Double[] val_)
	{
		throw new UnsupportedOperationException("Call set(double[]) instead");
	}

	public void set(double[] val_)
	{
		assert _underlying.length == val_.length;
		
		for (int i = 0; i < val_.length; i++)
		{
			((DoubleSettableIterator)_underlying[i]).set(val_[i]);
		}
	}

	@Override
	public void reset()
	{
		for (int i = 0; i < _underlying.length; i++)
		{
			_underlying[i].reset();
		}
	}

	@Override
	public boolean hasNextParallel()
	{
		return _underlying[0] instanceof ParallelIterator && ((ParallelIterator)_underlying[0]).hasNextParallel() ;
	}

	@Override
	public int nextParallel()
	{
		for (int i = 1; i < _underlying.length; i++)
		{
			((ParallelIterator)_underlying[i]).nextParallel();
		}
		
		return ((ParallelIterator)_underlying[0]).nextParallel();
	}

	@Override
	public int parallelCount()
	{
		return ((ParallelIterator)_underlying[0]).parallelCount();
	}

}
