package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.generic.BinaryOp;

public interface DoubleBinaryOp<E extends Exception> extends DoubleNaryOp<E>, BinaryOp<Double, E>
{
	@Override
	public default double perform(double... inputs_) throws E
	{
		return perform(inputs_[0], inputs_[1]);
	}

    /* (non-Javadoc)
	 * @see com.meliorbis.numerics.generic.NaryOp#perform(T[], int)
	 */
    @Override
    default public Double perform(Double... inputs_) throws E
    {
        return perform(inputs_[0].doubleValue(), inputs_[1].doubleValue());
    }

    @Override
    default public Double perform(Double left_, Double right_) throws E
    {
        return perform(left_, right_);
    }

	public double perform(double left_, double right_) throws E;
}