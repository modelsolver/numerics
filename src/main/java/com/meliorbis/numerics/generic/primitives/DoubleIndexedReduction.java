package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.SubSpaceSplitIterator;
import com.meliorbis.numerics.generic.impl.IndexedReduction;

public interface DoubleIndexedReduction<E extends Exception> extends IndexedReduction<Double, E>
{
	double perform(DoubleSubspaceSplitIterator iterator_) throws E;
	
	@Override
	default Double perform(SubSpaceSplitIterator<Double> iterator_) throws E
	{
		if(!(iterator_ instanceof DoubleSubspaceSplitIterator))
		{
			throw new NumericsException("Calling primitive reduction with non-primitive iterator");
		}
		
		return perform((DoubleSubspaceSplitIterator)iterator_);
	}
}
