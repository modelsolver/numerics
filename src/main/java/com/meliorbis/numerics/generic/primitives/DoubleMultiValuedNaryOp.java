/**
 * 
 */
package com.meliorbis.numerics.generic.primitives;

import org.apache.commons.lang.ArrayUtils;

import com.meliorbis.numerics.generic.MultiValuedNaryOp;

/**
 * @author Tobias Grasl
 */
@FunctionalInterface
public interface DoubleMultiValuedNaryOp<E extends Exception> extends MultiValuedNaryOp<Double, E>
{
	/**
     * Perform the operation on the provided inputs and return the result
     *
     * @param inputs_ The inputs to perform the operation on
     *
     * @return The result of the operation
     * 
     * @throws E If the operation fails
     */
    default Double[] perform(Double... inputs_) throws E
    {
        // TODO: Might want to put a warning here to ensure it is not called inadvertently
        return ArrayUtils.toObject(perform(ArrayUtils.toPrimitive(inputs_)));
    }
    
    double[] perform(double... inputs_) throws E;
}
