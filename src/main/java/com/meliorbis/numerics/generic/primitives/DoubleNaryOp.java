package com.meliorbis.numerics.generic.primitives;

import org.apache.commons.lang.ArrayUtils;

import com.meliorbis.numerics.generic.NaryOp;

/**
 * Specialised Operator class for primitive double
 * 
 * @param <E> The type of exception thrown by the operation
 */
public interface DoubleNaryOp<E extends Exception> extends NaryOp<Double, Double, E>
{
    /**
     * Perform the operation on the provided inputs and return the result
     *
     * @param inputs_ The inputs to perform the operation on
     *
     * @return The result of the operation
     * 
     * @throws E If the operation fails
     */
    default Double perform(Double... inputs_) throws E
    {
        // TODO: Might want to put a warning here to ensure it is not called inadvertently
        return perform(ArrayUtils.toPrimitive(inputs_));
    }

    /**
     * Perform the operation on the provided inputs and return the result
     *
     * @param inputs_ The inputs to perform the operation on
     *
     * @return The result of the operation
     * 
     * @throws E If the operation fails
     */
    double perform(double... inputs_) throws E;
}
