/**
 * 
 */
package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.SettableIterator;
import com.meliorbis.numerics.generic.impl.Reduction;

/**
 * Primitive double specialisation of IReduction
 * 
 * @author Tobias Grasl
 */
public interface DoubleReduction<E extends Exception> extends Reduction<Double, E>
{
	double perform(DoubleSettableIterator iterator_) throws E;
	
	default Double perform(SettableIterator<Double> iterator_) throws E
	{
		if(!(iterator_ instanceof DoubleSettableIterator))
		{
			throw new NumericsException("Calling primitive reduction with non-primitive iterator");
		}
		
		return perform((DoubleSettableIterator)iterator_);
	}
}
