/**
 * 
 */
package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.generic.SettableIndexedIterator;

/**
 * Specialisation of ISettableIndexedIterator for primitive doubles
 * 
 * @author Tobias Grasl
 */
public interface DoubleSettableIndexedIterator extends SettableIndexedIterator<Double>, DoubleSettableIterator
{

}
