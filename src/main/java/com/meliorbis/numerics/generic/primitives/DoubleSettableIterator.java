package com.meliorbis.numerics.generic.primitives;

import java.util.PrimitiveIterator;

import com.meliorbis.numerics.generic.SettableIterator;

/**
 * Specialisation of ISetableIterator for primitive doubles
 */
public interface DoubleSettableIterator extends SettableIterator<Double>, PrimitiveIterator.OfDouble
{
   /**
     * Sets the current position's value to be the provided one
     *
     * @param val_ The new value to set
     */
    public void set(double val_);
}
