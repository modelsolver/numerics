/**
 * 
 */
package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.generic.SubSpaceSplitIterator;

/**
 * Specialisation of ISubspaceSlitIterator for primitive doubles
 * 
 * @author Tobias Grasl
 */
public interface DoubleSubspaceSplitIterator extends DoubleSettableIndexedIterator, SubSpaceSplitIterator<Double>
{
	/**
	 * Return type conversion
	 */
	DoubleSubspaceSplitIterator getOrthogonalIterator();
}
