/**
 * 
 */
package com.meliorbis.numerics.generic.primitives;

import com.meliorbis.numerics.generic.UnaryOp;

/**
 * Performs unary operations on double
 * 
 * @author Tobias Grasl
 */
public interface DoubleUnaryOp<E extends Exception> extends DoubleNaryOp<E>, UnaryOp<Double, E>
{
	public double perform(double input_);

    @Override
    default public Double perform(Double input_)
    {
        return perform(input_.doubleValue());
    }

	/* (non-Javadoc)
	 * @see com.meliorbis.numerics.generic.NaryOp#perform(T[], int)
	 */
	@Override
	default public double perform(double... inputs_) throws E
	{
		return perform(inputs_[0]);
	}

    /* (non-Javadoc)
	 * @see com.meliorbis.numerics.generic.NaryOp#perform(T[], int)
	 */
    @Override
    default public Double perform(Double... inputs_) throws E
    {
        return perform(inputs_[0].doubleValue());
    }
}
