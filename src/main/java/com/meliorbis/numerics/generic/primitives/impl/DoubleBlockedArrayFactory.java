/**
 * 
 */
package com.meliorbis.numerics.generic.primitives.impl;

import java.util.List;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.MultiDimensionalArrayFactory;
import com.meliorbis.numerics.threading.Executor;
import com.meliorbis.utils.Utils;

/**
 * Factory that creates double arrays based on the blocked array pattern
 * 
 * @author Tobias Grasl
 */
public class DoubleBlockedArrayFactory implements
		MultiDimensionalArrayFactory<Double, DoubleArray>
{
	private final Executor _executor;

	public DoubleBlockedArrayFactory(Executor executor_)
	{
		_executor = executor_;
	}

	@Override
	public DoubleArray newArray(int... dimensions_)
	{
		return new DoubleArray(_executor, dimensions_);
	}

	@Override
	public DoubleArray newArray(Double... data_)
	{
		return new DoubleArray(data_, _executor);
	}
	
	public DoubleArray newArray(double... data_)
	{
		return new DoubleArray(data_, _executor);
	}

	@Override
	public Double fromString(String valueString_) throws NumericsException
	{
		try
		{
			return Double.valueOf(valueString_);
		} catch (NumberFormatException e)
		{
			throw new NumericsException("Badly Formed Number in CSV File");
		}
	}

	@Override
	public DoubleArray newArray(List<Integer> dimensions_) {
		
		return newArray(Utils.toIntArray(dimensions_));
	}
}
