package com.meliorbis.numerics.generic.primitives.impl;

import com.meliorbis.numerics.generic.NaryOp;
import com.meliorbis.numerics.generic.SettableIterator;
import com.meliorbis.numerics.generic.impl.NaryOpCallable;
import com.meliorbis.numerics.generic.primitives.DoubleArraySettableIterator;
import com.meliorbis.numerics.generic.primitives.DoubleMultiValuedNaryOp;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIterator;

/**
 * Specialisation of NaryOpCallable for primitive double arrays
 */
public class DoubleMultiValuedNaryOpCallable<E extends Exception> extends NaryOpCallable<Double, E>
{
    private final double[] _currentVals;

	public DoubleMultiValuedNaryOpCallable(DoubleMultiValuedNaryOp<E> op_, SettableIterator<? extends Double>[] inputs_, SettableIterator<?> result_,
                                boolean iterateResult_)
    {
        super((NaryOp<Double, ?, E>)op_, inputs_, result_, iterateResult_);

        _currentVals = new double[inputs_.length];
    }

	@SuppressWarnings("unchecked")
	@Override
    protected void computeNextStep() throws E
    {
        for (int i = 0; i < _iterators.length; i++) {
            _currentVals[i] = ((DoubleSettableIterator)_iterators[i]).nextDouble();
        }

        // Only iterate the result if requested (because the result could be the same as one of the inputs and hence be
        // already iterated
        if(_iterateResult)
        {
            ((DoubleArraySettableIterator)_resultIter).nextDoubles();
        }

        ((DoubleArraySettableIterator)_resultIter).set(((DoubleMultiValuedNaryOp<E>)_op).perform(_currentVals));
    }
}
