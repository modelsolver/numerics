package com.meliorbis.numerics.index;

public interface Index
{
	/**
	 * @return The dimension sizes
	 */
	public int[] getSizes();

	/**
	 * @return The total number of elements in the space
	 */
	public int numberOfElements();

	/**
	 * @return The number of dimensions in the space
	 */
	public int numberOfDimensions();

	public int toLinearIndex(int... logicalIndex_)
			throws IndexOutOfBoundsException;

	public int[] toLogicalIndex(int linearIndex_);

	public IndexIterator iterator();

    /**
     * Provides an iterator over the given range of linear indexes. to_ must be greater than or equal to from_
     *
     * @param from_ The linear index to start at (inclusive)
     * @param to_ The linear index to end at (exclusive). If to_ is greater than or equal to the number of elements in
     *            the index, the iterator will terminate at the end of the index
     *
     * @return An iterator that will return the requested range of elements in sequence
     */
    public IndexIterator rangeIterator(int from_, int to_);

    public SubIndexIterator iterator(int... dimensions_);

	/**
	 * Returns an iterator to iterate over all the points at the indicates index
	 * 
	 * @param index_ The index to iterate at; unset dimnesions are indicates with a -1 or dropped if at the end
	 * 
	 * @return  An iterator over the orthogonal dimensions at the specified index
	 */
	public SubIndexIterator iteratorAt(int[] index_);

	public SubIndex subIndexAt(int[] atPosition_);

	public Index nonSubbed();

	public Index transpose(int dimA_, int dimB_);

	/**
	 * Returns the index but with the source dimensions in the specified order
	 * 
	 * @param order_ The new order, e.g. [3 2 1] would reverse the order of a three d array
	 * 
	 * @return The subindex in the new order
	 */
	public Index reorder(int[] order_);

	/**
	 * Creates a number of iterators to iterate over the dimensions given in parallel. Each iterator
	 * returned will cover a different range in the other dimensions and repeatedly iterate over dimensions_
	 * within that range.
	 * 
	 * The maxCount_ specifies into how many iterators at most to split the operation. If maxCount_ is greater than
	 * the number of points in other dimensions, the number of iterators will equal the latter number. Otherwise, maxCount_
	 * iterators are returned
	 * 
	 * @param maxCount_ The maximum number of iterators to return
	 * @param dimensions_ The dimensions to iterator over
	 * 
	 * @return An array of iterators as described
	 */
	public SkippingLinearIterator[] parallelIterators(int maxCount_, int... dimensions_);
}