package com.meliorbis.numerics.index;

import java.util.PrimitiveIterator;

/**
 * Iterator over an index. The value provided by next is the linear index, but it also provides
 * access to the current index 
 * 
 * @author Tobias Grasl
 *
 */
public interface IndexIterator extends PrimitiveIterator.OfInt
{
	/**
	 * @return the currentIndex
	 */
	public int[] getCurrentIndex();

	/**
	 * Resets the iterator to its pre-start position
	 */
	public void reset();

}