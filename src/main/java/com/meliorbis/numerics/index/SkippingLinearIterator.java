package com.meliorbis.numerics.index;

import java.util.PrimitiveIterator;

/**
 * This type of iterator iterates over a (sub-)index but does not provide access to the index directly,
 * only to the linear index
 */
public interface SkippingLinearIterator extends PrimitiveIterator.OfInt
{
    /**
     * The current full linear index. If this iterator is iterating the full index,
     * this will be equal to the value last returned by next. Otherwise, the appropriate
     * linear index within the full index of the current position within the subindex
     *
     * @return The linear index of the current position in the full index
     */
    int getCurrentFullLinearIndex();
    
    /**
     * Indicates that the iterator has another set of parallel points to iterate over
     * 
     * @return true if there is more to do, false otherwise
     */
    boolean hasNextParallel();
    
    /**
     * Move to the next set of parallel points
     * 
     * @return The next parallel index
     */
    int nextParallel();
    
    /**
	 * Resets the iterator to its pre-start position
	 */
	public void reset();
}
