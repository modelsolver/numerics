/**
 * 
 */
package com.meliorbis.numerics.index;

/**
 * Interface of a subindex which provides is limited by dimension over a larger index. A subindex
 * also may hold a fixed position in the other dimensions of the full index, thus it is a index
 * over the sub-index dimensions at a particular point in the other dimensions of the full index
 * 
 * @author Tobias Grasl
 */
public interface SubIndex extends Index
{
	/**
	 * @return The dimensions this index is restricted to in the full index
	 */
	public abstract int[] getDimensions();

	/**
	 * Given an array of dimensions in the sub-index, yields their respective positions in the full index
	 * 
	 * @param dimensions_ The sub-index dimensions requested
	 * 
	 * @return The corresponding dimensions in the full index
	 */
	public abstract int[] subDimensionsInFullIndex(int... dimensions_);

	/**
	 * Retrieves the dimensions in the full index that this subindex does not include
	 * 
	 * @return An array with the full index dimensions not included in this subindex
	 */
	public abstract int[] otherDimensions();

	/**
	 * Given a position in the full index, retrieves the parts which are in the subindex
	 * 
	 * @param fullIndex_ The coordinates in the full index to be filtered
	 * 
	 * @return The coordinates which are part of the subindex
	 */
	public abstract int[] subIndex(int[] fullIndex_);

	/**
	 * @param subIndex_ The coordinates in the subindex
	 * 
	 * @return The full-index coordinates of the given point in the subindex
	 */
	public abstract int[] fullIndex(int[] subIndex_);
	
	/**
	 * Overridden to provide appropriate return type
	 */
	@Override
	public SubIndexIterator rangeIterator(int from_, int to_);
}
