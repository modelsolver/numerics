/**
 * 
 */
package com.meliorbis.numerics.index;


/**
 * Iterator over parts of an index, restricted by dimension. Note this iterator also keeps has values in the
 * other dimensions, and can give access to the index in those other dimensions or across the full index.
 * 
 * @author Tobias Grasl
 */
public interface SubIndexIterator extends IndexIterator, SkippingLinearIterator
{
	/**
	 * @return A iterator that iterates over the dimensions not iterated by this, at the current point of this
	 */
	public SubIndexIterator getOrthogonalIterator();
	
	/**
	 * @return The index in the dimensions other than the ones that this iterator is iterating over
	 */
	public int[] getOtherIndex();
	
	/**
	 * @return The full index over all dimensions, both those being iterated and those not being iterated
	 */
	public int[] getCurrentFullIndex();
}
