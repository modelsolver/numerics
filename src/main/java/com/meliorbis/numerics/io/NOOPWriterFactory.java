package com.meliorbis.numerics.io;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.meliorbis.numerics.generic.MultiDimensionalArray;

/**
 * Writer factory for writers that write nothing. For testing
 * 
 * @author Tobias Grasl
 */
public class NOOPWriterFactory implements NumericsWriterFactory
{
    private static final NumericsWriter NOOP_WRITER = new NumericsWriter(){

        @Override
        public void writeArray(String name_, MultiDimensionalArray<? extends Number, ?> array_) throws IOException
        {

        }

        @Override
        public void writeArrays(Map<String, MultiDimensionalArray<? extends Number, ?>> arrays_) throws IOException
        {

        }

        @Override
        public void writeStructure(String name_, Map<String, MultiDimensionalArray<? extends Number, ?>> arrays_) throws IOException
        {

        }

        @Override
        public void close() throws IOException
        {

        }
    };

    @Override
    public NumericsWriter create(File file_)
    {
        return NOOP_WRITER;
    }

    @Override
	public String defaultExtension()
	{
		return "";
	}   
}
