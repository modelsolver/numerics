package com.meliorbis.numerics.io;

import java.io.File;

/**
 * Creates input/output classes for reading or writing from a backing store
 *
 * @author Tobias Grasl
 */
public interface NumericsIOFactory<T>
{
    /**
     * By default, creates a File object using the path and calls the File-based method
     *
     * @param path_ The path to/from which IO is to be performed
     *
     * @return A reader or writer at the specified path
     */
    default T create(String path_) {
        return create(new File(path_));
    };

    /**
     * @param file_ The path to/from which IO is to be performed
     *
     * @return A reader or writer at the specified path
     */
    T create(File file_);
    
    /**
     * @return The default file extension to apply
     */
    String defaultExtension();
}
