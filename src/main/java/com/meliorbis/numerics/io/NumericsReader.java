package com.meliorbis.numerics.io;

import java.io.IOException;
import java.util.Map;

import com.meliorbis.numerics.generic.MultiDimensionalArray;

/**
 * The reader interface of reading structures consisting of multiple arrays
 *
 * @author Tobias Grasl
 */
public interface NumericsReader
{
    /**
     * Reads a single array with the given name
     *
     * @param name_ The name of the array
     * @param <T> The type of the array
     *
     * @return The named array
     *
     * @throws IOException If an error occurs
     */
    <T extends Number> MultiDimensionalArray<T, ?> getArray(String name_) throws IOException;

    /**
     * Retrieves all arrays, in a map by name
     *
     * @return The map of arrays by name
     *
     * @throws IOException If an error occurs
     */
    Map<String, MultiDimensionalArray<? extends Number, ?>> getArrays() throws IOException;

    /**
     * Reads a structur consisting of multiple arrays, which are returned in a map by name
     *
     * @param name_ The name of the structure
     *
     * @return The map of arrays in the structure
     *
     * @throws IOException If an error occurs
     */
    Map<String,MultiDimensionalArray<? extends Number, ?>> getStruct(String name_) throws IOException;
}
