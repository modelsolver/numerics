package com.meliorbis.numerics.io;

/**
 * Provides objects that can read numerics data from a backing store
 *
 * @author Tobias Grasl
 */
public interface NumericsReaderFactory<T> extends NumericsIOFactory<T>
{
}
