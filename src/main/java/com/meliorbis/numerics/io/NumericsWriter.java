package com.meliorbis.numerics.io;

import java.io.IOException;
import java.util.Map;

import com.meliorbis.numerics.generic.MultiDimensionalArray;

/**
 * Implementing classes can write arrays and named collections of arrays to a backing store
 *
 * @author Tobias Grasl
 */
public interface NumericsWriter
{
    /**
     * Write a single array with the provided name
     *
     * @param name_ The name under which the array should be stored
     * @param array_ The array itself
     *
     * @throws IOException If an error occurs
     */
    void writeArray(String name_, MultiDimensionalArray<? extends Number, ?> array_) throws IOException;

    /**
     * Write multiple arrays under the names they are mapped to
     *
     * @param arrays_ A map of arrays to write by name, all of which will be written
     *
     * @throws IOException If an error occurs
     */
    void writeArrays(Map<String, MultiDimensionalArray<? extends Number, ?>> arrays_) throws IOException;

    /**
     * Writes multiple arrays nested with an structure, which will be stored under the name provided
     *
     * @param name_ The name of the struture
     * @param arrays_ The map of arrays to write by name
     *
     * @throws IOException If an error occurs
     */
    void writeStructure(String name_, Map<String, MultiDimensionalArray<? extends Number, ?>> arrays_) throws IOException;

    /**
     * Complete writing and perform any necessary final operations and cleanup
     *
     * @throws IOException If an error occurs
     */
    void close() throws IOException;
}
