package com.meliorbis.numerics.io;

/**
 * Implementing classes provide writer classes which can be used to read from a backing store
 *
 * @author Tobias Grasl
 */
public interface NumericsWriterFactory extends NumericsIOFactory<NumericsWriter>
{
}
