package com.meliorbis.numerics.io.csv;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.meliorbis.numerics.Numerics;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.io.NumericsReader;

/**
 * A reader implementation that reads the data from a specified directory, with structures assumed to be subdirectories
 * and arrays assumed to be CSV files
 *
 * @author Tobias Grasl
 */
public class CSVReader implements NumericsReader
{
    private final File _dir;

    /**
     * Constructs a reader that reads from the directory at the specified path, using the provided numerics library
     *
     * @param path_ Path to directory containing the files
     */
    public CSVReader(String path_)
    {
        this(new File(path_));
    }

    /**
     * Constructs a reader that reads from the directory specified, using the provided numerics library
     *
     * @param dir_ Directory containing the files
     */
    public CSVReader(File dir_)
    {
        _dir = dir_;
    }

    @Override
    public <T extends Number> MultiDimensionalArray<T,?> getArray(String name_) throws IOException
    {
        name_ = CSVUtil.ensureExtension(name_);

        return readArrayFromDir(name_, _dir);
    }

    private <T extends Number> MultiDimensionalArray<T,?> readArrayFromDir(String name_, File dir_)
    {
        return Numerics.instance().readFormattedCSV(new File(dir_, name_));
    }

    @Override
    public Map<String, MultiDimensionalArray<? extends Number, ?>> getArrays() throws IOException
    {
        return readMapFromDirectory(_dir);
    }

    private Map<String, MultiDimensionalArray<? extends Number, ?>> readMapFromDirectory(File dir_) throws IOException
    {
        final String[] csvFileNames = dir_.list(CSVUtil.CSV_FILTER);

        final HashMap<String, MultiDimensionalArray<? extends Number, ?>> results =
                new HashMap<String, MultiDimensionalArray<? extends Number, ?>>();

        for (String fileName : csvFileNames)
        {
            // Strip the filename in the array name to use
            results.put(fileName.substring(0,fileName.length()-4), readArrayFromDir(fileName, dir_));
        }

        return results;
    }

    @Override
    public Map<String, MultiDimensionalArray<? extends Number, ?>> getStruct(String name_) throws IOException
    {
        return readMapFromDirectory(new File(_dir,name_));
    }
}
