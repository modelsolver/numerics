package com.meliorbis.numerics.io.csv;

import java.io.File;

import com.meliorbis.numerics.io.NumericsReaderFactory;

/**
 * @author Tobias Grasl
 */
public class CSVReaderFactory implements NumericsReaderFactory<CSVReader>
{
    @Override
    public CSVReader create(File file_)
    {
        return new CSVReader(file_);
    }
    
    @Override
	public String defaultExtension()
	{
		return ".csv";
	}   
}
