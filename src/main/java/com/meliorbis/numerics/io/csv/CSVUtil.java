package com.meliorbis.numerics.io.csv;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by toby on 07/03/2014.
 */
public abstract class CSVUtil
{
    public static final FilenameFilter CSV_FILTER = new FilenameFilter()
    {
        @Override
        public boolean accept(File dir, String name)
        {
            return name.toLowerCase().endsWith(".csv");
        }
    };

    private CSVUtil(){}

    static public String ensureExtension(String name_)
    {
        return name_.toLowerCase().endsWith("csv") ? name_ : (name_ + ".csv");
    }
}
