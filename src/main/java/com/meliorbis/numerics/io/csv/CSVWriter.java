package com.meliorbis.numerics.io.csv;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.meliorbis.numerics.Numerics;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.io.NumericsWriter;

/**
 * A writer implementation that writes state consisting of multiple arrays to
 * a directory of CSV files
 *
 * @author Tobias Grasl
 */
public class CSVWriter implements NumericsWriter
{
    private final File _dir;

    /**
     * @param dir_ The directory to write the csv files to
     * @param numerics_ The numerics library to use
     */
    CSVWriter(File dir_)
    {
        _dir = dir_;
    }

    private void ensureDir(File dir_)
    {
        if(!dir_.exists())
        {
            dir_.mkdirs();
        }
        else if(!dir_.isDirectory())
        {
            throw new IllegalArgumentException("The specified paht must be a directory!");
        }
    }

    @Override
    public void writeArray(String name_, MultiDimensionalArray<? extends Number, ?> array_) throws IOException
    {
        writeArrayToDirectory(name_, array_, _dir);
    }

    private void writeArrayToDirectory(String name_, MultiDimensionalArray<? extends Number, ?> array_, File dir_)
    {
        ensureDir(dir_);
        name_ = CSVUtil.ensureExtension(name_);

        Numerics.instance().writeFormattedCSV(array_, new File(dir_, name_));
    }

    @Override
    public void writeArrays(Map<String, MultiDimensionalArray<? extends Number, ?>> arrays_) throws IOException
    {
        for (Map.Entry<String, MultiDimensionalArray<? extends Number, ?>> namedArray : arrays_.entrySet())
        {
            writeArray(namedArray.getKey(), namedArray.getValue());
        }
    }

    /**
     * Creates a subdirectory with the provided name of the structure and writes the named arrays into that subdirectory
     *
     * @param name_ The name of the structure, which will be the name of the subdirectory
     * @param arrays_ The arrays in the structure, which will be written to the subdirectory
     *
     * @throws IOException If an error occurs
     */
    @Override
    public void writeStructure(String name_, Map<String, MultiDimensionalArray<? extends Number, ?>> arrays_) throws IOException
    {
        File subDirectory = new File(_dir,name_);

        for (Map.Entry<String, MultiDimensionalArray<? extends Number, ?>> namedArray : arrays_.entrySet())
        {
            writeArrayToDirectory(namedArray.getKey(), namedArray.getValue(), subDirectory);
        }
    }

    @Override
    public void close() throws IOException
    {
        // NOTHING TO DO!
    }
}
