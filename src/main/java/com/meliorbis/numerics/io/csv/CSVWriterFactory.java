package com.meliorbis.numerics.io.csv;

import java.io.File;

import com.meliorbis.numerics.io.NumericsWriterFactory;

/**
 * @author Tobias Grasl
 */
public class CSVWriterFactory implements NumericsWriterFactory
{
   @Override
    public CSVWriter create(File file_)
    {
        return new CSVWriter(file_);
    }
   
   @Override
	public String defaultExtension()
	{
		return ".csv";
	}   
}
