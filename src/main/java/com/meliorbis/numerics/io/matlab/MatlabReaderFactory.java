package com.meliorbis.numerics.io.matlab;

import java.io.File;

import com.meliorbis.numerics.io.NumericsReaderFactory;

/**
 * @author Tobias Grasl
 */
public class MatlabReaderFactory implements NumericsReaderFactory<MatlabReader>
{
    @Override
    public MatlabReader create(File file_)
    {
        return new MatlabReader(file_);
    }
    
    @Override
	public String defaultExtension()
	{
		return ".mat";
	}   
}
