package com.meliorbis.numerics.io.matlab;

import java.io.File;

import com.meliorbis.numerics.io.NumericsWriterFactory;

/**
 * @author Tobias Grasl
 */
public class MatlabWriterFactory implements NumericsWriterFactory
{
   @Override
    public MatlabWriter create(File file_)
    {
        return new MatlabWriter(file_);
    }

	@Override
	public String defaultExtension()
	{
		return ".mat";
	}   
}
