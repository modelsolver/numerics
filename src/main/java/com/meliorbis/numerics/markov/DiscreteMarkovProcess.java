package com.meliorbis.numerics.markov;

import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Represents a discrete markov process and provides access to the discrtee levels and the
 * transition probabilities between levels
 * 
 * @author Tobias Grasl
 */
public interface DiscreteMarkovProcess
{
    DoubleArray<?> getLevels();

    DoubleArray<?> getTransitionProbabilities();
}
