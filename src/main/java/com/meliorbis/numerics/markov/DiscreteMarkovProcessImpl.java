package com.meliorbis.numerics.markov;

import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * A discrete markov process defines the discrete available levels and the matrix of transition provabilities between
 * levels
 */
public class DiscreteMarkovProcessImpl implements DiscreteMarkovProcess
{
    private final DoubleArray<?> _levels;
    private final DoubleArray<?> _transitionProbabilities;


    public DiscreteMarkovProcessImpl(DoubleArray<?> levels_, DoubleArray<?> transitionProbabilities_)
    {
        _levels = levels_;
        _transitionProbabilities = transitionProbabilities_;
    }

    /**
     * A vector of available levels the process can attain
     *
     * @return A one-dimensional array containing the levels
     */
    @Override
    public DoubleArray<?> getLevels()
    {
        return _levels;
    }

    /**
     * A matrix of transition probabilities between levels
     *
     * @return A two-dimensional array of transition probabilities
     */
    @Override
    public DoubleArray<?> getTransitionProbabilities()
    {
        return _transitionProbabilities;
    }
}
