package com.meliorbis.numerics.test;

import org.apache.commons.math3.util.Precision;

import com.meliorbis.numerics.generic.BinaryOp;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleBinaryOp;

public abstract class ArrayAssert {
	
	static final class DiffSpec
	{
		public DiffSpec(int count_, Double expected_, Double actual_)
		{
			_count = count_;
			_expected = expected_;
			_actual = actual_;
		}
		int _count;
		Double _expected;
		Double _actual;
	}
	
	static final class EqualsOp implements DoubleBinaryOp<RuntimeException>
	{
		private final double _precision;
		int _count = 0;
		DiffSpec _firstDiff = null;
		
		EqualsOp(double precision_)
		{
			_precision = precision_;
		}


		@Override
		public Double perform(Double inputA_, Double inputB_)
		{	
            return perform(inputA_.doubleValue(), inputB_.doubleValue());
		}

        @Override
        public double perform(double inputA_, double inputB_) throws RuntimeException
        {
            if(_firstDiff == null && !Precision.equals(inputA_, inputB_, _precision))
            {
                _firstDiff = new DiffSpec(_count, inputA_, inputB_);
            }
            _count++;
            return -1d;
        }


        String describeDiff()
		{
			return String.format("Failed after %s successful calls, expected %s, actual %s",_firstDiff._count,_firstDiff._expected, _firstDiff._actual);
		}
	}

	private ArrayAssert(){}
	
	@SuppressWarnings("unchecked")
	public static void assertEquals(DoubleArray<?> arrayA_, DoubleArray<?> arrayB_, final double precision_) throws MultiDimensionalArrayException
	{
		try
		{
			EqualsOp op = new EqualsOp(precision_);
			arrayA_.with(arrayB_).map(op);
			
			if(op._firstDiff != null)
			{
				org.junit.Assert.fail(op.describeDiff());
			}
		}catch(AssertionError ae)
		{
			System.out.println("Full Array");
			throw ae;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void assertEquals(double[] vals_, DoubleArray<?> arrayB_, final double precision_) throws MultiDimensionalArrayException
	{
		try
		{
			DoubleArray<?> comparator = arrayB_.copy();
			comparator.fill(vals_);
			EqualsOp op = new EqualsOp(precision_);
			comparator.with(arrayB_).map(op);
			
			if(op._firstDiff != null)
			{
				org.junit.Assert.fail(op.describeDiff());
			}
		}catch(AssertionError ae)
		{
			System.out.println("Full Array");
			throw ae;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void assertEquals(MultiDimensionalArray<Integer,?> arrayA_, MultiDimensionalArray<Integer,?> arrayB_) throws MultiDimensionalArrayException
	{
		try
		{
			arrayA_.with(arrayB_).map(new BinaryOp<Integer, RuntimeException>()
            {
                int _count = 0;


                @Override
                public Integer perform(Integer inputA_, Integer inputB_)
                {
                    org.junit.Assert.assertEquals("Failed after " + _count + " successful calls", inputA_, inputB_);
                    _count++;
                    return null;
                }

            });
		}catch(AssertionError ae)
		{
			System.out.println("Full Array");
//				System.out.println();
			throw ae;
		}
	}

}
