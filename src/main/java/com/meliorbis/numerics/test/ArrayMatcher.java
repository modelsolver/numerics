package com.meliorbis.numerics.test;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert.EqualsOp;

public final class ArrayMatcher extends BaseMatcher<DoubleArray<?>>
{
	final MultiDimensionalArray<Double, ?> _expected;
	private EqualsOp _equalsOp;

	
	public ArrayMatcher(DoubleArray<?> expected_, double allowedError_)
	{
		_equalsOp = new EqualsOp(allowedError_);
		_expected = expected_;
	}

	@Override
	public void describeTo(Description description_)
	{
		description_.appendText("Equal to the provided array");
	}
	
	@SuppressWarnings({ "unchecked"})
	@Override
	public boolean matches(Object arg0_)
	{
		if(!(arg0_ instanceof DoubleArray))
		{
			return false;
		}
		
		try
		{
			_expected.with((DoubleArray<?>)arg0_).map(_equalsOp);
		} catch (MultiDimensionalArrayException e)
		{
			throw new RuntimeException(e);
		}
		
		return _equalsOp._firstDiff == null;
	}
}
