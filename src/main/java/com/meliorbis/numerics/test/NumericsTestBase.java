package com.meliorbis.numerics.test;

import com.meliorbis.numerics.DoubleNumerics;
import com.meliorbis.numerics.Numerics;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Created by toby on 04/04/2014.
 */
public class NumericsTestBase
{
    public static final double MAX_ERROR = 1e-15;
    protected Numerics<Double> _numerics = DoubleNumerics.instance();

    protected DoubleArray<?> createArray(double... values_)
    {
        return _numerics.getArrayFactory().newArray(values_);
    }
    
    protected DoubleArray<?> createArray(int... dimensions_)
    {
        return _numerics.getArrayFactory().newArray(dimensions_);
    }
}
