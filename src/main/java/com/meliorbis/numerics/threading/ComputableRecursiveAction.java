package com.meliorbis.numerics.threading;

import java.util.concurrent.Callable;

/**
 * A Callable which always returns null and in which the 'compute' method is called from
 * call by default.
 */
public interface ComputableRecursiveAction extends Callable<Object>
{
    void compute();

	@Override
	default public Object call()
	{
		compute();
		return null;
	}
}
