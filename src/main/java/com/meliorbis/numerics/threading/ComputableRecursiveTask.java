package com.meliorbis.numerics.threading;

import java.util.concurrent.Callable;

/**
  * A Callable which produces a result in which the 'compute' method is called from
 * call by default.
 */
public interface ComputableRecursiveTask<T> extends Callable<T>
{
    T compute() throws Exception;

	@Override
	default T call() throws Exception
	{
		return compute();
	}
}
