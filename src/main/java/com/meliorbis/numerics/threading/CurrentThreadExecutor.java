package com.meliorbis.numerics.threading;

import java.util.List;
import java.util.stream.Collectors;

import com.meliorbis.numerics.NumericsException;

/**
 * Runs all tasks passed on the calling thread, i.e. singlethreaded
 * 
 * @author Tobias Grasl
 */
public class CurrentThreadExecutor implements Executor {

    @Override
    public void executeAndWait(List<? extends ComputableRecursiveAction> actions_) throws NumericsException
    {
        actions_.forEach(action -> {
            action.compute();
        });
    }

    @Override
	public <T> List<T> executeAndGet(List<? extends ComputableRecursiveTask<T>> tasks_)
			throws NumericsException {
		
    	// Holds the last exception thrown
    	final Exception[] thrown = new Exception[1];
		
    	List<T> results = tasks_.stream().map(task -> {

			try
			{
				return task.compute();
			} catch (Exception e)
			{
				thrown[0] = e;
				return null;
			}
            
        }).collect(Collectors.toList());
		
    	// If am exception was thrown, pass it on
    	if(thrown[0] != null) {
    		throw new NumericsException(thrown[0]);
    	}
    	
		return results;
	}

	@Override
	public void destroy() {
	}
}

