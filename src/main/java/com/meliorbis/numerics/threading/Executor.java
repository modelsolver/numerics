/**
 * 
 */
package com.meliorbis.numerics.threading;

import java.util.List;

/**
 * Can execute callables
 * 
 * @author Tobias Grasl
 */
public interface Executor extends Cloneable
{
	/**
	 * Executes the provided actions and waits for them all to finish before returning
	 * 
	 * @param actions_ The callables to be executed
	 */
	public void executeAndWait(List<? extends ComputableRecursiveAction> actions_);

    /**
     * Executes the provided tasks and waits for them all to finish before returning
     *
     * @param tasks_ The callables to be executed
     * @param <T> The type of the elements for which a task is to be executed
     *
     * @return The list of results from the tasks, in the order of the list of tasks
     */
    public <T> List<T> executeAndGet(List<? extends ComputableRecursiveTask<T>> tasks_);

	/**
	 * Cleans up this executor and renders it unusable
	 */
	public void destroy();
}
