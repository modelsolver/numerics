package com.meliorbis.utils;
/**
 * 
 */


import java.io.Serializable;

/**
 * A class that holds and ordered pair of arbitrary objects. It is mutable.
 * 
 * @author toby
 */
public class Pair< L, R > implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private L _left;
	private R _right;
	
	public Pair()
	{	
	}
	
	public Pair(L left_, R right_)
	{
		_left = left_;
		_right = right_;
	}
	
	public final L getLeft()
	{
		return _left;
	}

	public final void setLeft(L left_)
	{
		_left = left_;
	}

	public final R getRight()
	{
		return _right;
	}

	public final void setRight(R right_)
	{
		_right = right_;
	}

	@Override
	public String toString()
	{
		return String.format("Left: %s\nRight: %s", _left, _right);
	}
	
	
}
