function [ array ] = readFormattedCSV( fileName )
%READFORMATTEDCSV Reads a CSV file as output by the meliorbis Numerics
%java library
    fid = fopen(fileName);
    line1 = textscan(fid,'%s\n',1);
    fclose(fid);
    
    entries = textscan(line1{1}{1},'%s','Delimiter',',');
    
    entryCount = size(entries{1});
    
    sizes = zeros(entryCount(1)-1,1);
    permuteArray = zeros(1,entryCount(1)-1);
    
    for index = 1:length(sizes)
        sizes(index) = str2num(entries{1}{index+1});
        permuteArray(index) = length(sizes)-index+1;
    end
    
    inputArray = dlmread(fileName,',',1,0);
    
    if(length(sizes)>1)
	    array=permute(reshape(inputArray,(flipud(sizes))'),permuteArray);
    else
	array = inputArray;
    end
end

