function writeFormattedCSV( array, fileName )
%READFORMATTEDCSV Reads a CSV file as output by the meliorbis Numerics
%java library
    sizestr = 'java.lang.Double';
    
     for index = 1:ndims(array)
        sizestr = [sizestr ',' num2str(size(array,index))];
        permuteArray(index) = ndims(array)-index+1;
     end
    
    if(ndims(array)>1)
	    array=reshape(permute(array,permuteArray),[numel(array),1]);
    end
    
    fid = fopen(fileName,'w');
    fprintf(fid,[sizestr '\n']);
    fclose(fid);
    
    dlmwrite(fileName, array, '-append', 'precision',20)
end