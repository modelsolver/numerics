package com.meliorbis.numerics;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import org.junit.Test;

import com.meliorbis.numerics.index.IndexIterator;
import com.meliorbis.numerics.index.SkippingLinearIterator;
import com.meliorbis.numerics.index.impl.Index;
import com.meliorbis.numerics.index.impl.Index.SubSpaceSplitIterator;

public class TestMultiDimensionalIndex
{

	@Test
	public void testToLinear()
	{
		Index index = new Index(3,4,5);
		
		assertEquals(26,index.toLinearIndex(1,1,1));
	}
	
	@Test
	public void testProperties()
	{
		Index index = new Index(3,4,5);
		
		assertEquals(3,index.numberOfDimensions());
		assertEquals(60,index.numberOfElements());
		assertArrayEquals(new int[]{3,4,5}, index.getSizes());
	}

	@Test(expected=NoSuchElementException.class)
	public void testIterator()
	{
		Index index = new Index(3,4,5);
		
		IndexIterator iterator = index.iterator();
		
		assertTrue(iterator.hasNext());
		assertEquals(0,(int) iterator.nextInt());
		
		for(int i = 0; i < 24; i++)
		{
			iterator.nextInt();
		}
		
		assertEquals(25,(int)iterator.nextInt());
		assertArrayEquals(new int[]{1,1,0},iterator.getCurrentIndex());
		
		while(iterator.hasNext())
		{
			iterator.nextInt();
		}
		
		// Should throw an exception
		iterator.nextInt();
		
		fail("Should have got a NoSuchElementException");
	}

	@Test
	public void testGetOther()
	{
		Index index = new Index(3,4,5);
		
		SubSpaceSplitIterator primaryIter = index.iterator(0);
		
		ArrayList<SubSpaceSplitIterator> orths = new ArrayList<Index.SubSpaceSplitIterator>();
		
		while(primaryIter.hasNext())
		{
			primaryIter.nextInt();
			SubSpaceSplitIterator orth = primaryIter.getOrthogonalIterator();
			orths.add(orth);
			orth.getOtherIndex();
		}
		for(int orthInd = 0; orthInd < orths.size(); orthInd++)
		{
			assertEquals("Incorrect other index at position "+orthInd,orthInd, orths.get(orthInd).getOtherIndex()[0]);
		}	
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testSubSpaceSplitIterator()
	{
		Index index = new Index(3,4,5);
		
		Index.SubSpaceSplitIterator iterator = index.iterator(0,2);
		
		assertTrue(iterator.hasNext());
		assertEquals(0,(int) iterator.nextInt());
		
		SubSpaceSplitIterator perpendicularIterator = iterator.getOrthogonalIterator();
		
		int perpCount = 0;
		
		while (perpendicularIterator.hasNext())
		{
			perpendicularIterator.nextInt();
			perpCount++;
		}
		
		assertEquals(4, perpCount);
		
		int maxIndex = 0;
		
		while (iterator.hasNext())
		{
			maxIndex = iterator.nextInt();
		}
		
		assertEquals(14,maxIndex);
		
		// Should throw an exception
		iterator.nextInt();
		
		fail("Should have got a NoSuchElementException");
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testSubSpaceSplitIteratorWithStart()
	{
		Index index = new Index(3,4,5);
		
		Index.SubSpaceSplitIterator iterator = index.new SubSpaceSplitIterator(new int[]{1,-1,-1}, new int[]{0,2});
		
		assertTrue(iterator.hasNext());
		assertEquals(0,(int) iterator.nextInt());
		
		SubSpaceSplitIterator perpendicularIterator = iterator.getOrthogonalIterator();
		
		int perpCount = 0;
		
		while (perpendicularIterator.hasNext())
		{
			perpendicularIterator.nextInt();
			perpCount++;
		}
		
		assertEquals(4, perpCount);
		
		int maxIndex = 0;
		
		while (iterator.hasNext())
		{
			maxIndex = iterator.nextInt();
		}
		
		assertEquals(14,maxIndex);
		
		// Should throw an exception
		iterator.nextInt();
		
		fail("Should have got a NoSuchElementException");
	}
	
	@Test
	public void testTranspose()
	{
		Index index = new Index(3,4,5);
		Index otherIndex = new Index(5,3,4);
		
		com.meliorbis.numerics.index.Index transposed = index.transpose(1, 2);
		
		int[] sizes = transposed.getSizes();
		
		assertArrayEquals(sizes, new int[]{3,5,4});
		
		transposed = transposed.transpose(0, 1);
		
		sizes = transposed.getSizes();
		
		assertArrayEquals(sizes, new int[]{5,3,4});
		
		IndexIterator iterator = transposed.iterator();
		IndexIterator otherIterator = otherIndex.iterator();
		
		while(iterator.hasNext())
		{
			iterator.nextInt();
			
			otherIterator.nextInt();
			
			int[] currentIndex = iterator.getCurrentIndex();
			assertArrayEquals(otherIterator.getCurrentIndex(),currentIndex);
			assertEquals(index.toLinearIndex(currentIndex[1],currentIndex[2],currentIndex[0]), ((SubSpaceSplitIterator)iterator).getCurrentFullLinearIndex());
		}
	}
	
	@Test
	public void testSubSubSpaceSplitIterator()
	{
		Index index = new Index(2,3,4);
		
		SubSpaceSplitIterator iterator = (SubSpaceSplitIterator) index.subIndexAt(new int[]{-1,-1,2}).iteratorAt(new int[]{-1,1});
		
		assertEquals(2, iterator.getCurrentFullIndex().length);
		
		iterator.nextInt();
		
		assertArrayEquals(new int[]{0,1}, iterator.getCurrentFullIndex());
	}

    @Test
    public void testParallelIterators()
    {
        Index index = new Index(2,3,4,5);
        final SkippingLinearIterator[] iterators = index.parallelIterators(3, 0, 2);

        // There should be 15 (3*5) iterators
        assertEquals(3, iterators.length);

        final SubSpaceSplitIterator checkIterator =
                index.iterator(0, 2).getOrthogonalIterator();

        for (SkippingLinearIterator iterator : iterators)
        {
            while(iterator.hasNextParallel())
            {  
            	iterator.nextParallel();
            	checkIterator.nextInt();
	            final SubSpaceSplitIterator orthCheck = checkIterator.getOrthogonalIterator();
	
	            int expectedIndex = 0;
	            while(iterator.hasNext())
	            {
	                int currentIndex = iterator.nextInt();
	                orthCheck.nextInt();
	
	                assertEquals(expectedIndex++, currentIndex);
	
	                assertEquals(orthCheck.getCurrentFullLinearIndex(),
	                        iterator.getCurrentFullLinearIndex());
	            }
	            
	            // There should be 8 (2*4) elements in each parallel iterator
	            assertEquals(8, expectedIndex);
            }
        }
    }

    @Test
    public void testSubParallelIterators()
    {
        Index index = new Index(2,3,4,5);

        final SkippingLinearIterator[] iterators = index.subIndexAt(new int[]{-1,-1,-1,2}).parallelIterators(2, 0, 2);

        // There should be 3 iterators
        assertEquals(2, iterators.length);

        final SubSpaceSplitIterator checkIterator = index.subIndexAt(new int[]{-1,-1,-1,2}).
                iterator(0, 2).getOrthogonalIterator();

        for (SkippingLinearIterator iterator : iterators)
        {
        	while(iterator.hasNextParallel())
        	{
        		iterator.nextParallel();
        	
	            checkIterator.nextInt();
	            final SubSpaceSplitIterator orthCheck = checkIterator.getOrthogonalIterator();
	
	            int expectedIndex = 0;
	
	            while(iterator.hasNext())
	            {
	                int currentIndex = iterator.nextInt();
	                orthCheck.nextInt();
	
	                assertEquals(expectedIndex++, currentIndex);
	
	                assertEquals(orthCheck.getCurrentFullLinearIndex(),
	                        iterator.getCurrentFullLinearIndex());
	            }
	            // There should be 8 (2*4) elements in each parallel iterator
	            assertEquals(8, expectedIndex);
	        }
        }
    }
}
