/**
 * 
 */
package com.meliorbis.numerics;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.impl.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;

/**
 * @author toby
 */
public class TestNumerics extends NumericsTestBase
{
	
	@Test
	public void testWriteFormattedCSV() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		DoubleArray<?> original = _numerics.getArrayFactory().newArray(3,4,2);
		original.fill(new double[]{1d,2d,3d,4d,5d,6d,7d,8d,9d,10d,11d,12d,13d,14d,15d,16d,17d,18d,19d,20d,21d,22d,23d,24d});
		
		File tempFile = File.createTempFile("testNumerics",null);
		
		_numerics.writeFormattedCSV(original, tempFile.getPath());
		
		DoubleArray<?> read = (DoubleArray<?>) _numerics.<Double>readFormattedCSV(tempFile.getPath());
		
		ArrayAssert.assertEquals(original, read, 1e-15);
		
		tempFile.delete();
	}
	
	@Test
	public void testReadIntCSV() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		File tempFile = File.createTempFile("testNumerics",null);
		
		FileWriter writer = new FileWriter(tempFile);
		writer.write("1,2\n3,4\n5,6");
		writer.close();
		
		IntegerArray array = _numerics.readIntCSV(new FileInputStream(tempFile),',');
		
		int[] sizes = array.size();
		
		assertEquals(2, sizes.length);
		assertEquals(3,sizes[0]);
		assertEquals(2,sizes[1]);
		
		assertEquals(2,(int)array.get(0,1));

		assertEquals(4,(int)array.get(1,1));		
	}
	
	@Test
	public void testCSVRead() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		File tempFile = File.createTempFile("testNumerics",null);
		
		FileWriter writer = new FileWriter(tempFile);
		writer.write("1.1,1.2\n1.3,1.4\n1.5,1.6");
		writer.close();
		
		DoubleArray<?> array = _numerics.readCSV(tempFile.getAbsolutePath());
		
		int[] sizes = array.size();
		
		assertEquals(2, sizes.length);
		assertEquals(3,sizes[0]);
		assertEquals(2,sizes[1]);
		
		assertEquals(1.2,array.get(0,1),1e-10);

		assertEquals(1.4,array.get(1,1),1e-10);		
	}
	
	@Test
	public void testReadCSVFormatted() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		File tempFile = File.createTempFile("testNumerics",null);
		
		FileWriter writer = new FileWriter(tempFile);
		writer.write("java.lang.Double,3,4,2\n1.1,1.2\n1.3,1.4\n1.5,1.6\n1.7,1.8\n1.9,1.10\n1.11,1.12\n" +
				"1.21,1.22\n1.23,1.24\n1.25,1.26\n1.27,1.28\n1.29,1.210\n1.211,1.212\n");
		writer.close();
		
		DoubleArray<?> array = (DoubleArray<?>) _numerics.<Double>readFormattedCSV(tempFile.getAbsolutePath());
		
		int[] sizes = array.size();
		
		assertEquals(3, sizes.length);
		assertEquals(3,sizes[0]);
		assertEquals(4,sizes[1]);
		assertEquals(2,sizes[2]);
		
		assertEquals(1.21,array.get(2,2,1),1e-10);

		assertEquals(1.23,array.get(1,3,0),1e-10);		
		
		DoubleArray<?> array2 = (DoubleArray<?>) _numerics.<Double>readFormattedCSV(
				new FileInputStream(tempFile));
		
		ArrayAssert.assertEquals(array, array2, 1e-15);
		
		/* Empty File returns null
		 */
		File tempFileEmptu = File.createTempFile("testNumericsEmpty",null);
		DoubleArray<?> arrayEmpty = (DoubleArray<?>) _numerics.<Double>readFormattedCSV(tempFileEmptu.getAbsolutePath());
		
		assertTrue(arrayEmpty == null);
	}
	
	@Test(expected= NumericsException.class)
	public void testCSVReadEmpty() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		File tempFile = File.createTempFile("testNumerics",null);
		
		FileWriter writer = new FileWriter(tempFile);
		writer.write("");
		writer.close();
		
		_numerics.readCSV(tempFile.getAbsolutePath());
		
		fail("Should have thrown exception");
	}
	
	@Test(expected= NumericsException.class)
	public void testCSVUnequalRows() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		File tempFile = File.createTempFile("testNumerics",null);
		
		FileWriter writer = new FileWriter(tempFile);
		writer.write("1.1,1.2\n1.3,1.4,1.5");
		writer.close();
		
		_numerics.readCSV(tempFile.getAbsolutePath());
		
		fail("Should have thrown exception");
	}
	
	@Test(expected= NumericsException.class)
	public void testCSVMalformedNumber() throws IOException, NumericsException, MultiDimensionalArrayException
	{
		File tempFile = File.createTempFile("testNumerics",null);
		
		FileWriter writer = new FileWriter(tempFile);
		writer.write("1.1,1.2\n1.3,1.4sfdas");
		writer.close();
		
		_numerics.readCSV(tempFile.getAbsolutePath());
		
		fail("Should have thrown exception");
	}
}
