package com.meliorbis.numerics.convergence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import com.meliorbis.utils.Pair;

public class ConvergerTest
{
	private static final double DELTA = 1e-10;
	Mockery context = new Mockery();
	
	@Test
	public void testReturnsFinalState()
	{
		Converger converger = new Converger();
		
		assertNotNull(converger);

		@SuppressWarnings("unchecked")
		Convergable<Double, RuntimeException> convergable = context.mock(Convergable.class);

		context.checking(new Expectations() {{
			oneOf(convergable).performIteration(10d); will(returnValue(
					new Pair<Double, Criterion>(10d, new DoubleCriterion(0d))));
		}});
		
		Double result = converger.converge(convergable, 10d, 1e-5);
		
		assertEquals(10d, result, DELTA);
	}
	
	@Test
	public void testCallsConvergableIfNotConverged()
	{
		Converger converger = new Converger();
		
		assertNotNull(converger);

		@SuppressWarnings("unchecked")
		Convergable<Double, RuntimeException> convergable = context.mock(Convergable.class);

		context.checking(new Expectations() {{
			oneOf(convergable).performIteration(10d); will(returnValue(
					new Pair<Double, Criterion>(9d, new DoubleCriterion(5d))));
			
			oneOf(convergable).performIteration(9d); will(returnValue(
					new Pair<Double, Criterion>(8.5d, new DoubleCriterion(1d))));

			oneOf(convergable).performIteration(8.5d); will(returnValue(
					new Pair<Double, Criterion>(8.4d, new DoubleCriterion(1e-5d))));

			oneOf(convergable).performIteration(8.4d); will(returnValue(
					new Pair<Double, Criterion>(8.4d, new DoubleCriterion(1e-6d))));

		}});
		
		Double result = converger.converge(convergable, 10d, 1e-5);
		
		assertEquals(8.4, result, DELTA);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCallsCallback()
	{
		Converger converger = new Converger();
		
		assertNotNull(converger);

		Convergable<Double, RuntimeException> convergable = context.mock(Convergable.class);

		Converger.Callback cb = context.mock(Converger.Callback.class);

		context.checking(new Expectations() {{
			oneOf(convergable).performIteration(10d); will(returnValue(
					new Pair<Double, Criterion>(9d, new DoubleCriterion(5d))));
			
			oneOf(convergable).performIteration(9d); will(returnValue(
					new Pair<Double, Criterion>(8.5d, new DoubleCriterion(1d))));

			oneOf(convergable).performIteration(8.5d); will(returnValue(
					new Pair<Double, Criterion>(8.4d, new DoubleCriterion(1e-5d+DELTA))));

			oneOf(convergable).performIteration(8.4d); will(returnValue(
					new Pair<Double, Criterion>(8.4d, new DoubleCriterion(1e-6d))));
			
			oneOf(cb).notify(with(any(Pair.class)), with(1));
			oneOf(cb).notify(with(any(Pair.class)), with(2));
			oneOf(cb).notify(with(any(Pair.class)), with(3));
			oneOf(cb).notify(with(any(Pair.class)), with(4));

		}});
		
		converger.addCallback(cb);
		
		Double result = converger.converge(convergable, 10d, 1e-5);
		
		assertEquals(8.4, result, DELTA);
		
		context.assertIsSatisfied();
	}

}
