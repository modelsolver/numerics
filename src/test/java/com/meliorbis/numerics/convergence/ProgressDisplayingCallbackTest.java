package com.meliorbis.numerics.convergence;

import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.Test;

import com.meliorbis.utils.Pair;

public class ProgressDisplayingCallbackTest
{

	@Test
	public void test()
	{
		StringWriter writer = new StringWriter();
		
		ProgressDisplayingCallback cb = 
				new ProgressDisplayingCallback(new PrintWriter(writer));
		
		cb.notify(null, 1);
		
		assertEquals(0, writer.getBuffer().length());
		
		cb.notify(null, 10);
		cb.notify(null, 1010);
		
		assertEquals("..", writer.toString());

		cb.notify(new Pair<Double,Criterion>(null, new DoubleCriterion(2.5)), 700);
		
		assertEquals("...[7]: 2.50e+00\n", writer.toString());
	}

}
