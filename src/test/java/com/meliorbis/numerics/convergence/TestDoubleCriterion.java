package com.meliorbis.numerics.convergence;

import org.junit.Test;
import static org.junit.Assert.*;
public class TestDoubleCriterion
{
	@Test
	public void testToString() {
		DoubleCriterion criterion = new DoubleCriterion(.1);
		
		assertEquals("1.00e-01", criterion.toString());

		criterion = new DoubleCriterion("Name", .1);

		assertEquals("Name: 1.00e-01", criterion.toString());
	}
}
