package com.meliorbis.numerics.convergence;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestMaxCriterion
{
	private static final double DELTA = 1e-10;

	@Test 
	public void testValue() 
	{
		MaxCriterion crit = new MaxCriterion(new DoubleCriterion(1.0), new DoubleCriterion(.1), new DoubleCriterion(1.5));
		assertEquals(1.5,crit.getValue(), DELTA);
	}
	
	
	@Test
	public void testSingleValue()
	{
		MaxCriterion crit = new MaxCriterion(new DoubleCriterion(.1));
		assertEquals(.1,crit.getValue(), DELTA);
	}
	
	@Test
	public void testToString()
	{
		MaxCriterion crit = new MaxCriterion(new DoubleCriterion("A",1.0), new DoubleCriterion(.1), new DoubleCriterion("C",1.5));
		
		assertEquals("A: 1.00e+00, 1.00e-01, C: 1.50e+00", crit.toString());
	}
}
