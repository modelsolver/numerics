package com.meliorbis.numerics.fixedpoint;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.meliorbis.numerics.function.MultiVariateVectorFunction;
import com.meliorbis.numerics.test.NumericsTestBase;

/**
 * Created by toby on 24/01/2014.
 */
public class MultiStageMultiVariateFPFinderTest extends NumericsTestBase
{
    @Test
    public void testFindFixedPoint() throws Exception
    {
        final MultiVariateVectorFunction<Double> fn = new MultiVariateVectorFunction<Double>()
        {

            @Override
            public Double[] call(Double... args_)
            {
                return new Double[]{
                        // 2^2 + log(1) - 2 = 2
                        Math.pow(args_[0],2d)+Math.log(args_[1]) - 2,

                        // (2 + 1)/3 = 1
                        (args_[0]+args_[1])/3d
                };
            }
        };

        final MultiStageMultiVariateFPFinder fpFinder =
                new MultiStageMultiVariateFPFinder(_numerics, 1e-8, 1e-8, Arrays.asList(
                        new int[][]{
                                new int[]{1},new int[]{0}
                        }
                ));

        fpFinder.setDamping(0.5d, 0.5d);

        final double[] fixedPoint = fpFinder.findFixedPoint(fn, new double[]{4d, 4d});

        Assert.assertArrayEquals(new double[]{2d,1d}, fixedPoint,1e-6);
    }
}
