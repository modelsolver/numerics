package com.meliorbis.numerics.fixedpoint;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.math3.util.Precision;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Assert;
import org.junit.Test;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.function.MultiVariateVectorFunction;
import com.meliorbis.numerics.test.NumericsTestBase;

/**
 * Created by toby on 24/01/2014.
 */
public class MultiVariateNewtonFixedPointFinderTest extends NumericsTestBase
{
	private static final double DELTA = 1e-8;

	@Test
	public void testValidateNonError()
	{
		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		fpFinder.validateDistance(Math.sqrt(.5), null, null);

		// No exception thrown should be fine
	}

	@Test(expected = NumericsException.class)
	public void testValidateErrorInputNaN()
	{
		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		fpFinder.validateDistance(Double.NaN, new Double[]
		{ 1d, Double.NaN }, new Double[]
		{ 1d, 2d });
	}

	@Test(expected = NumericsException.class)
	public void testValidateErrorOututNaN()
	{
		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		fpFinder.validateDistance(Double.NaN, new Double[]
		{ 1d, 1d }, new Double[]
		{ 1d, Double.NaN });
	}

	@Test
	public void testEvaluateMetric()
	{
		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		// Normal scenario yields correct result
		{
			Double[] input = new Double[]
			{ 1d, 2d };
			Double[] output = new Double[]
			{ 1.5d, 1.5d };

			double metric = fpFinder.evaluateMetric(input, output);

			assertEquals(Math.sqrt(.5), metric, DELTA);
		}

		// NaN yields NaN
		{
			Double[] input = new Double[]
			{ 1d, 2d };
			Double[] output = new Double[]
			{ 1.5d, Double.NaN };

			double metric = fpFinder.evaluateMetric(input, output);

			assertTrue(Double.isNaN(metric));
		}

		// Infty yields Infty
		{
			Double[] input = new Double[]
			{ 1d, 2d };
			Double[] output = new Double[]
			{ 1.5d, Double.POSITIVE_INFINITY };

			double metric = fpFinder.evaluateMetric(input, output);

			assertTrue(Double.isInfinite(metric));
		}
	}

	@Test(expected = NumericsException.class)
	public void testNoChangeThrowsException() throws Exception
	{
		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		Double[] outA = new Double[]
		{ 1d, 2d, 3d };
		Double[] outB = new Double[]
		{ 1d, 2d, 3d };

		fpFinder.calculatePartialDiffs(outA, outB, 1e-6);
	}

	@Test
	public void testPartialDerivCalc() throws Exception
	{
		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		{
			// A single diff is enough
			Double[] outA = new Double[]
			{ 1d, 2d, 3d };
			Double[] outB = new Double[]
			{ 1d, 2d, 3d + 1e-5 };

			double[] partials = fpFinder.calculatePartialDiffs(outA, outB, 1e-6);

			assertArrayEquals(new double[]
			{ 0d, 0d, 10d }, partials, DELTA);
		}
		{
			// But all variables are considered
			Double[] outA = new Double[]
			{ 1d, 2d, 3d };
			Double[] outB = new Double[]
			{ 1d + 1e-6, 2d + 5e-6, 3d + 1e-5 };

			double[] partials = fpFinder.calculatePartialDiffs(outA, outB, 1e-6);

			assertArrayEquals(new double[]
			{ 1d, 5d, 10d }, partials, DELTA);
		}
	}

	private static final class DoubleArrayMatcher extends BaseMatcher<Double[]>
	{
		private final Double[] _toMatch;

		public DoubleArrayMatcher(Double[] toMatch_)
		{
			super();
			
			assert toMatch_ != null;
			
			_toMatch = toMatch_;
		}

		@Override
		public void describeTo(Description description_)
		{
			description_.appendText(Arrays.toString(_toMatch));
		}

		@Override
		public boolean matches(Object item_)
		{
			if(item_ instanceof Double[]) {
				
				Double[] param = (Double[]) item_;
				
				if(param.length != _toMatch.length) {
					return false;
				}
				
				for(int i = 0; i < param.length; i++)
				{
					if(!Precision.equals(_toMatch[i], param[i],DELTA))
					{
						return false;
					}
				}
				
				return true;
			}
		
			return false;
		}
		
	}
	
	private DoubleArrayMatcher doubleArray(Double[] toMatch_)
	{
		return new DoubleArrayMatcher(toMatch_);
	}
	
	@Test
	public void testIncreaseDeltaOnNoChange()
	{
		Mockery context = new Mockery();

		@SuppressWarnings("unchecked")
		MultiVariateVectorFunction<Double> fn = context.mock(MultiVariateVectorFunction.class);

		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		
		context.checking(new Expectations()
		{
			{
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 2 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 4 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 8 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 16 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 32 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 64 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 128 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 256 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 512 * 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
				oneOf(fn).call(with(doubleArray(new Double[]
				{ 1d + 1024* 1e-8, 1d })));
				will(returnValue(new Double[]
				{ 2d, 2d }));
			}
		});

		boolean ex = false;
		try {
			fpFinder.findFixedPoint(fn, new double[]
			{ 1d, 1d });
		} catch(NumericsException e) {
			ex = true;
		}
		assertTrue(ex);
		context.assertIsSatisfied();
	}

	@Test
	public void testFindFixedPoint() throws Exception
	{
		final MultiVariateVectorFunction<Double> fn = new MultiVariateVectorFunction<Double>()
		{

			@Override
			public Double[] call(Double... args_)
			{
				return new Double[]
				{
						// 2 + log(1) = 2
						args_[0] + Math.log(args_[1]),

						// (2 + 1)/3 = 1
						(args_[0] + args_[1]) / 3d };
			}
		};

		final MultiVariateNewtonFixedPointFinder fpFinder = new MultiVariateNewtonFixedPointFinder(1e-8, 1e-8);

		final double[] fixedPoint = fpFinder.findFixedPoint(fn, new double[]
		{ 4d, 4d });

		Assert.assertArrayEquals(new double[]
		{ 2d, 1d }, fixedPoint, 1e-6);
	}
}
