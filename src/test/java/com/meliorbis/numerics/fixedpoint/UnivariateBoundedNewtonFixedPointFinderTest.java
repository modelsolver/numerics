package com.meliorbis.numerics.fixedpoint;

import org.junit.Test;
import org.junit.Assert;

import com.meliorbis.numerics.function.MultiVariateVectorFunction;


/**
 * Created by toby on 24/01/2014.
 */
public class UnivariateBoundedNewtonFixedPointFinderTest
{
@Test
    public void testFindFixedPoint() throws Exception
    {
        final MultiVariateVectorFunction<Double> fn = new MultiVariateVectorFunction<Double>()
        {

            @Override
            public Double[] call(Double... args_)
            {
                return new Double[]{
                        // 2 + log(1) = 2
                        5*Math.log(args_[0])
                };
            }
        };

        final UnivariateBoundedNewtonFixedPointFinder fpFinder =
                new UnivariateBoundedNewtonFixedPointFinder(1e-6, 1e-10);

        double[] fixedPoint = fpFinder.findFixedPoint(fn, new double[]{1d});

        Assert.assertEquals(1.295855509095368, fixedPoint[0],1e-6);

        fixedPoint = fpFinder.findFixedPoint(fn, new double[]{10d});

        Assert.assertEquals(12.713206788867632, fixedPoint[0], 1e-6);
    }
}
