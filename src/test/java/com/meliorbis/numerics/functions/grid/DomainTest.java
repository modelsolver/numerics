package com.meliorbis.numerics.functions.grid;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.meliorbis.numerics.function.primitives.DoubleRectangularGridDomain;
import com.meliorbis.numerics.generic.primitives.DoubleArray;


public class DomainTest extends FunctionsTestBase
{
	private DoubleRectangularGridDomain _domain;
	
	@Before
	public void setUp()
	{
		super.setUp();
		
		_domain = _factory.createDomain(new DoubleArray<?>[] {
				createArray(1d,2d,3d),
				createArray(4d,5d),
				createArray(7d)
		});
	}
	
	@Test
	public void equals()
	{
		DoubleRectangularGridDomain otherDomain = _factory.createDomain(new DoubleArray<?>[] {
			createArray(1d,2d,3d),
			createArray(4d,5d),
			createArray(7d)});
		
		assertEquals(_domain, otherDomain);
		
		DoubleRectangularGridDomain otherDomain2 = _factory.createDomain(new DoubleArray<?>[] {
			createArray(1d,2d,3d),
			createArray(4d,5d),
			createArray(7d,8d)});
		
		assertTrue(!_domain.equals(otherDomain2));
		
		DoubleRectangularGridDomain otherDomain3 = _factory.createDomain(new DoubleArray<?>[] {
			createArray(1d,2d,3d),
			createArray(4d,5d)});
		
		assertTrue(!_domain.equals(otherDomain3));
	}
	
	@Test
	public void reportedSizeIsCorrect()
	{
		assertEquals(3, _domain.getNumberOfDimensions());
	}
	
	@Test
	public void correctAxisIsReturned()
	{
		assertEquals(createArray(4d,5d), _domain.getAxis(1));
		assertEquals(createArray(1d,2d,3d), _domain.getAxis(0));
		assertEquals(createArray(7d), _domain.getAxis(2));
	}
	
	@Test
	public void defaultValuesArrayHasCorrectSize()
	{
		assertArrayEquals(new int[] {3,2,1,1},_domain.createValueGrid().size());
	}
	
	@Test
	public void specificOutputSizeValuesArrayHasCorrectSize()
	{
		assertArrayEquals(new int[] {3,2,1,7},_domain.createValueGrid(7).size());
	}
}
