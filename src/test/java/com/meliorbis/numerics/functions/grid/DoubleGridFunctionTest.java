package com.meliorbis.numerics.functions.grid;


import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;

import org.junit.Test;

import com.meliorbis.numerics.function.FunctionException;
import com.meliorbis.numerics.function.primitives.DoubleGridFunction;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert;

import static org.junit.Assert.assertEquals;

/**
 * @author Tobias Grasl
 */
public class DoubleGridFunctionTest extends FunctionsTestBase
{ 
	private static class TestDblSpecialisation
	{
		double foo(Double[] doubles_)
		{
			System.out.println("Called Object");
			return foo(ArrayUtils.toPrimitive(doubles_));
		}

		private double foo(double[] primitive_)
		{
			System.out.println("Called Primitive");
			return primitive_[0];
		}
	}
	
	@Test
	public void callPrimitiveSpec()
	{
		TestDblSpecialisation testDblSpecialisation = new TestDblSpecialisation();
		
		testDblSpecialisation.foo(new Double[]{1.5d});
		testDblSpecialisation.foo(new double[]{1.5d});
	}
	
    @Test(expected = FunctionException.class)
    public void tooFewValueDimensionsThrowsException()
    {
        _factory.createFunction(Arrays.asList(
                createArray(1d,2d),
                createArray(1d,2d)),
                createArray(2));
    }

    @Test(expected = FunctionException.class)
    public void inconsistentDimensionsThrowsException()
    {
    	_factory.createFunction(Arrays.asList(
                createArray(1d,2d),
                createArray(1d,2d)),
                createArray(2,3,2,4));
    }

    @Test(expected = FunctionException.class)
    public void tooManyInputsThrowsException()
    {
		DoubleArray<?> values = createArray(3, 2);

        // Constant, 2-valued function
        values.fill(1d, 2d,
                1d, 2d,
                1d, 2d);

        DoubleGridFunction fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d)), values);

        fn.callWithDouble(1d,2d);
    }

    @Test(expected = FunctionException.class)
    public void tooFewInputsThrowsException()
    {
        DoubleArray<?> values = createArray(3, 2);

        // Constant, 2-valued function
        values.fill(1d, 2d,
                1d, 2d,
                1d, 2d);

        DoubleGridFunction fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d), createArray(1d,2d)), values);

        fn.callWithDouble(1d);
    }


    @Test
    public void oneInput()
    {
        DoubleArray<?> values = createArray(3, 2);

        // Constant, 2-valued function
        values.fill(1d,2d,
                    1d,2d,
                    1d,2d);

        DoubleGridFunction fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d)), values);

        ArrayAssert.assertEquals(createArray(1d,2d),fn.callWithDouble(1.5), MAX_ERROR);

        // Constant, 2-valued function
        values.fill(1d,6d,
                    2d,4d,
                    3d,3d);

        fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d)), values);

        ArrayAssert.assertEquals(createArray(1.5d,5d),fn.callWithDouble(1.5), MAX_ERROR);
        ArrayAssert.assertEquals(createArray(2.25,3.75),fn.callWithDouble(2.25), MAX_ERROR);

        // Multidimenional results not really meaningful
//        // Multidimensional result
//        values = createArray(3, 2, 2);
//
//        values.fill(1d, 6d, 10d, 2d,
//                    2d, 4d, 9d, 3d,
//                    3d, 3d, 3d, 6d);
//
//        fn = _factory.createFunction(Arrays.asList(
//                createArray(1d, 2d, 3d)), values);
//
//        ArrayAssert.assertEquals(createArray(2,2).fill(2.5,3.5,6d,4.5),fn.callWithDouble(2.5), MAX_ERROR);
    }

    @Test
    public void multipleInput()
    {
        DoubleArray<?> values = createArray(3, 2, 2);

        values.lastDimSlice(0).fill(    1d, 6d,
                                        2d, 4d,
                                        3d, 3d);

        values.lastDimSlice(1).fill(    10d, 2d,
                                        9d, 3d,
                                        3d, 7d);

        DoubleGridFunction fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 6d), createArray(1d,2d)), values);

        ArrayAssert.assertEquals(createArray(3d, 5.75d), fn.callWithDouble(3d,1.5d), MAX_ERROR);
    }

    @Test
    public void testDerivative()
    {
        DoubleArray<?> values = createArray(3, 2);

        // Constant, 2-valued function
        values.fill(1d,2d,
                1d,2d,
                1d,2d);

        DoubleGridFunction fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d)), values);

        ArrayAssert.assertEquals(createArray(0d, 0d), fn.partialDerivative(0).callWithDouble(1.5), MAX_ERROR);


        // Constant, 2-valued function
        values.fill(1d,3d,
                2d,2d,
                3d,1d);

        _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d)), values);

        ArrayAssert.assertEquals(createArray(1d,-1d),fn.partialDerivative(0).callWithDouble(1.5), MAX_ERROR);


        // Single value, two dimensional
        fn = _factory.createFunction(Arrays.asList(
                createArray(1d, 2d, 3d),createArray(1d, 2d)), values);

        assertEquals(0d, fn.partialDerivative(0, new double[]{Double.NaN, 1.5}).callWithDouble(1.5).get(0), MAX_ERROR);

        // Restricting and calling the other way round should yield the same result
        assertEquals(0d, fn.partialDerivative(0, new double[]{1.5}).callWithDouble(1.5).get(0), MAX_ERROR);
    }
}
