package com.meliorbis.numerics.functions.grid;

import org.junit.Before;

import com.meliorbis.numerics.function.primitives.DoubleGridFunctionFactory;
import com.meliorbis.numerics.test.NumericsTestBase;

public class FunctionsTestBase extends NumericsTestBase
{
	protected DoubleGridFunctionFactory _factory;

	public FunctionsTestBase()
	{
		super();
	}

	@Before
	public void setUp()
	{
		_factory = new DoubleGridFunctionFactory();
	}

}