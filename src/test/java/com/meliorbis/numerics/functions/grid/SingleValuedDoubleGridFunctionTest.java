package com.meliorbis.numerics.functions.grid;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import com.meliorbis.numerics.function.FunctionException;
import com.meliorbis.numerics.function.primitives.SingleValuedDoubleGridFunction;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * @author Tobias Grasl
 */
public class SingleValuedDoubleGridFunctionTest extends FunctionsTestBase
{
    /**
     * Check basic calls of single-input function
     */
    @Test
    public void testOneDimensional() throws Exception
    {
        // Constant fn
        SingleValuedDoubleGridFunction fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d)
        ), createArray(0.5d, 0.5d, 0.5d));

        assertEquals(0.5, fn.callDouble(1.5), MAX_ERROR);

        assertEquals(0.5, fn.callDouble(2d), MAX_ERROR);

        // Identity fn
        fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d)
        ), createArray(1d, 2d, 3d));

        assertEquals(1.5, fn.callDouble(1.5), MAX_ERROR);

        assertEquals(2d, fn.callDouble(2d), MAX_ERROR);


        fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d)
        ), createArray(6d, 5d, 4d));

        assertEquals(1.75, fn.callDouble(5.25), MAX_ERROR);

        assertEquals(4.0d, fn.callDouble(3d), MAX_ERROR);

        // Test where the values have an extra dimension of the correct size, 1
        final DoubleArray<?> values = createArray(3, 1);
        values.fill(6d, 5d, 4d);

        fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d)
        ), values);

        assertEquals(1.75, fn.callDouble(5.25), MAX_ERROR);

        assertEquals(4.0d, fn.callDouble(3d), MAX_ERROR);
    }

    /**
     * Check basic calls of multi-input function
     */
    @Test
    public void testMultiDimensional()
    {
        DoubleArray<?> values = createArray(3, 2);
        values.fill(0.5,0.5,0.5,0.5,0.5,0.5);

        // Constant fn
        SingleValuedDoubleGridFunction fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d),createArray(0.1,0.5)
        ), values);

        assertEquals(0.5, fn.callDouble(1.5,0.2), MAX_ERROR);

        assertEquals(0.5, fn.callDouble(2d,0.4), MAX_ERROR);

        // Non-constant
        values = createArray(3, 2, 1);
        values.fill(1d, 2d,
                    2d, 4d,
                    3d, 6d);

        fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d),createArray(0.1,0.5)
        ), values);

        assertEquals((1.5d*3d +3d)/4d, fn.callDouble(1.5,0.2), MAX_ERROR);

        assertEquals(3.5, fn.callDouble(2d,0.4), MAX_ERROR);
    }


    /**
     * Checks that an exception is thrown when attempting to create a single valued array
     * where the value-dimensions is greater than size 1
     */
    @Test(expected = FunctionException.class)
    public void testMultiValuedThrowsException()
    {
        DoubleArray<?> values = createArray(3, 2, 2);
        // Constant fn
        SingleValuedDoubleGridFunction fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d),createArray(0.1,0.5)
        ), values);

        fn.callDouble(1d, 2d);
    }

    /**
     * Checks that an exception is thrown when attempting to create a single valued array
     * where there are multiple value-dimensions in excess of the grid dimensions
     */
    @Test(expected = FunctionException.class)
    public void testMultiDimensionalValuesThrowsException()
    {
        DoubleArray<?> values = createArray(3, 2, 1, 1);
        // Constant fn
        SingleValuedDoubleGridFunction fn = _factory.createSingleValuedFunction(Arrays.asList(
                createArray(1d, 2d, 3d),createArray(0.1,0.5)
        ), values);

        fn.callDouble(1d, 2d);
    }
}
