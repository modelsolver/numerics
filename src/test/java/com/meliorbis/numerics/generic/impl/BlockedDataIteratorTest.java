package com.meliorbis.numerics.generic.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.SettableIndexedIterator;
import com.meliorbis.numerics.generic.SubSpaceSplitIterator;

public class BlockedDataIteratorTest {

	private GenericBlockedArrayData<Integer> _data;

	@Test
	public void testFullIteration() throws MultiDimensionalArrayException {
		
		SettableIndexedIterator<Integer> iterator = _data.iterator();
		
		int count = 0;
		int value;
		
		while((value = iterator.next()) == 0)
		{
			count++;
		}
		
		// Should be the linear index of the position we set 7 at		
		assertEquals(19,count);
		assertEquals(7,value);
		
		count++;
		
		while(iterator.hasNext())
		{
			assertEquals(0, (int)iterator.next());
			count++;
		}
		
		// Number of elements
		assertEquals(24, count);
	}

	@Before
	public void setUp() throws MultiDimensionalArrayException {
		_data = new GenericBlockedArrayData<Integer>(new int[]{2,3,4},0, length -> new Integer[length], length -> new Integer[length][] );
		
		_data.set(Integer.valueOf(7), new int[]{1,1,3});
	}
	
	@Test
	public void testIndexedIteration()
	{
		SettableIndexedIterator<Integer> iterator = _data.iteratorAt(1,1);
		
		int count = 0;
		int value;
		
		while((value = iterator.next()) == 0)
		{
			count++;
		}
		
		// Should be the linear index of the position we set 7 at		
		assertEquals(3,count);
		assertEquals(7,value);
		
		assertFalse(iterator.hasNext());
		
		iterator = _data.iteratorAt(1,-1,3);
		
		count = 0;
		
		while((value = iterator.next()) == 0)
		{
			count++;
		}
		
		// Should be the linear index of the position we set 7 at		
		assertEquals(1,count);
		assertEquals(7,value);
		count++;
		
		while(iterator.hasNext())
		{
			iterator.next();
			count++;
		}
		
		assertEquals(3,count);
	}
	
	@Test
	public void testSubspaceIteration()
	{
		SubSpaceSplitIterator<Integer> iterator = (SubSpaceSplitIterator<Integer>) _data.iteratorAcross(1,2);
		
		int outerCount = 0;
		
		while(iterator.hasNext())
		{
			iterator.next();
			outerCount++;
		
			int innerCount = 0;
			SubSpaceSplitIterator<Integer> orth = iterator.getOrthogonalIterator();
			
			while(orth.hasNext())
			{
				innerCount++;
				Integer val = orth.next();
				
				if(val != 0)
				{
					int[] subIndex = orth.getIndex();
					
					assertEquals(1,subIndex.length);
					assertEquals(1, subIndex[0]);
					
					int[] fullIndex = orth.getFullIndex();
					
					assertEquals(3, fullIndex.length);
					assertEquals(1, fullIndex[0]);
					assertEquals(1, fullIndex[1]);
					assertEquals(3, fullIndex[2]);
					
					assertEquals(val, orth.get());
					assertEquals(7,(int)val);
				}
			}
			
			assertEquals(2,innerCount);
		}
		
		assertEquals(12,outerCount);
	}
}
