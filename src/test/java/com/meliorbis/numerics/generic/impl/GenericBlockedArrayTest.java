package com.meliorbis.numerics.generic.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.math3.util.Precision;
import org.junit.Test;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.ArrayFactory1D;
import com.meliorbis.numerics.generic.ArrayFactory2D;
import com.meliorbis.numerics.generic.BinaryOp;
import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.NaryOp;
import com.meliorbis.numerics.generic.SettableIndexedIterator;
import com.meliorbis.numerics.generic.SettableIterator;
import com.meliorbis.numerics.index.SubIndex;
import com.meliorbis.numerics.threading.ComputableRecursiveAction;
import com.meliorbis.numerics.threading.CurrentThreadExecutor;
import com.meliorbis.numerics.threading.Executor;

public class GenericBlockedArrayTest
{
	private final class Impl extends GenericBlockedArray<Float, Impl>
	{
		
		public Impl(BlockedArrayData<Float> data_, Executor executor_)
		{
			super(data_, executor_);
			// TODO Auto-generated constructor stub
		}

		protected Impl(Float[] data_, Executor executor_)
		{
			super(data_, executor_);
		}

		public Impl(BlockedArrayData<Float> data_, SubIndex dimensionCounter_, Executor executor_)
		{
			super(data_, dimensionCounter_, executor_);
		}

		@Override
		public Comparator<Float> getComparator()
		{
			return (a,b) -> a < b ? -1 : (a > b) ? 1 : 0;
		}

		@Override
		protected ArrayFactory2D<Float> getArrayFactory2D()
		{
			return n -> new Float[n][];
		}

		@Override
		protected ArrayFactory1D<Float> getArrayFactory1D()
		{
			return n -> new Float[n];
		}

		@Override
		protected Impl createSub(BlockedArrayData<Float> _data_, SubIndex dimensionCounter_)
		{
			return new Impl(_data_, dimensionCounter_, _executor);
		}

		@Override
		protected Impl createNew(int... dimensions_)
		{
			GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(dimensions_,0f,n->new Float[n],n->new Float[n][]);
			
			return new Impl(data, new CurrentThreadExecutor());
		}

		@Override
		protected Float getZero()
		{
			return 0f;
		}

		@Override
		protected Float getMinusOne()
		{
			return -1f;
		}

		@Override
		public BinaryOp<Float, RuntimeException> getAddOp()
		{
			return (Float a, Float b) -> a+b;
		}

		@Override
		public BinaryOp<Float, RuntimeException> getSubtractionOp()
		{
			return (Float a, Float b) -> a-b;
		}

		@Override
		public BinaryOp<Float, RuntimeException> getMultOp()
		{
			return (Float a, Float b) -> a*b;
		}

		@Override
		public BinaryOp<Float, RuntimeException> getDivisionOp()
		{
			return (Float a, Float b) -> a/b;
		}
		
	}


	private static final float DELTA = (float)1e-5;
	
	@Test
	public void testCreateFromValues()
	{
		Impl impl = new Impl(new Float[] {1f,2f,3f,4f,5f,6f}, new CurrentThreadExecutor());
	
		SettableIndexedIterator<Float> iterator = impl.iterator();
		for(int i = 0; i< 6; i++) 
		{
			assertEquals(i+1, (float)iterator.next(),DELTA);
		}
		
	}
	
	@Test
	public void testCreateDataFromValues()
	{
		Impl impl = new Impl(new Float[6], new CurrentThreadExecutor());
		
		BlockedArrayData<Float> data = impl.createDataFromValues(new Float[] {1f,2f,3f,4f,5f,6f});
		
		SettableIndexedIterator<Float> iterator = data.iterator();
		for(int i = 0; i< 6; i++) 
		{
			assertEquals(i+1, (float)iterator.next(),DELTA);
		}
	}
	
	@Test
	public void testSetGet()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());
	
		impl.set(5.5f, 3,2,1);
		int[] phys = data.logicalToPhysical(new int[] {3,2,1});
		assertEquals(5.5f,data.get(phys[0], phys[1]),DELTA);
		
		assertEquals(5.5f,impl.get(3,2,1),DELTA);

		// Sanity check: other values are not 5.5!
		assertTrue(!Precision.equals(5.5f,impl.get(3,2,0),DELTA));
	}
	
	@Test
	public void testGetSetTransposed()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor()).transpose(0, 2);
		
		impl.set(5.5f, 1,2,3);
		int[] phys = data.logicalToPhysical(new int[] {3,2,1});
		assertEquals(5.5f,data.get(phys[0], phys[1]),DELTA);
		
		assertEquals(5.5f,impl.get(1,2,3),DELTA);

		// Sanity check: other values are not 5.5!
		assertTrue(!Precision.equals(5.5f,impl.get(1,2,1),DELTA));
	}
	
	@Test
	public void testGetSetLinear()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());
		
		// 23 is the linear index for 3,2,1 (3*6+2*2 + 1)
		impl.set(5.5f, 23);
		int[] phys = data.logicalToPhysical(new int[] {3,2,1});
		assertEquals(5.5f,data.get(phys[0], phys[1]),DELTA);
		
		assertEquals(5.5f,impl.get(23),DELTA);

		// Sanity check: other values are not 5.5!
		assertTrue(!Precision.equals(5.5f,impl.get(20),DELTA));
	}
	
	@Test
	public void testCopy()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		for(int i=0;i<24;i++) {
			impl.set((float)i+1, i);
		}
		
		Impl copy = impl.copy();
		
		impl.fill(0f);
		assertEquals((float)25*12, copy.sum(), DELTA);
		assertEquals((float)0, impl.sum(), DELTA);
	}
	
	@Test
	public void testGetSetLinearTransposed()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor()).transpose(0, 1);
		
		// 23 is the linear index for 3,2,1 (3*6+2*2 + 1)
		impl.set(5.5f, 23);
		assertEquals(5.5f,impl.get(2,3,1),DELTA);
		
		assertEquals(5.5f,impl.get(23),DELTA);

		// Sanity check: other values are not 5.5!
		assertTrue(!Precision.equals(5.5f,impl.get(20),DELTA));
	}
	
	@Test
	public void testMatrixMultiply()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		GenericBlockedArrayData<Float> data2 = new GenericBlockedArrayData<Float>(new int[] {2,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl2 = new Impl(data2, new CurrentThreadExecutor());

		impl.fill(1f,2f,3f,4f);
		impl2.fill(5f,6f,7f,8f);
				
		Impl product = impl.matrixMultiply(impl2, f -> f*f);

		assertEquals(1*5*5 + 4*49,product.get(0,0), DELTA);
		assertEquals(1*36 + 4*64,product.get(0,1), DELTA);
		assertEquals(9*25+16*49,product.get(1,0), DELTA);
		assertEquals(9*36+16*64,product.get(1,1), DELTA);
	}
	
	@Test
	public void testFillDimensions()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {1,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		impl.fillDimensions(new Float[] {5f, 7f},1);
		
		assertEquals(7f, impl.get(0,1),DELTA);
		assertEquals(5f, impl.get(0,0),DELTA);
		
		GenericBlockedArrayData<Float> data2 = new GenericBlockedArrayData<Float>(new int[] {2},0f,n->new Float[n],n->new Float[n][]);		
		Impl filler = new Impl(data2, new CurrentThreadExecutor());

		filler.set(3f, 0);
		filler.set(4f, 1);
		
		impl.fillDimensions(filler,1);
		
		assertEquals(4f, impl.get(0,1),DELTA);
		assertEquals(3f, impl.get(0,0),DELTA);		
		
	}
	
	@Test
	public void testMatrixMultiplyVector()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		GenericBlockedArrayData<Float> data2 = new GenericBlockedArrayData<Float>(new int[] {2,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl2 = new Impl(data2, new CurrentThreadExecutor());

		impl.fill(1f,2f);
		impl2.fill(5f,6f,7f,8f);
				
		Impl product = impl.matrixMultiply(impl2, f -> f*f);

		assertEquals(1*5*5 + 4*49,product.get(0), DELTA);
		assertEquals(1*36 + 4*64,product.get(1), DELTA);
		
		GenericBlockedArrayData<Float> data3 = new GenericBlockedArrayData<Float>(new int[] {2},0f,n->new Float[n],n->new Float[n][]);		
		Impl vec2 = new Impl(data3, new CurrentThreadExecutor());
		
		vec2.fill(3f,4f);
		
		final Impl result = impl.matrixMultiply(vec2);
		assertEquals(1,result.numberOfElements());
		assertEquals(11f,result.get(0),DELTA);
		
		Impl res2 = impl2.matrixMultiply(vec2,f -> f*f);
		
		assertArrayEquals(new int[] {2,1}, res2.size());
		assertEquals(15*15+24*24,res2.get(0),DELTA);
		assertEquals(21*21+32*32,res2.get(1),DELTA);
		
	}
	
	@Test(expected=MultiDimensionalArrayException.class)
	public void testMatrixMultiply3D()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		GenericBlockedArrayData<Float> data2 = new GenericBlockedArrayData<Float>(new int[] {2,2,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl2 = new Impl(data2, new CurrentThreadExecutor());

		impl.matrixMultiply(impl2, f -> f*f);
	}
	
	@Test(expected=MultiDimensionalArrayException.class)
	public void testMatrixMultiplyNonConformable()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		GenericBlockedArrayData<Float> data2 = new GenericBlockedArrayData<Float>(new int[] {3,2},0f,n->new Float[n],n->new Float[n][]);		
		Impl impl2 = new Impl(data2, new CurrentThreadExecutor());

		impl.matrixMultiply(impl2, f -> f*f);
	}
	
	@Test
	public void testSum()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		for(int i=0;i<24;i++) {
			impl.set((float)i+1, i);
		}
		
		assertEquals((float)25*12, impl.sum(), DELTA);
	}
	
	@Test
	public void testMean()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,5},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		for(int i=0;i<10;i++) {
			impl.set(((float)(i+1))/10, i);
		}
		
		Impl levels = new Impl(new Float[] {1f,2f,3f,4f,5f}, new CurrentThreadExecutor());

		float expected = 0f;
		
		for(float i = 1; i<6;i+=1f) {
			expected += i*i/5f + i/2f;
		}
		
		assertEquals(expected, impl.mean(levels,1), DELTA);
	}
	
	@Test
	public void testSecondMoment()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,5},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		for(int i=0;i<10;i++) {
			impl.set(((float)(i+1))/10, i);
		}
		
		Impl levels = new Impl(new Float[] {1f,2f,3f,4f,5f}, new CurrentThreadExecutor());

		float expected = 0f;
		
		for(float i = 1; i<6;i+=1f) {
			expected += i*i*i/5f + i*i/2f;
		}
		
		assertEquals(expected, impl.secondMoment(levels,1), DELTA);
	}
	
	@Test
	public void testMultiply()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,5},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		for(int i=0;i<10;i++) {
			impl.set(((float)(i+1))/10, i);
		}

		Impl result = (Impl) impl.multiply(10f);
				
		assertEquals(4, result.get(3), DELTA);
		assertEquals(7, result.get(6), DELTA);
	}
	
	@Test
	public void testAdd()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {2,5},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		for(int i=0;i<10;i++) {
			impl.set(((float)(i+1))/10, i);
		}

		Impl result = (Impl) impl.add(10f);
				
		assertEquals(10.4, result.get(3), DELTA);
		assertEquals(10.7, result.get(6), DELTA);
	}
	
	@Test
	public void testMultiplyArrays()
	{
		
		Impl impl = new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		Impl impl2 = new Impl(new Float[] {1f,4f,9f}, new CurrentThreadExecutor());

		Impl res = impl2.multiply(impl);
		
		for(int i=0;i<3;i++) {
			assertEquals(((float)i+1)*((float)i+1)*((float)i+1), res.get(i), DELTA);
		}
	}
	
	@Test
	public void testAddArrays()
	{
		
		Impl impl = new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		Impl impl2 = new Impl(new Float[] {1f,4f,9f}, new CurrentThreadExecutor());

		Impl res = impl2.add(impl);
		
		for(int i=0;i<3;i++) {
			assertEquals(((float)i+1)*((float)i+1)+((float)i+1), res.get(i), DELTA);
		}
	}
	
	@Test
	public void testDivideArrays()
	{
		
		Impl impl = new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		Impl impl2 = new Impl(new Float[] {1f,4f,9f}, new CurrentThreadExecutor());

		Impl res = impl2.divide(impl);
		
		for(int i=0;i<3;i++) {
			assertEquals(((float)i+1)*((float)i+1)/((float)i+1), res.get(i), DELTA);
		}
	}
	
	@Test
	public void testSubtractArrays()
	{
		
		Impl impl = new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		Impl impl2 = new Impl(new Float[] {1f,4f,9f}, new CurrentThreadExecutor());

		Impl res = impl2.subtract(impl);
		
		for(int i=0;i<3;i++) {
			assertEquals(((float)i+1)*((float)i+1)-((float)i+1), res.get(i), DELTA);
		}
	}
	
	@Test
	public void testSetMatching()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());
		
		impl.set(1f, 10);
		
		// All except the one set above should be set since all are 0
		impl.setMatching(Float.valueOf(0f), 2.2f);
		
		assertEquals(1f, impl.get(10), DELTA);
		assertEquals(2.2f, impl.get(9), DELTA);
		assertEquals(2.2f*23 + 1f, impl.sum(), DELTA);
	}
	
	@Test
	public void testToArrayPlain()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {4,3,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());
		
		float[] expected = new float[24];
		
		for(int i = 0; i<24;i++) {
			impl.set((float)i+1,i);
			expected[i] = i+1;
		}
		impl.toArray();
		
		assertArrayEquals(expected, ArrayUtils.toPrimitive(impl.toArray()),DELTA);
	}
	
	@Test
	public void testToArrayTransposed()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(5,new int[] {5,2},0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());
		
		for(int i = 0; i<10;i++) {
			impl.set((float)i+1,i);
		}
		
		assertArrayEquals(new float[] {1f,3f,5f,7f,9f,2f,4f,6f,8f,10f}, ArrayUtils.toPrimitive(impl.transpose(0,1).toArray()),DELTA);
	}
	
	@Test
	public void testCreatePointWiseMultivaluedResult()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {5},0f,n->new Float[n],n->new Float[n][]);
		Impl impl = new Impl(data, new CurrentThreadExecutor());
		Impl other1 = new Impl(new Float[5], new CurrentThreadExecutor());
		Impl other2 = new Impl(new Float[5], new CurrentThreadExecutor());

		Impl[] arrays = impl.createPointWiseMultiValuedResult(v -> new Float[2], new Impl[] {other1,other2});
		
		assertEquals(2, arrays.length);
		assertArrayEquals(new int[] {5}, arrays[0].size());
		assertArrayEquals(new int[] {5}, arrays[1].size());		
	}
	
	@Test(expected=NumericsException.class)
	public void testCreatePointWiseMultivaluedResultWrapsExecption()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {5},0f,n->new Float[n],n->new Float[n][]);
		Impl impl = new Impl(data, new CurrentThreadExecutor());
		Impl other1 = new Impl(new Float[5], new CurrentThreadExecutor());
		Impl other2 = new Impl(new Float[5], new CurrentThreadExecutor());

		impl.createPointWiseMultiValuedResult(v ->{throw new UnsupportedOperationException();}, new Impl[] {other1,other2});
		fail("Should not reach here");
	}
	
	@Test(expected=MultiDimensionalArrayException.class)
	public void testFillException()
	{
		Impl impl = new Impl(new Float[2], new CurrentThreadExecutor());
		
		impl.fill(new Float[3]);
	}
	
	@Test
	public void testFillNoException()
	{
		Impl impl = new Impl(new Float[2], new CurrentThreadExecutor());
		
		impl.fill(new Float[] {4f,2f});
		
		assertEquals(4f,impl.get(0),DELTA);
		assertEquals(2f,impl.get(1),DELTA);
	}
	
	@Test
	public void testFillRepeated()
	{
		Impl impl = new Impl(new Float[4], new CurrentThreadExecutor());

		impl.fill(new Float[] {4f,2f});
		
		assertEquals(4f,impl.get(0),DELTA);
		assertEquals(2f,impl.get(1),DELTA);
		assertEquals(4f,impl.get(2),DELTA);
		assertEquals(2f,impl.get(3),DELTA);				
	}
	
	@Test
	public void testCreateReduceAction()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {10000,2},
				0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		Impl target = new Impl(new Float[10000], new CurrentThreadExecutor());
		
		int[] count = new int[] {0};
		
		Reduction<Float, RuntimeException> reduce = iter -> {
			synchronized (count)
			{
				count[0]++;
				return (float)count[0];
			}
		};
		
		// Can't test it directly easily, but this call is expected to traverse eacg row once
		List<? extends SettableIterator<Float>> parallelIterators = impl.parallelIterators(new int[] {1});
		
		parallelIterators.forEach(iterator ->  {
			ComputableRecursiveAction reduceAction = impl.createReduceAction(reduce, iterator, target);
			reduceAction.call();
		});
		
		// So we should get 10000 calls...
		assertEquals(10000,count[0]);
		
		// The highest result should be 10000
		assertEquals((float)10000,target.max(),DELTA);
		// And the lowest 1 (i.e. all rows have been utilised, none have been passed over)
		assertEquals((float)1,target.min(),DELTA);
	}
	
	@Test(expected=MultiDimensionalArrayException.class)
	public void testCreateReduceActionWrapsException()
	{
		GenericBlockedArrayData<Float> data = new GenericBlockedArrayData<Float>(new int[] {10000,2},
				0f,n->new Float[n],n->new Float[n][]);
		
		Impl impl = new Impl(data, new CurrentThreadExecutor());

		Impl target = new Impl(new Float[10000], new CurrentThreadExecutor());
		
		Reduction<Float, Exception> reduce = iter -> {
			throw new Exception("The Mother of All Exceptions");
		};
		
		// Can't test it directly easily, but this call is expected to traverse eacg row once
		List<? extends SettableIterator<Float>> parallelIterators = impl.parallelIterators(new int[] {1});
		
		parallelIterators.forEach(iterator ->  {
			ComputableRecursiveAction reduceAction = impl.createReduceAction(reduce, iterator, target);
			reduceAction.call();
		});
	}
	
	@Test
	public void testEquals()
	{
		Impl a= new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		Impl b= new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		Impl c= new Impl(new Float[] {1f,2f,3f,4f}, new CurrentThreadExecutor());
		Impl d= new Impl(new Float[] {1f,4f,3f}, new CurrentThreadExecutor());
		Impl e= new Impl(new Float[] {1f,4f}, new CurrentThreadExecutor());
		Impl f= new Impl(new Float[] {null,4f}, new CurrentThreadExecutor());
		Impl g= new Impl(new Float[] {null,4f}, new CurrentThreadExecutor());
		
		assertEquals(a,b);
		assertEquals(b,a);
		assertTrue(!a.equals(5));
		assertTrue(!a.equals(c));
		assertTrue(!c.equals(a));
		assertTrue(!a.equals(d));
		assertEquals(f,g);
		assertEquals(g,f);
		assertTrue(!f.equals(e));
		assertTrue(!e.equals(f));
	}
	
	@Test
	public void testGetSecondOperandOp()
	{
		Impl a= new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		
		NaryOp<Float, Float, RuntimeException> op = a.getSecondOperandOp();
		
		assertEquals(4f,op.perform(3f,4f),DELTA);
		assertEquals(3f,op.perform(4f,3f),DELTA);
	}
	
	@Test
	public void testFirstLast()
	{
		Impl a= new Impl(new Float[] {1f,2f,3f}, new CurrentThreadExecutor());
		
		assertEquals(3f,a.last(),DELTA);
		assertEquals(1f,a.first(),DELTA);
	}
}
