package com.meliorbis.numerics.generic.impl;

import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Test;

import static com.meliorbis.numerics.IntArrayFactories.*;
import com.meliorbis.numerics.generic.IntegerArray;

public class IntegerArrayTest
{

	@Test
	public void testBasicOps()
	{
		IntegerArray<?> a = createIntArray(3,4);
		IntegerArray<?> b = createIntArray(1,2);
		
		assertEquals(createIntArray(4,6),a.add(b));
		assertEquals(createIntArray(3,8),a.multiply(b));
		assertEquals(createIntArray(2,2),a.subtract(b));
		assertEquals(createIntArray(3,2),a.divide(b));
		
		assertEquals(createIntArray(2,3),a.add(((com.meliorbis.numerics.generic.impl.IntegerArray)a).getMinusOne()));
		assertEquals(createIntArray(6,8),a.multiply(new Integer(2)));
		assertEquals(createIntArray(-1,0),a.subtract(new Integer(4)));
		assertEquals(createIntArray(1,1),a.divide(new Integer(3)));
		
		Comparator<Integer> comparator = a.getComparator();
		
		assertTrue(comparator.compare(1,2) < 0);
		assertTrue(comparator.compare(101,97) > 0);
		assertTrue(comparator.compare(10,10) == 0);
		assertTrue(comparator.compare(null,10) < 0);
		assertTrue(comparator.compare(10,null) > 0);
		
	}

}
