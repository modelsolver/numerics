package com.meliorbis.numerics.generic.primitives;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DoubleNaryOpTest
{
	private static final double DELTA = 1e-15;

	@Test
	public void testConvertsObjectsToPrimitives()
	{
		final double[] calledWith = new double[2];
		
		
		DoubleNaryOp<RuntimeException> op = new DoubleNaryOp<RuntimeException>()
		{
			@Override
			public double perform(double... inputs_) throws RuntimeException
			{
				System.arraycopy(inputs_, 0, calledWith, 0, inputs_.length);
				return 0;
			}
		};
		
		op.perform(new Double[] {Double.valueOf(.1), Double.valueOf(.2)});
		
		assertEquals(.1, calledWith[0], DELTA);
		assertEquals(.1, calledWith[0], DELTA);
	}
	
	private static final class TestException extends Exception
	{
		
	}
	
	@Test(expected=TestException.class)
	public void testBubblesException() throws TestException
	{
		DoubleNaryOp<TestException> op = new DoubleNaryOp<TestException>()
		{
			@Override
			public double perform(double... inputs_) throws TestException
			{
				throw new TestException();
			}
		};
		
		op.perform(new Double[] {Double.valueOf(.1), Double.valueOf(.2)});	
	}
}
