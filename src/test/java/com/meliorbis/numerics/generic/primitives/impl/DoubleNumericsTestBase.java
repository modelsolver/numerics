/**
 * 
 */
package com.meliorbis.numerics.generic.primitives.impl;

import com.meliorbis.numerics.DoubleNumerics;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.NumericsTestBase;

/**
 * Base class for tests of things using double numerics
 * 
 * @author Tobias Grasl
 */
public abstract class DoubleNumericsTestBase extends NumericsTestBase
{
	protected static final double ALLOWED_ERROR = 1e-15;
	protected static DoubleNumerics _numerics;
	
	protected DoubleArray<?> createArray(double... doubles_)
	{
		return _numerics.getArrayFactory().newArray(doubles_);	
	}

	protected DoubleArray<?> createArray(int... size_)
	{
		return _numerics.getArrayFactory().newArray(size_);	
	}
}
