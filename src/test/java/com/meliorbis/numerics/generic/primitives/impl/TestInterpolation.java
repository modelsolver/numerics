package com.meliorbis.numerics.generic.primitives.impl;

import static com.meliorbis.numerics.generic.primitives.impl.Interpolation.interpDimension;
import static com.meliorbis.numerics.generic.primitives.impl.Interpolation.interpSingleDiscontinuous;
import static com.meliorbis.numerics.generic.primitives.impl.Interpolation.interpolateFunction;
import static com.meliorbis.numerics.generic.primitives.impl.Interpolation.interpolateFunctionAcross;
import static org.junit.Assert.*;

import org.junit.Test;

import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIndexedIterator;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIterator;
import com.meliorbis.numerics.generic.primitives.DoubleSubspaceSplitIterator;
import com.meliorbis.numerics.generic.primitives.impl.Interpolation.Callback;
import com.meliorbis.numerics.generic.primitives.impl.Interpolation.Params;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;

public class TestInterpolation extends NumericsTestBase
{
	/**
	 * Tests that points lying a the lower x bound receive the lower y value
	 * 
	 * @throws MultiDimensionalArrayException
	 */
	@Test
	public void testInterpolateFunctionAtLowerBound()
	{
		DoubleArray<?> x = _numerics.getArrayFactory().newArray(2,1);
		DoubleArray<?> y = _numerics.getArrayFactory().newArray(2,1);
		DoubleArray<?> targetX = _numerics.getArrayFactory().newArray(3,1);
		
		DoubleSettableIndexedIterator xIter = x.iterator();
		DoubleSettableIndexedIterator yIter = y.iterator();
		
		int i = 1;
		while(xIter.hasNext())
		{
			xIter.nextDouble();
			yIter.nextDouble();
			
			xIter.set((double) i);
			yIter.set((double) 2*i++);
		}
		
		DoubleSettableIndexedIterator targetIter = targetX.iterator();
		
		double target = 1;
		while(targetIter.hasNext())
		{
			targetIter.nextDouble();
			
			targetIter.set(target);
			
			target += .5d;
		}
				
		DoubleArray<?> interpResults = interpolateFunction(x, y, 0, targetX, new Params().constrained());
		
		targetX.fill(new double[]{2d,3d,4d});
		// The one that is before the value on the grid should be 0
		ArrayAssert.assertEquals(targetX, interpResults, MAX_ERROR);
	}
	
	@Test
	public void testInterpolateFunctionAcross()
	{
		DoubleArray<?> x = _numerics.getArrayFactory().newArray(2,2);
		DoubleArray<?> y = _numerics.getArrayFactory().newArray(2,2);
		DoubleArray<?> targetX = _numerics.getArrayFactory().newArray(2,3,3);
		
		DoubleSettableIndexedIterator xIter = x.iterator();
		DoubleSettableIndexedIterator yIter = y.iterator();
		
		int i = 1;
		while(xIter.hasNext())
		{
			xIter.nextDouble();
			yIter.nextDouble();
			
			xIter.set((double) i);
			yIter.set((double) i++);
		}
		
		DoubleSubspaceSplitIterator targetIter = targetX.iteratorAcross(new int[]{0,1});
		
		double target = 0.5;
		while(targetIter.hasNext())
		{
			targetIter.nextDouble();
			
			DoubleSettableIndexedIterator orth = targetIter.getOrthogonalIterator();
			
			while(orth.hasNext())
			{
				orth.nextDouble();
				orth.set(target);
			}
			
			target += 1d;
		}
				
		DoubleArray<?> interpResults = interpolateFunctionAcross(x, y, 1, targetX, new Params().constrained(), 2 );
		
		
		
		// The one that is before the value on the grid should be at the lower bound
		targetX.set(1d, 0,0,0);
		targetX.set(1d, 0,0,1);
		targetX.set(1d, 0,0,2);
		
		ArrayAssert.assertEquals(targetX, interpResults, MAX_ERROR);
		
		x = _numerics.getArrayFactory().newArray(2);
		y = _numerics.getArrayFactory().newArray(2);
		targetX = _numerics.getArrayFactory().newArray(3,2);
		
		xIter = x.iterator();
		yIter = y.iterator();
		
		i = 1;
		while(xIter.hasNext())
		{
			xIter.nextDouble();
			yIter.nextDouble();
			
			xIter.set((double) i);
			yIter.set((double) i++);
		}
		
		DoubleSettableIndexedIterator newTargetIter = targetX.iterator();
		
		target = 0.5;
		while(newTargetIter.hasNext())
		{
			newTargetIter.nextDouble();
			
			newTargetIter.set(target);
			
			target += 1d;
		}
		
		
		interpResults = interpolateFunctionAcross(x, y, 0, targetX, new Params().constrained(), 1);
		
		DoubleArray<?> expected = _numerics.getArrayFactory().newArray(targetX.size());
		expected.fill(new double[]{1d,1.5d,2.5d,3.5d,4.5d,5.5d});
		
		ArrayAssert.assertEquals(expected, interpResults,MAX_ERROR);
	}
	
	@Test
	public void testInterpolateFunctionTo1D()
	{
		DoubleArray<?> x = _numerics.getArrayFactory().newArray(2,2);
		DoubleArray<?> y = _numerics.getArrayFactory().newArray(2,2);
		DoubleArray<?> targetX = _numerics.getArrayFactory().newArray(3);
		
		DoubleSettableIndexedIterator xIter = x.iterator();
		DoubleSettableIndexedIterator yIter = y.iterator();
		
		int i = 1;
		while(xIter.hasNext())
		{
			xIter.nextDouble();
			yIter.nextDouble();
			
			xIter.set((double) i);
			yIter.set((double) i++);
		}
		
		targetX.fill(1d,2d,3d);
				
		DoubleArray<?> interpResults = interpolateFunction(x, y, 1, targetX, new Params().constrained());
		
		assertArrayEquals(new int[] {2,3},interpResults.size());
		
		DoubleArray<?> expected = _numerics.getArrayFactory().newArray(2,3);
		// First one should map as is
		expected.at(0).fill(targetX);
		
		// second one should be constrained to 3
		expected.at(1).fill(createArray(3d,3d,3d));
		ArrayAssert.assertEquals(expected, interpResults, MAX_ERROR);
	}
	
	@Test
	public void testInterpolateFunction()
	{
		DoubleArray<?> x = _numerics.getArrayFactory().newArray(2,2);
		DoubleArray<?> y = _numerics.getArrayFactory().newArray(2,2);
		DoubleArray<?> targetX = _numerics.getArrayFactory().newArray(2,3);
		
		DoubleSettableIndexedIterator xIter = x.iterator();
		DoubleSettableIndexedIterator yIter = y.iterator();
		
		int i = 1;
		while(xIter.hasNext())
		{
			xIter.nextDouble();
			yIter.nextDouble();
			
			xIter.set((double) i);
			yIter.set((double) i++);
		}
		
		DoubleSettableIndexedIterator targetIter = targetX.iterator();
		
		double target = 0.5;
		while(targetIter.hasNext())
		{
			targetIter.nextDouble();
			
			targetIter.set(target);
			
			target += 1d;
		}
				
		DoubleArray<?> interpResults = interpolateFunction(x, y, 1, targetX, new Params().constrained());
		
		// The one that is before the value on the grid should be 1 (the lower bound)
		targetX.set(1d, 0,0);
		ArrayAssert.assertEquals(targetX, interpResults, MAX_ERROR);

		DoubleArray<?> expandedTarget = _numerics.getArrayFactory().newArray(2,3,3);
		expandedTarget.fillDimensions(targetX, 0,1);
		expandedTarget = expandedTarget.across(2).multiply(createArray(.9,1d,1.1));
		
		ArrayAssert.assertEquals(targetX,expandedTarget.at(-1,-1,1),MAX_ERROR);
		
		DoubleArray<?> interpResults2 = interpolateFunction(x, y, 1, expandedTarget.at(-1,-1,1), new Params().constrained());
		
		ArrayAssert.assertEquals(interpResults,interpResults2,MAX_ERROR);
	}
	
	@Test
	public void testInterp1d()
	{
		DoubleArray<?> ys = _numerics.getArrayFactory().newArray(10);
		DoubleArray<?> xs = _numerics.getArrayFactory().newArray(10);
		
		DoubleSettableIterator yIter = ys.iterator();
		DoubleSettableIterator xIter = xs.iterator();
		
		for(double d = 0.1d;d < 1.01;d+=0.1d)
		{
			yIter.nextDouble();
			xIter.nextDouble();
			yIter.set(Math.pow(d,2));
			xIter.set(d);
		}
		
		DoubleArray<?> interpValue = interpDimension(ys,xs, 0.55d, 0);
		
		int[] size = interpValue.size();
		assertEquals(1,size.length);
		assertEquals(1,size[0]);
		assertEquals(0.305,interpValue.get(0),MAX_ERROR);
	}

	@Test
	public void testInterp()
	{
		DoubleArray<?> ys = _numerics.getArrayFactory().newArray(4,2);
		ys.fillDimensions(_numerics.getArrayFactory().newArray(
				new double[]{	1.1,2.1,
								1.5,3.5,
								2.9,5.9,
								3.5,7.5
				}),0,1);
		
		DoubleArray<?> xs = _numerics.getArrayFactory().newArray(new double[]{1d,1.5});
		
		DoubleArray<?> interp = interpDimension(ys, xs, 1.3,1);
		
		assertEquals(1,interp.numberOfDimensions());
		assertEquals(4,interp.size()[0]);
		
		assertEquals(0.6*2.1+0.4*1.1,interp.get(0),MAX_ERROR);
		assertEquals(0.6*3.5+0.4*1.5,interp.get(1),MAX_ERROR);
		assertEquals(0.6*5.9+0.4*2.9,interp.get(2),MAX_ERROR);
		assertEquals(0.6*7.5+0.4*3.5,interp.get(3),MAX_ERROR);
	}
	
	@Test
	public void testDoubleInterp()
	{
		DoubleArray<?> ys = _numerics.getArrayFactory().newArray(2,2,2);
		ys.fillDimensions(_numerics.getArrayFactory().newArray(
				new double[]{	1.1,2.1,
								1.5,3.5,
								2.9,5.9,	
								3.5,7.5
				}),0,1,2);
		
		DoubleArray<?> x1s = _numerics.getArrayFactory().newArray(new double[]{1d,1.5});
		DoubleArray<?> x2s = _numerics.getArrayFactory().newArray(new double[]{1d,5d});
		
		DoubleArray<?> interpolated = Interpolation.interp(ys,Interpolation.spec(1, x1s, 1.1),Interpolation.spec(2,x2s,3));
		DoubleArray<?> altInterp = interpDimension(interpDimension(ys, x2s, 3d, 2),x1s,1.1,1);
		
		int[] size = interpolated.size();
		
		assertEquals(1, size.length);
		assertEquals(2, size[0]);
		
		assertEquals(altInterp.get(0),interpolated.get(0), MAX_ERROR);
		assertEquals(altInterp.get(1),interpolated.get(1), MAX_ERROR);
	}
	
	@Test
	public void interpDiscontinuousHandlesDiscontinuity()
	{
 		DoubleArray<?> yTarget = createArray(3);
 		DoubleArray<?> xTarget = createArray(2.5,3.5,3.75);
 		
 		DoubleArray<?> xSource = createArray(1d,2d,4d,5d,6d);
 		DoubleArray<?> ySource = createArray(10d,4d,6d,8d,9d);
 		
 		DoubleArray<?> discontinuities = createArray(3d);
 		
		interpSingleDiscontinuous(yTarget, xTarget.iterator(), ySource.iterator(), xSource.iterator(), discontinuities.iterator(), false, false, Callback.NOOP);
		
		assertEquals(1d, yTarget.get(0), MAX_ERROR);
		assertEquals(5d, yTarget.get(1), MAX_ERROR);
		assertEquals(5.5d, yTarget.get(2), MAX_ERROR);
	}
	
	@Test
	public void interpDiscontinuousHandlesDiscontinuityOnGrid()
	{
 		DoubleArray<?> yTarget = createArray(3);
 		DoubleArray<?> xTarget = createArray(2.5,3.5,4.5);
 		
 		DoubleArray<?> xSource = createArray(1d,2d,4d,5d,6d);
 		DoubleArray<?> ySource = createArray(10d,4d,6d,8d,9d);
 		
 		DoubleArray<?> discontinuities = createArray(4d);
 		
		interpSingleDiscontinuous(yTarget, xTarget.iterator(), ySource.iterator(), xSource.iterator(), discontinuities.iterator(), false, false, Callback.NOOP);
		
		assertEquals(1d, yTarget.get(0), MAX_ERROR);
		assertEquals(-5d, yTarget.get(1), MAX_ERROR);
		assertEquals(7.5d, yTarget.get(2), MAX_ERROR);
	}
	
	@Test
	public void interpDiscontinuousHandlesNormal()
	{
 		DoubleArray<?> yTarget = createArray(2);
 		DoubleArray<?> xTarget = createArray(4d/3d,2.5,3.5,4.5);
 		
 		DoubleArray<?> xSource = createArray(1d,2d,4d,5d);
 		DoubleArray<?> ySource = createArray(10d,4d,6d,8d);
 		
 		DoubleArray<?> discontinuities = createArray(3d);
 		
		interpSingleDiscontinuous(yTarget, xTarget.iterator(), ySource.iterator(), xSource.iterator(), discontinuities.iterator(), false, false, Callback.NOOP);
		
		assertEquals(8d, yTarget.get(0), MAX_ERROR);
		assertEquals(1d, yTarget.get(1), MAX_ERROR);
		assertEquals(5d, yTarget.get(2), MAX_ERROR);
		assertEquals(7d, yTarget.get(3), MAX_ERROR);
	}
	
	@Test
	public void interpDiscontinuousHandlesMultiplePreDisc()
	{
 		DoubleArray<?> yTarget = createArray(2);
 		DoubleArray<?> xTarget = createArray(4d/3d,2.5,8d/3d,3.5,4.5);
 		
 		DoubleArray<?> xSource = createArray(1d,2d,4d,5d);
 		DoubleArray<?> ySource = createArray(10d,4d,6d,8d);
 		
 		DoubleArray<?> discontinuities = createArray(3d);
 		
		interpSingleDiscontinuous(yTarget, xTarget.iterator(), ySource.iterator(), xSource.iterator(), discontinuities.iterator(), false, false, Callback.NOOP);
		
		assertEquals(8d, yTarget.get(0), MAX_ERROR);
		assertEquals(1d, yTarget.get(1), MAX_ERROR);
		assertEquals(0d, yTarget.get(2), MAX_ERROR);
		assertEquals(5d, yTarget.get(3), MAX_ERROR);
		assertEquals(7d, yTarget.get(4), MAX_ERROR);
	}
	
	@Test
	public void interpDiscontinuousMultiD()
	{
 		DoubleArray<?> xTarget = createArray(2,2);
 				
 		xTarget.fill(4d/3d,3.5,2.5,4.5);
 		
 		DoubleArray<?> xSource = createArray(4,2);
 		
 		xSource.at(-1, 0).fill(1d,2d,4d,5d);
 		xSource.at(-1, 1).fill(1d,2d,5d,6d);
 	
 		DoubleArray<?> ySource = createArray(4,2);
 		
 		ySource.fillDimensions(new double[] {10d,4d,6d,8d},0);
 		
 		DoubleArray<?> discontinuities = createArray(2,1);
 		
 		discontinuities.fill(3d,4d);
 		
 		Params params = new Params().withDiscontinuities(idx -> discontinuities.iteratorAt(idx));
 		
 		DoubleArray<?> yTarget =  interpolateFunction(xSource, ySource, 0 , xTarget, params);
		
		assertEquals(10 + (4-10)/3, yTarget.get(0,0), MAX_ERROR);
		assertEquals(4 + (4-10)/2, yTarget.get(1,0), MAX_ERROR);
		
		assertEquals(4 + (4-10)*1.5, yTarget.get(0,1), MAX_ERROR);
		assertEquals(6 - (8-6)*.5, yTarget.get(1,1), MAX_ERROR);
	}
}
