package com.meliorbis.numerics.generic.primitives.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.math3.util.Precision;
import org.junit.Test;

import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleIndexedReduction;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIndexedIterator;
import com.meliorbis.numerics.generic.primitives.DoubleSubspaceSplitIterator;
import com.meliorbis.numerics.generic.primitives.DoubleUnaryOp;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;
import com.meliorbis.utils.Pair;
import com.meliorbis.utils.Utils;

public class TestPrimitiveDoubleArrayFunctions extends NumericsTestBase {

	
	
	@Test
	public void testPullToMean()
	{
		// This one has an assymetric distance from the mean...
		DoubleArray<?> input = createArray(0d,1d,2d,3.5d,3.5d);
		
		DoubleArray<?> pulled = DoubleArrayFunctions.pullToMean(input, .5);
		
		// ...so the outermost point moves too
		ArrayAssert.assertEquals(createArray(0d,1.25d,2d,3.5-0.125*1.5,3.5-0.125*1.5),pulled,MAX_ERROR);

		// This one is symmetric...
		input = createArray(0d,1d,2d,3d,4d);
		
		pulled = DoubleArrayFunctions.pullToMean(input, .5);
		
		// ...so the outermost point should not move
		ArrayAssert.assertEquals(createArray(0d,1.25d,2d,2.75,4d),pulled,MAX_ERROR);

	
	}
	
	@Test
	public void testGrad()
	{
		DoubleArray<?> ys = _numerics.getArrayFactory().newArray(4,2);
		ys.fillDimensions(_numerics.getArrayFactory().newArray(
				new double[]{	1.1,2.1,
								1.5,3.5,
								2.9,5.9,
								3.5,7.5
				}),0,1);
		
		DoubleArray<?> xs = createArray(1d,1.5,3d,5d);
		
		DoubleArray<?> grad = DoubleArrayFunctions.grad(ys, xs, 0);
		
		assertEquals(2,grad.numberOfDimensions());
		assertEquals(4,grad.size()[0]);
		assertEquals(2,grad.size()[1]);
		
		assertEquals(0.8,grad.get(0,0),MAX_ERROR);
		assertEquals(2.8,grad.get(0,1),MAX_ERROR);
		assertEquals(0.6+0.93333333333333333333333/4,grad.get(1,0),MAX_ERROR);
		assertEquals(2.8*1.5/2d + 1.6*0.5/2d,grad.get(1,1),MAX_ERROR);
		assertEquals(1.4/1.5*2d/3.5+0.3*1.5/3.5,grad.get(2,0),MAX_ERROR);
		assertEquals((1.6*2+0.8*1.5)/3.5,grad.get(2,1),MAX_ERROR);
		assertEquals(.6/2,grad.get(3,0),MAX_ERROR);
		assertEquals(1.6/2,grad.get(3,1),MAX_ERROR);
	}
	
	@Test
	public void testWeightedHarmonicGrad()
	{
		DoubleArray<?> ys = _numerics.getArrayFactory().newArray(4,2);
		ys.fillDimensions(_numerics.getArrayFactory().newArray(
				new double[]{	1.1,2.1,
								1.5,2.1,
								2.9,5.9,
								3.5,7.5
				}),0,1);
		
		DoubleArray<?> xs = _numerics.getArrayFactory().newArray(new double[]{1d,1.5,3d,5d});
		
		DoubleArray<?> grad = DoubleArrayFunctions.gradWeigthedHarmonic(ys, xs, 0);
		
		assertEquals(2,grad.numberOfDimensions());
		assertEquals(4,grad.size()[0]);
		assertEquals(2,grad.size()[1]);
		
		assertEquals(0.8,grad.get(0,0),MAX_ERROR);
		assertEquals(0d,grad.get(0,1),MAX_ERROR);
		assertEquals(0d,grad.get(1,1),MAX_ERROR);
	}
	
	@Test
	public void testWeightedHarmonicGradDifferentXs()
	{
		DoubleArray<?> ys = _numerics.getArrayFactory().newArray(4,2);
		ys.fillDimensions(_numerics.getArrayFactory().newArray(
				new double[]{	1.1,2.1,
								1.5,2.1,
								2.9,5.9,
								3.5,7.5
				}),0,1);
		
		DoubleArray<?> xs = _numerics.getArrayFactory().newArray(4,2);
		xs.fillDimensions(_numerics.getArrayFactory().newArray(
				new double[]{	1d,2d,
								1.5,3d,
								3d,6d,
								5d,10d
				}),0,1);
		
		DoubleArray<?> grad = DoubleArrayFunctions.gradWeigthedHarmonic(ys, xs, 0);
		
		assertEquals(2,grad.numberOfDimensions());
		assertEquals(4,grad.size()[0]);
		assertEquals(2,grad.size()[1]);
		
		assertEquals(0.8,grad.get(0,0),MAX_ERROR);
		assertEquals(0d,grad.get(0,1),MAX_ERROR);
		assertEquals(0.4d,grad.get(3,1),MAX_ERROR);
	}
	
	@Test
	public void testCombineTransitions()
	{
		final DoubleArray<?> first = _numerics.getArrayFactory().newArray(3,3);
		final DoubleArray<?> second = _numerics.getArrayFactory().newArray(2,2,2,2);
		
		first.fill(
				.1,.2,.7,
				.3,.4,.3,
				.4,.5,.1
		);
		
		second.fill(
				.1,.2,.3,.4,
				.2,.3,.4,.1,
				.3,.4,.15,.15,
				.4,.5,.1,0d
		);
		
		DoubleArray<?> result = DoubleArrayFunctions.combineTransitions(first, second);
		
		result.across(0, 3).reduce(new DoubleIndexedReduction<RuntimeException>()
		{
			@Override
			public double perform(DoubleSubspaceSplitIterator iterator_) throws RuntimeException
			{
				while(iterator_.hasNext())
				{
					double resValue = iterator_.nextDouble();
					
					assertEquals(first.get(iterator_.getIndex()) * second.get(iterator_.getOtherIndex()), resValue, MAX_ERROR);
				}
				
				return Double.NaN;
			}
		});
	}
	
	@Test
	public void testNoFixedPoint()
	{
		DoubleArray<?> x = createArray(1d,2d);
		DoubleArray<?> y = createArray(2d,3d);
		
		DoubleArray<?> fixedPoints = DoubleArrayFunctions.interpolateFixedPoints(x, y, 0);
		
		assertEquals(Double.NaN,fixedPoints.get(0), MAX_ERROR);
	}
	
	@Test
	public void testFixedPoint()
	{
		DoubleArray<?> x = createArray(1d,2d);
		DoubleArray<?> y = createArray(1.5d,1.5d);
		
		DoubleArray<?> fixedPoints = DoubleArrayFunctions.interpolateFixedPoints(x, y, 0);
		
		assertEquals(1.5,fixedPoints.get(0), MAX_ERROR);
	}
	
	@Test
	public void testFixedPointArray()
	{
		DoubleArray<?> x = createArray(0d,1d,2d,3d,5d);
		
		DoubleArray<?> y = _numerics.getArrayFactory().newArray(5,2);
		y.at(-1,0).fill(new double[]{0.25d,1.25d,1.75d,2d,3d});
		y.at(-1,1).fill(new double[]{2.5d,2.5d,2.5d,2.5d,2.5d});
		
		
		DoubleArray<?> fixedPoints = DoubleArrayFunctions.interpolateFixedPoint(y, x, 0);
		ArrayAssert.assertEquals(createArray(1.5,2.5),fixedPoints,MAX_ERROR);
	}
	
	@Test
	public void testMultipleFixedPoints()
	{
		DoubleArray<?> x = createArray(1d,2d, 3d);
		DoubleArray<?> y = createArray(1.1d,1.9d,9.9d);
		
		DoubleArray<?> fixedPoints = DoubleArrayFunctions.interpolateFixedPoints(x, y, 0);
		
		assertEquals(2d+0.1/7d,fixedPoints.get(0), MAX_ERROR);
	}
	
	@Test
	public void testLog()
	{
		DoubleArray<?> x = _numerics.getArrayFactory().newArray(2,3,4);
		
		DoubleSettableIndexedIterator iterator = x.iterator();
		
		while (iterator.hasNext())
		{
			iterator.nextDouble();
			// insert random number between 1 and 101
			iterator.set(Math.random()*100+1);
		}
		
		DoubleArray<?> logVals = x.map(DoubleArrayFunctions.log);
	
		iterator = x.iterator();
		DoubleSettableIndexedIterator logIterator = logVals.iterator();
		
		while (iterator.hasNext())
		{
			double original = iterator.nextDouble();
			double logVal = logIterator.nextDouble();
			
			assertEquals(Math.log(original),logVal,MAX_ERROR);
		}
	}
	
	@Test
	public void testExp()
	{
		DoubleArray<?> x = _numerics.getArrayFactory().newArray(2,3,4);
		
		DoubleSettableIndexedIterator iterator = x.iterator();
		
		while (iterator.hasNext())
		{
			iterator.nextDouble();
			// insert random number between -50 and 50
			iterator.set(Math.random()*100-50);
		}
		
		DoubleArray<?> expVals = x.map(DoubleArrayFunctions.exp);
	
		iterator = x.iterator();
		DoubleSettableIndexedIterator expIterator = expVals.iterator();
		
		while (iterator.hasNext())
		{
			double original = iterator.nextDouble();
			double expVal = expIterator.nextDouble();
			
			assertEquals(Math.exp(original),expVal,MAX_ERROR);
		}
	}
	
	@Test
	public void testNLogSequence()
	{
		double[] sequence = Utils.sequence(0d, Math.log(Math.log(100+1)+1), 200);
		
		DoubleArray<?> logValues = createArray(sequence);
		
		logValues.modifying().map(new DoubleUnaryOp<RuntimeException>(){

			@Override
			public double perform(double input_)
			{
				return Math.exp(Math.exp(input_)-1d)-1d;
			}
			});
		
		ArrayAssert.assertEquals(logValues, DoubleArrayFunctions.nLogSequence(100d, 200, 2), MAX_ERROR);
	}

    @Test
    public void testInterpolateFixedPointSimple()
    {
        /* In this test, y(n) only changes in the diretion of x(n), so that a single Newton iteration should
         * suffice
         */
        final DoubleArray<?> y = _numerics.getArrayFactory().newArray(3, 3, 3, 3);

        final DoubleArray<?> x1 = createArray(1d, 2d, 3d);
        final DoubleArray<?> x2 = createArray(3d, 5d, 7d);
        final DoubleArray<?> x3 = createArray(7d, 8d, 9d);

        y.at(-1,-1,-1,0).fillDimensions(DoubleArrayFunctions.squeezeTo(x1, 2.5, .5), 0);
        y.at(-1,-1,-1,1).fillDimensions(DoubleArrayFunctions.squeezeTo(x2, 4.5, .5), 1);
        y.at(-1,-1,-1,2).fillDimensions(DoubleArrayFunctions.squeezeTo(x3, 8, .5), 2);

        final DoubleArray<?> result = DoubleArrayFunctions.interpolateFixedPoint(y,
                new DoubleArray<?>[]{x1, x2, x3});

        assertEquals(3,result.numberOfElements());
        assertEquals(2.5,result.get(0),1e-6);
        assertEquals(4.5,result.get(1),1e-6);
        assertEquals(8, result.get(2), 1e-6);


        ArrayAssert.assertEquals(
                Interpolation.interp(y, Interpolation.spec(0, x1, result.get(0)), Interpolation.spec(1, x2, result.get(1)), Interpolation.spec(2, x3, result.get(2))),
                    result,1e-6);

    }

    @Test
    public void testInterpolateFixedPoint()
    {
        final DoubleArray<?> y = _numerics.getArrayFactory().newArray(3, 3, 3, 3);

        final DoubleArray<?> x1 = createArray(1d, 2d, 3d);
        final DoubleArray<?> x2 = createArray(3d, 5d, 7d);
        final DoubleArray<?> x3 = createArray(7d, 8d, 9d);

        y.at(-1,-1,-1,0).fillDimensions(DoubleArrayFunctions.squeezeTo(x1, 2.5, .5), 0);
        y.at(-1,-1,-1,1).fillDimensions(DoubleArrayFunctions.squeezeTo(x2, 4.5, .5), 1);
        y.at(-1,-1,-1,2).fillDimensions(DoubleArrayFunctions.squeezeTo(x3, 8, .5), 2);

        y.modifying().across(0).multiply(createArray(0.9,1d,1.1));

        final DoubleArray<?> result = DoubleArrayFunctions.interpolateFixedPoint(y, new DoubleArray<?>[]{x1, x2, x3});

        ArrayAssert.assertEquals(
                Interpolation.interp(y, Interpolation.spec(0, x1, result.get(0)), Interpolation.spec(1, x2, result.get(1)), Interpolation.spec(2, x3, result.get(2))),
                result,1e-6);
    }

    @Test
    public void testJacobeanAtPoint()
    {
        /* In this test, y(n) only changes in the diretion of x(n), so that a single Newton iteration should
         * suffice
         */
        final DoubleArray<?> y = _numerics.getArrayFactory().newArray(3, 3, 3, 3);

        final DoubleArray<?> x1 = createArray(1d, 2d, 3d);
        final DoubleArray<?> x2 = createArray(3d, 5d, 7d);
        final DoubleArray<?> x3 = createArray(7d, 8d, 9d);

        y.at(-1,-1,-1,0).fillDimensions(x1,0);
        y.at(-1,-1,-1,1).fillDimensions(x2.multiply(2d), 1);
        y.at(-1, -1, -1, 2).fillDimensions(x3.multiply(3d), 2);

        Pair<DoubleArray<?>, DoubleArray<?>> result = DoubleArrayFunctions.jacobeanAtPoint(y,
                new DoubleArray<?>[]{x1, x2, x3}, new double[]{1.5, 4d, 8.5});

        final DoubleArray<?> expected = _numerics.getArrayFactory().newArray(3, 3);

        expected.set(1d,0,0);
        expected.set(2d,1,1);
        expected.set(3d,2,2);

        ArrayAssert.assertEquals(
                expected,
                result.getLeft(), MAX_ERROR);

        y.modifying().across(0).multiply(createArray(2d, 1d, (2d/3d)));


        result = DoubleArrayFunctions.jacobeanAtPoint(y,
                new DoubleArray<?>[]{x1, x2, x3}, new double[]{1.5, 4d, 8.5});

        final DoubleArray<?> jacobean = result.getLeft();

        // In the x1 dimension, the gradient of the first should be cancelled, and the other two should no longer be zero
        assertEquals(0d, jacobean.get(0, 0), MAX_ERROR);
        assertTrue(!Precision.equals(0d, jacobean.get(1, 0), MAX_ERROR));
        assertTrue(!Precision.equals(0d, jacobean.get(2,0),MAX_ERROR ));

        // In the x2 and x3 dimension, the gradients against the other two should still be 0
        assertEquals(0d,jacobean.get(0,1),MAX_ERROR);
        assertEquals(0d,jacobean.get(2,1),MAX_ERROR);
        assertEquals(0d,jacobean.get(0,2),MAX_ERROR);
        assertEquals(0d,jacobean.get(1,2),MAX_ERROR);
    }
	
}
