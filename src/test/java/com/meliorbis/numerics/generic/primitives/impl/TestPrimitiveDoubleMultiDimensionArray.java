package com.meliorbis.numerics.generic.primitives.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleMultiValuedNaryOp;
import com.meliorbis.numerics.generic.primitives.DoubleReduction;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIterator;
import com.meliorbis.numerics.generic.primitives.DoubleUnaryOp;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.threading.CurrentThreadExecutor;
import com.meliorbis.numerics.threading.Executor;
import com.meliorbis.utils.Utils;

public final class TestPrimitiveDoubleMultiDimensionArray
{
	private static final double DELTA = 1e-15;
	private Executor _executor = new CurrentThreadExecutor();
	
	@Test
	public void testEquals() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10, 5, 4, 18);

		array.set(14.2, 3, 3, 3, 3);

		assertTrue(array.equals(array));

		DoubleArray array2 = new DoubleArray(_executor, 10, 5, 4, 18);

		array2.set(14.2, 3, 3, 3, 3);

		assertTrue(array.equals(array2));

		array2.set(0.1, 0, 0, 0, 0);

		assertTrue(!array.equals(array2));

		assertTrue(!array.equals(null));

		assertTrue(!array.equals(9));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testStack() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 10, 5, 4, 18);
		DoubleArray array2 = new DoubleArray(_executor, 10, 5, 4, 18);
		DoubleArray array3 = new DoubleArray(_executor, 10, 5, 4, 18);

		array1.set(1d, 3, 3, 3, 3);
		array2.set(2d, 4, 4, 2, 4);
		array3.set(3d, 5, 4, 1, 5);

		DoubleArray stacked = (DoubleArray) array1.stack(array2, array3);

		assertEquals(1d, stacked.get(3, 3, 3, 3, 0), DELTA);
		assertEquals(2d, stacked.get(4, 4, 2, 4, 1), DELTA);
		assertEquals(3d, stacked.get(5, 4, 1, 5, 2), DELTA);

		assertEquals(6d, stacked.sum(), DELTA);

		DoubleArray array4 = new DoubleArray(_executor, 10, 5, 4);

		boolean caughtException = false;

		try
		{
			array1.stack(array2, array4);
		} catch (MultiDimensionalArrayException e)
		{
			caughtException = true;
		}

		assertTrue(caughtException);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testStackFinal() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 10, 5, 4, 18,1);
		DoubleArray array2 = new DoubleArray(_executor, 10, 5, 4, 18,1);
		DoubleArray array3 = new DoubleArray(_executor, 10, 5, 4, 18,1);

		array1.set(1d, 3, 3, 3, 3,0);
		array2.set(2d, 4, 4, 2, 4,0);
		array3.set(3d, 5, 4, 1, 5,0);

		DoubleArray stacked = (DoubleArray) array1.stackFinal(array2, array3);

		assertArrayEquals(new int[] {10,5,4,18,3}, stacked.size());
		assertEquals(1d, stacked.get(3, 3, 3, 3, 0), DELTA);
		assertEquals(2d, stacked.get(4, 4, 2, 4, 1), DELTA);
		assertEquals(3d, stacked.get(5, 4, 1, 5, 2), DELTA);

		assertEquals(6d, stacked.sum(), DELTA);

		DoubleArray array4 = new DoubleArray(_executor, 10, 5, 4);

		boolean caughtException = false;

		try
		{
			array1.stack(array2, array4);
		} catch (MultiDimensionalArrayException e)
		{
			caughtException = true;
		}

		assertTrue(caughtException);

	}

	@Test
	public void testUnsetIsZero() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10, 5, 4, 18);

		assertEquals(0d, array.get(4, 3, 3, 10), 1e-15);
	}

	@Test
	public void testGetsWhatIsSet() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10, 5, 4, 18);

		array.set(14.2, 3, 3, 3, 3);

		// Check that the value we set is returned
		assertEquals("Set value not returned", 14.2, array.get(3, 3, 3, 3), DELTA);

		// Check that other values are still 0
		assertEquals(0d, array.get(4, 3, 3, 10), 1e-15);
	}

	@Test(expected = MultiDimensionalArrayException.class)
	public void testIncorrectDimensionsThrowException() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10, 5, 4, 18);
		array.set(0.0, 1, 2);
	}

	@Test
	public void testPointWiseProduct() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3, 4, 5);
		DoubleArray array2 = new DoubleArray(_executor, 3, 4, 5);

		array1.set(3d, 2, 2, 2);
		array2.set(2d, 2, 2, 2);
		array2.set(1d, 1, 2, 3);

		DoubleArray result = array1.multiply(array2);

		assertEquals(6d, result.get(2, 2, 2), 1e-15);
		assertEquals(0d, result.get(1, 2, 3), 1e-15);
		assertEquals(0d, result.get(2, 2, 1), 1e-15);

		result = array1.multiply(7d);

		assertEquals(21d, result.get(2, 2, 2), 1e-15);
		assertEquals(0d, result.get(1, 2, 3), 1e-15);
		assertEquals(0d, result.get(2, 2, 1), 1e-15);

		result = array1.multiply(0d);

		assertEquals(0d, result.get(2, 2, 2), 1e-15);
		assertEquals(0d, result.get(1, 2, 3), 1e-15);
		assertEquals(0d, result.get(2, 2, 1), 1e-15);
	}
	
	@Test
	public void testFillAcrossFromSub()
	{
		DoubleArray array1 = new DoubleArray(_executor, 3, 4, 5, 7);
		DoubleArray array2 = new DoubleArray(_executor, 4, 6, 7);
		
		array1.fillDimensions(array2.at(-1,0),1,3);
	}
	

	@Test
	public void testModifying() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3, 4, 5);
		DoubleArray array2 = new DoubleArray(_executor, 3, 4, 5);

		array1.set(3d, 2, 2, 2);
		array2.set(2d, 2, 2, 2);
		array2.set(1d, 1, 2, 3);

		array1.modifying().multiply(array2);

		assertEquals(6d, array1.get(2, 2, 2), 1e-15);
		assertEquals(0d, array1.get(1, 2, 3), 1e-15);
		assertEquals(0d, array1.get(2, 2, 1), 1e-15);

		array1.modifying().multiply(7d);

		assertEquals(42d, array1.get(2, 2, 2), 1e-15);
		assertEquals(0d, array1.get(1, 2, 3), 1e-15);
		assertEquals(0d, array1.get(2, 2, 1), 1e-15);

		array1.modifying().subtract(12d);

		assertEquals(30d, array1.get(2, 2, 2), 1e-15);
		assertEquals(-12d, array1.get(1, 2, 3), 1e-15);
		assertEquals(-12d, array1.get(2, 2, 1), 1e-15);

		array1.modifying().multiply(0d);

		assertEquals(0d, array1.get(2, 2, 2), 1e-15);
		assertEquals(0d, array1.get(1, 2, 3), 1e-15);
		assertEquals(0d, array1.get(2, 2, 1), 1e-15);

        // Check that modifying followed by across works
        DoubleArray array3 = new DoubleArray(_executor, 2,2);
        DoubleArray array4 = new DoubleArray(_executor, 2);

        array3.fill(Utils.sequence(1d,4d,4));
        array4.fill(2d,3d);

        array3.modifying().across(1).multiply(array4);

        DoubleArray expected = new DoubleArray(_executor, 2,2);
        expected.fill(2d,6d,6d,12d);
        ArrayAssert.assertEquals(expected, array3,DELTA);

        // reset, and try across followed by modifying
        array3.fill(Utils.sequence(1d,4d,4));
        array3.across(1).modifying().multiply(array4);
        
        DoubleArray array = new DoubleArray(_executor, 10, 5, 4, 18);

		array.set(14.2, 3, 3, 3, 3);
		array.modifying().add(1d);
		
		assertEquals(15.2, array.get(3,3,3,3), DELTA);
		array.modifying().divide(8d);
		assertEquals(15.2/8d, array.get(3,3,3,3), DELTA);
	}

	@Test
	public void testPointWiseProductAcross() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3, 4, 5);
		DoubleArray array2 = new DoubleArray(_executor, 4, 5);

		array1.set(3d, 2, 2, 2);
		array2.set(2d, 2, 2);
		array2.set(1d, 2, 3);

		DoubleArray result = array1.across(1, 2).multiply(array2);

		assertEquals(6d, result.get(2, 2, 2), 1e-15);
		assertEquals(0d, result.get(1, 2, 3), 1e-15);
		assertEquals(0d, result.get(2, 2, 1), 1e-15);
	}

	@Test(expected=MultiDimensionalArrayException.class)
	public void testFill()
	{
		DoubleArray array1 = new DoubleArray(_executor, 2,2);
		
		array1.fill(1d,2,3,4);
		
		assertEquals(1d, array1.get(0),DELTA);
		assertEquals(2d, array1.get(1),DELTA);
		assertEquals(3d, array1.get(2),DELTA);
		assertEquals(4d, array1.get(3),DELTA);
		
		array1.fill(2d,3d);
		
		assertEquals(2d, array1.get(0),DELTA);
		assertEquals(3d, array1.get(1),DELTA);
		assertEquals(2d, array1.get(2),DELTA);
		assertEquals(3d, array1.get(3),DELTA);

		array1.fill(2d,3d,4d);
	}
	
	@Test
	public void testDivide()
	{
		DoubleArray array1 = new DoubleArray(_executor, 2,2);
		
		array1.fill(1d,2,3,4);
		
		DoubleArray result = array1.divide(4);
		
		assertEquals(1d/4, result.get(0),DELTA);
		assertEquals(2d/4, result.get(1),DELTA);
		assertEquals(3d/4, result.get(2),DELTA);
		assertEquals(1d, result.get(3),DELTA);
	}
	
	@Test
	public void testSubtract()
	{
		DoubleArray array1 = new DoubleArray(_executor, 2,2);
		
		array1.fill(1d,2,3,4);
		
		DoubleArray result = array1.subtract(4);
		
		assertEquals(-3, result.get(0),DELTA);
		assertEquals(-2, result.get(1),DELTA);
		assertEquals(-1, result.get(2),DELTA);
		assertEquals(0, result.get(3),DELTA);
	}

	@Test
	public void testAdd() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3, 4, 5 );
		DoubleArray array2 = new DoubleArray(_executor, 3, 4, 5 );

		array1.set(3d, 2, 2, 2);
		array2.set(2d, 2, 2, 2);
		array2.set(1d, 1, 2, 3);

		DoubleArray result = array1.add(array2);

		assertEquals(5d, result.get(2, 2, 2), 1e-15);
		assertEquals(1d, result.get(1, 2, 3), 1e-15);
		assertEquals(0d, result.get(2, 2, 1), 1e-15);

		result = array1.add(7d);

		assertEquals(10d, result.get(2, 2, 2), 1e-15);
		assertEquals(7d, result.get(1, 2, 3), 1e-15);

		result = array1.add(0d);

		assertEquals(3d, result.get(2, 2, 2), 1e-15);
		assertEquals(0d, result.get(1, 2, 3), 1e-15);
	}

	@Test
	public void testCreateFromData() throws MultiDimensionalArrayException
	{
		double[] inputData = new double[]
		{ 1.2, 1.3, 1.4, 1.5 };
		DoubleArray array = new DoubleArray(inputData, _executor);

		int[] sizes = array.size();
		assertEquals(1, sizes.length);
		assertEquals(inputData.length, sizes[0]);

		for (int i = 0; i < inputData.length; i++)
		{
			assertEquals(inputData[i], array.get(i), 1e-15);
		}
	}

	@Test
	public void testFillDimensions() throws MultiDimensionalArrayException
	{
		double[] inputData = new double[]
		{ 1.2, 1.3, 1.4, 1.5 };

		DoubleArray inputArray = new DoubleArray(inputData, _executor);
		DoubleArray array = new DoubleArray(_executor, 4, 3);

		array.fillDimensions(inputArray, 0);

		for (int i = 0; i < inputData.length; i++)
		{
			assertEquals(inputData[i], array.get(i, i % 3), DELTA);
		}

		DoubleArray threeD = new DoubleArray(_executor, 6, 5, 2);

		// Now we fill along two dimensions which have different sizes than
		// source, and are not consecutive in the target
		threeD.fillDimensions(array, 0, 2);

		assertEquals(1.4d, threeD.get(3, 3, 1), 1e-15);
		assertEquals(1.4d, threeD.get(3, 2, 1), 1e-15);
		assertEquals(1.4d, threeD.get(4, 3, 0), 1e-15);
		assertEquals(1.5d, threeD.get(4, 0, 1), 1e-15);
		assertEquals(1.2d, threeD.get(0, 0, 1), 1e-15);
		
		DoubleArray twoD = new DoubleArray(_executor, 1,2);
		twoD.fillDimensions(new double[] {1d,2d},1);
		
		assertEquals(1d, twoD.get(0,0), DELTA);
		assertEquals(2d, twoD.get(0,1), DELTA);
		
	}

	@Test
	public void testMatrixMultiply() throws MultiDimensionalArrayException
	{
		DoubleArray A = new DoubleArray(_executor, 3, 2);
		DoubleArray B = new DoubleArray(_executor, 2, 4);

		A.fillDimensions(new DoubleArray(new Double[]
		{ 1d, 2d, 3d, 4d, 5d, 6d }, _executor), 0, 1);
		B.fillDimensions(new DoubleArray(new Double[]
		{ 7d, 8d, 9d, 10d, 11d, 12d, 13d, 14d }, _executor), 0, 1);

		DoubleArray result = A.matrixMultiply(B);

		int[] size = result.size();

		assertEquals(2, size.length);
		assertEquals(3, size[0]);
		assertEquals(4, size[1]);

		assertEquals(29d, result.get(0, 0), 1e-15);
		assertEquals(65d, result.get(1, 0), 1e-15);
		assertEquals(72d, result.get(1, 1), 1e-15);
		assertEquals(123d, result.get(2, 2), 1e-15);
	}

	@Test
	public void testMatrixVectorMultiply() throws MultiDimensionalArrayException
	{
		DoubleArray A = new DoubleArray(_executor, 3);
		A.fill(new double[]
		{ 1d, 2d, 3d });
		DoubleArray B = new DoubleArray(_executor, 3);
		B.fill(new double[]
		{ 1d, 2d, 3d });

		DoubleArray product = A.matrixMultiply(B);

		assertArrayEquals(new int[]
		{ 1 }, product.size());
		assertEquals(14d, product.get(0), DELTA);

		DoubleArray C = new DoubleArray(_executor, 2, 3);
		C.fill(new double[]
		{ 1d, 2d, 3d, 4d, 5d, 6d });

		product = C.matrixMultiply(A);

		assertArrayEquals(new int[]
		{ 2, 1 }, product.size());
		assertEquals(14d, product.get(0), DELTA);
		assertEquals(32d, product.get(1), DELTA);
	}

	@Test
	public void testMatrixMultiplyTransposed() throws MultiDimensionalArrayException
	{
		DoubleArray A = new DoubleArray(_executor, 3, 2);
		DoubleArray B = new DoubleArray(_executor, 2, 4);

		// Sets the whole matrix to minus inf
		B.setMatching(0d, Double.NEGATIVE_INFINITY);
		A.fillAt(new DoubleArray(new double[]

		{ 1, 1 }, _executor), 0);

		DoubleArray nonTransposed = A.matrixMultiply(B);

		assertTrue(Double.isInfinite(nonTransposed.get(0, 0)));
		assertTrue(Double.isNaN(nonTransposed.get(1, 1)));

		DoubleArray transposed = A.matrixMultiply(B, (DoubleUnaryOp<RuntimeException>) value_ -> Double.isNaN(value_) ? 0d : value_);

		assertTrue(Double.isInfinite(transposed.get(0, 0)));
		assertEquals(0d, transposed.get(1, 1), DELTA);
	}

	@Test
	public void testTranspose()
	{
		DoubleArray A = new DoubleArray(_executor, 3, 2);

		A.fillDimensions(new DoubleArray(new Double[]
		{ 1d, 2d, 3d, 4d, 5d, 6d }, _executor), 0, 1);

		DoubleArray B = (DoubleArray) A.transpose(0, 1);

		int[] size = B.size();

		assertEquals(2, size.length);
		assertEquals(2, size[0]);
		assertEquals(3, size[1]);

		assertEquals(3d, B.get(0, 1), 1e-15);
		assertEquals(6d, B.get(1, 2), 1e-15);

		B.set(17d, 1, 2);

		assertEquals(17d, B.get(1, 2), 1e-15);

		assertEquals(17d, A.get(2, 1), 1e-15);

		DoubleArray product = A.matrixMultiply(B);

		assertEquals(314d, product.get(2, 2), DELTA);
		assertEquals(83d, product.get(2, 1), DELTA);
		assertEquals(11d, product.get(0, 1), DELTA);
		
		DoubleArray C = new DoubleArray(_executor, 2, 2, 2, 2);
		
		C.fill(Utils.sequence(1d, 16d, 16));
		
		DoubleArray D = C.arrangeDimensions(new int[]{2,1,0});
		
		assertEquals(16d, D.max(), DELTA);
		
		DoubleArray transposedSelf = A.transpose(1, 1);
		
		ArrayAssert.assertEquals(A, transposedSelf, DELTA);
		
		
	}

	@Test
	public void testMin() throws MultiDimensionalArrayException
	{
		DoubleArray threeD = new DoubleArray(_executor, 6, 5, 2);

		for (int i = 0; i < 60; i++)
		{
			threeD.set(-(-Math.pow(i / 10 - i % 10, 2) + i / 10), i);
		}

		DoubleArray maxValues = threeD.across(1).min();

		int[] size = maxValues.size();
		assertEquals(2, size.length);

		// Check that the actual dimension sizes are correct
		assertEquals(6, size[0]);
		assertEquals(2, size[1]);

		assertEquals(0d, maxValues.get(0, 0), DELTA);
		assertEquals(-2d, maxValues.get(2, 0), DELTA);

		// Now do a min over all dimensions by not specifying them
		double minVal = threeD.min();

		assertEquals(-5d, minVal, DELTA);
	}

	@Test
	public void testMax() throws MultiDimensionalArrayException
	{
		DoubleArray threeD = new DoubleArray(_executor, 6, 5, 2);

		for (int i = 0; i < 60; i++)
		{
			threeD.set(-Math.pow(i / 10 - i % 10, 2) + i / 10, i);
		}

		DoubleArray maxValues = threeD.across(1).max();

		int[] size = maxValues.size();
		assertEquals(2, size.length);

		// Check that the actual dimension sizes are correct
		assertEquals(6, size[0]);
		assertEquals(2, size[1]);

		assertEquals(0d, maxValues.get(0, 0), DELTA);
		assertEquals(2d, maxValues.get(2, 0), DELTA);

		maxValues = threeD.across(1,2).max();

		size = maxValues.size();
		assertEquals(1, size.length);

		assertEquals(0d, maxValues.get(0), DELTA);
		assertEquals(2d, maxValues.get(2), DELTA);

		// Now do a max over all dimensions by not specifying them
		Double max = threeD.max();
		assertEquals(5d, max, DELTA);
	}

	@Test
	public void testMaxLarge() throws MultiDimensionalArrayException
	{
		DoubleArray threeD = new DoubleArray(_executor, 300, 7);

		DoubleSettableIterator iterator = (DoubleSettableIterator) threeD.iterator();

		while (iterator.hasNext())
		{
			iterator.nextDouble();
			iterator.set(Math.random());
		}

		DoubleArray max = threeD.across(0).max();

		for (int i = 0; i < 7; i++)
		{
			double maxValue = Double.NEGATIVE_INFINITY;

			for (int j = 0; j < 300; j++)
			{
				double current = threeD.get(j, i);

				if (current > maxValue)
				{
					maxValue = current;
				}
			}

			assertEquals("Incorrect Max", maxValue, max.get(i), 1e-15);
		}
	}

	@Test
	public void testFillAt()
	{
		DoubleArray threeD = new DoubleArray(_executor, 6, 5, 2);

		threeD.fillAt(new DoubleArray(new double[]
		{ 1.1, 1.2, 1.3, 1.4, 1.5 }, _executor), 3, -1, 1);

		assertEquals(1.4, threeD.get(3, 3, 1), DELTA);
		assertEquals(1.2, threeD.get(3, 1, 1), DELTA);
		assertEquals(0d, threeD.get(2, 3, 1), DELTA);

		DoubleArray fourD = new DoubleArray(_executor, 300, 7, 2, 12);

		fourD.at(-1, -1, 0, 0).fillDimensions(new double[]
		{ 1d, 2d, 3d, 4d, 5d, 6d, 7d },1);
		
		assertEquals(3d, fourD.get(101, 2, 0, 0), DELTA);
	}
	
	
	@Test
	public void testSetMatching() throws MultiDimensionalArrayException
	{
		DoubleArray threeD = new DoubleArray(_executor, 6, 5, 2);

		// Fill some values
		threeD.fillAt(new DoubleArray(new Double[]
		{ 1.1, 1.2, 1.3, 1.4, 1.5 }, _executor), 3, -1, 1);

		// Now set those matching 0 to -1
		threeD.setMatching(0d, -1d);

		assertEquals(1.4, threeD.get(3, 3, 1), DELTA);
		assertEquals(1.2, threeD.get(3, 1, 1), DELTA);
		assertEquals(-1d, threeD.get(2, 3, 1), DELTA);

		// Checks that null blocks are matched with zero
		DoubleArray oneD = new DoubleArray(_executor, 3, 10);
		oneD.set(1.2, 0);
		oneD.set(1.3, 9);

		oneD.setMatching(0d, 1d);

		assertEquals(1.2, oneD.get(0), DELTA);
		assertEquals(1.3, oneD.get(9), DELTA);
		assertEquals(1d, oneD.get(2), DELTA);
		assertEquals(1d, oneD.get(3), DELTA);
		assertEquals(1d, oneD.get(4), DELTA);
		assertEquals(1d, oneD.get(5), DELTA);
		assertEquals(1d, oneD.get(6), DELTA);
		assertEquals(1d, oneD.get(7), DELTA);
		assertEquals(1d, oneD.get(8), DELTA);
	}

	@Test
	public void testSum() throws MultiDimensionalArrayException
	{
		DoubleArray threeD = new DoubleArray(_executor, 3, 2, 2);

		// Fill some values
		threeD.fillDimensions(new DoubleArray(new Double[]
		{ 1d, 2d, 3d, 4d, 5d, 6d, 7d, 8d, 9d, 10d, 11d, 12d }, _executor), 0, 1, 2);

		DoubleArray sum = threeD.across(0,1,2).sum();

		assertEquals(1, sum.numberOfElements());
		assertEquals(12d * 13d / 2d, (double) sum.get(0), DELTA);
		// Check that not passing any dimensions sums over all of them
		assertEquals(sum.get(0), threeD.sum(), DELTA);

		sum = threeD.across(2).sum();

		assertEquals(6, sum.numberOfElements());
		assertEquals(3d, (double) sum.get(0, 0), DELTA);
		assertEquals(11d, (double) sum.get(1, 0), DELTA);
		assertEquals(23d, (double) sum.get(2, 1), DELTA);

		DoubleArray twoD = new DoubleArray(_executor, 11, 1);

		twoD.fill(new double[]
		{ 1d, 2d, 3d, 4d, 5d, 6d, 7d, 8d, 9d, 10d, 11d });

		// Check that not passing any dimensions sums over the whole thing
		assertEquals(66d, twoD.sum(), DELTA);
		assertEquals(66d, twoD.across(0,1).sum().get(0), DELTA);

	}
	
	@Test
	public void testMultiMap()
	{
		DoubleArray a = new DoubleArray(_executor, 3);
		
		a.fill(new double[]{1d,2d,3d});
		
		DoubleArray[] results = a.map((DoubleMultiValuedNaryOp<RuntimeException>)(double[] x) -> {
			return new double[]{x[0]*x[0],x[0]*x[0]*x[0]};
		});
		
		DoubleArray b = new DoubleArray(_executor, 3);
		
		b.fill(new double[]{1d,4d,9d});
		
		DoubleArray c = new DoubleArray(_executor, 3);
		
		c.fill(new double[]{1d,8d,27d});
		
		ArrayAssert.assertEquals(b, results[0],DELTA);
		ArrayAssert.assertEquals(c, results[1],DELTA);		
	}
	
	@Test
	public void testMultiMapAcross()
	{
		DoubleArray a = new DoubleArray(_executor, 2,3);
		
		a.fill(new double[]{1,2,3,4,5,6});
		
		DoubleArray b = new DoubleArray(_executor, 3);
		
		b.fill(new double[]{1d,4d,9d});
		
		@SuppressWarnings("unchecked")
		DoubleArray[] results = a.across(1).with(b).map((DoubleMultiValuedNaryOp<RuntimeException>)(double[] x) -> {
			return new double[]{x[0]*x[1],x[0]+x[1]};
		});
		
		
		
		DoubleArray c = new DoubleArray(_executor, 2, 3);
		
		c.fill(new double[]{1,8,27,4,20,54});
		
		ArrayAssert.assertEquals(c, results[0],DELTA);		
		
		DoubleArray d = new DoubleArray(_executor, 2, 3);
		
		d.fill(new double[]{2,6,12,5,9,15});
		
		ArrayAssert.assertEquals(d, results[1],DELTA);		
	}
	// @Test
	// public void testMean() throws MultiDimensionalArrayException
	// {
	// DoubleArray threeD = new DoubleArray(_executor, 2,2,2);
	//
	// // Fill some values
	// threeD.fill(new DoubleArray(new
	// Double[]{0.125,0.25,0.3125,0.0625,0.0625,0.0625,0.0625,0.0625},_executor));
	//
	// // Sanity check the configuration
	// assertEquals(1d, threeD.sum(),ALLOWED_ERROR);
	//
	// assertEquals(0.75*4d+0.25*10d,threeD.mean(new DoubleArray(new
	// Double[]{4d,10d},_executor),0),ALLOWED_ERROR);
	//
	// assertEquals(0.5*17d+0.5*-5d,threeD.mean(new DoubleArray(new
	// Double[]{17d,-5d},_executor),1),ALLOWED_ERROR);
	//
	// assertEquals(4.5/8d*11d+3.5/8d*12d,threeD.mean(new DoubleArray(new
	// Double[]{11d,12d},_executor),2),ALLOWED_ERROR);
	// }
	//
	// @Test
	// public void testVariance() throws MultiDimensionalArrayException
	// {
	// DoubleArray threeD = new DoubleArray(_executor, 2,2,2);
	//
	// // Fill some values
	// threeD.fill(new DoubleArray(new
	// Double[]{0.125,0.25,0.3125,0.0625,0.0625,0.0625,0.0625,0.0625},_executor));
	//
	// // Sanity check the configuration
	// assertEquals(1d, threeD.sum(),ALLOWED_ERROR);
	//
	// assertEquals(0.75*4d*4d+0.25*10d*10d,threeD.secondMoment(new
	// DoubleArray(new Double[]{4d,10d},_executor),0),ALLOWED_ERROR);
	//
	// assertEquals(0.5*17d*17d+0.5*5d*5d,threeD.secondMoment(new
	// DoubleArray(new Double[]{17d,-5d},_executor),1),ALLOWED_ERROR);
	//
	// assertEquals(4.5/8d*11d*11d+3.5/8d*12d*12d,threeD.secondMoment(new
	// DoubleArray(new Double[]{11d,12d},_executor),2),ALLOWED_ERROR);
	// }

	@Test
	public void testReduce() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10, 5, 4, 18);
		array.fill(1d);
		
		DoubleReduction<RuntimeException> reduction = iterator -> {
			double val = 0;
			
			while(iterator.hasNext()) {
				val += iterator.nextDouble();
			}
			
			return val;
		};
		
		double sum = array.reduce(reduction);
		
		assertEquals(array.numberOfElements(), sum, DELTA);
		
		sum = array.at(-1,-1,2,-1).reduce(reduction);
		
		assertEquals(array.numberOfElements()/4, sum, DELTA);

	}
}
