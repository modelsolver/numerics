package com.meliorbis.numerics.generic.primitives.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIterator;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.threading.Executor;
import com.meliorbis.numerics.threading.MultiThreadedExecutor;
import com.meliorbis.utils.Utils;

public final class TestSubArray
{
	private static final double ALLOWED_ERROR = 1e-15;
	private final Executor _executor = new MultiThreadedExecutor();
	
	@Test
	public void testSubSub() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 2,2,2);
		array.fill(new double[]{0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8});
		
		DoubleArray subArray = array.at(-1,0,1).at(1);
		
		int[] size = subArray.size();
		assertEquals(1,size.length);
		assertEquals(1,size[0]);
		assertEquals(0.6d, subArray.get(0), 0d);
	}

    @Test
    public void testSubSubAdd() throws MultiDimensionalArrayException
    {
		DoubleArray array = new DoubleArray(_executor, 20,10,5,2);

        array.set(1d,10,5,2,1);

        final DoubleArray other = new DoubleArray(_executor, 20, 2);

        other.fill(Utils.sequence(1d,40d,40));

        array.at(-1,5).at(-1, 2).modifying().add(other);

        assertEquals(23d, array.get(10,5,2,1),0d);
        assertEquals(21d, array.get(10,5,2,0),0d);

    }
	
	@Test
	public void testSubSubSpaceOp() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 2,2,2);
		array.fill(new double[]{0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8});
		
		DoubleArray other = new DoubleArray(_executor, 2);
		other.fill(new double[]{2d,3d});
		
		DoubleArray result = array.at(-1,-1,1).across(1).divide(other);
		
		assertArrayEquals(new int[]{2,2},result.size());
		
		assertEquals(0.1d, result.get(0,0), ALLOWED_ERROR);
		assertEquals(0.4d/3d, result.get(0,1), ALLOWED_ERROR);
		assertEquals(0.3d, result.get(1,0), ALLOWED_ERROR);
		assertEquals(0.8d/3d, result.get(1,1), ALLOWED_ERROR);
	}
	
	@Test
	public void testSubIteratorAt() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 2,2,2);
		array.fill(new double[]{0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8});
		
		DoubleArray copy = array.copy();
		copy.set(100d,0,0,1);
		copy.set(200d,1,0,1);
		
		DoubleSettableIterator iteratorAt = (DoubleSettableIterator) array.at(-1,-1,1).at(-1,0).iterator();
		
		iteratorAt.nextDouble();
		iteratorAt.set(100d);
		iteratorAt.nextDouble();
		iteratorAt.set(200d);
		
		ArrayAssert.assertEquals(copy, array,ALLOWED_ERROR);
	}
	
	@Test
	public void testUnsetIsZero() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10,5,4,18);
		DoubleArray subArray = array.at(6,3,-1,-1);
		assertEquals(0d,subArray.get(3,10),1e-15);
	}

	@Test
	public void testGetsWhatIsSet() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10,5,4,18);
		
		array.set(14.2,3,3,3,3);
		
		DoubleArray subArray = array.at(3,3,-1,-1);

		
		// Check that the value we set is returned
		assertEquals("Set value not returned", 14.2, subArray.get(3,3), ALLOWED_ERROR);
		
		// Check that other values are still 0
		assertEquals(0d,subArray.get(3,10),1e-15);
		
		// Set something through the subArray
		subArray.set(17.3,3,12);
		
		// Check that the value we set is returned
		assertEquals("Set value not returned", 17.3, subArray.get(3,12), ALLOWED_ERROR);
		assertEquals("Set value not returned by full array", 17.3, array.get(3,3,3,12), ALLOWED_ERROR);
		
		DoubleArray subArray2 = array.at(-1,2,3,-1);
		subArray2.set(1.1,1,2);
		assertEquals(1.1,array.get(1,2,3,2),ALLOWED_ERROR);
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void testIncorrectDimensionsThrowException() throws MultiDimensionalArrayException
	{
		DoubleArray array = new DoubleArray(_executor, 10,5,4,18);
		DoubleArray subArray2 = array.at(-1,2,3,-1);
		subArray2.set(1.1,1,3,2);
		
	}
	
	@Test
	public void testPointWiseProduct() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3,3,4,5);
		DoubleArray array2 = new DoubleArray(_executor, 3,4,5);
		
		array1.set(3d,2,1,2,2);
		array2.set(2d,2,2,2);
		array2.set(1d,1,2,3);
		
		DoubleArray result = array1.at(-1, 1,-1,-1).multiply(array2);
		
		assertEquals(6d, result.get(2,2,2),1e-15);
		assertEquals(0d, result.get(1,2,3),1e-15);
		assertEquals(0d, result.get(2,2,1),1e-15);
		
		array1 = array1.at(-1,1,-1,-1);
		result = array1.multiply(7d);
		
		assertEquals(21d, result.get(2,2,2),1e-15);
		assertEquals(0d, result.get(1,2,3),1e-15);
		assertEquals(0d, result.get(2,2,1),1e-15);
		
		result = array1.multiply(0d);
		
		assertEquals(0d, result.get(2,2,2),1e-15);
		assertEquals(0d, result.get(1,2,3),1e-15);
		assertEquals(0d, result.get(2,2,1),1e-15);
	}
	
	@Test
	public void testModifying() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3,3,4,5);
		DoubleArray array2 = new DoubleArray(_executor, 3,4,5);
		
		array1.set(3d,2,1,2,2);
		array2.set(2d,2,2,2);
		array2.set(1d,1,2,3);
		
		DoubleArray array1Copy = array1.copy();
		
		DoubleArray result = array1.at(-1, 1,-1,-1).multiply(array2);
		
		ArrayAssert.assertEquals(array1Copy, array1, ALLOWED_ERROR);
		
		array1.at(-1, 1,-1,-1).modifying().multiply(array2);
		
		array1Copy.fillAt(result,-1,1,-1,-1);
		
		ArrayAssert.assertEquals(array1Copy, array1, ALLOWED_ERROR);
	}
	
	@Test
	public void testMatrixMultiply() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor, 3,3,4,5);
		DoubleArray array2 = new DoubleArray(_executor, 3,4,5);
		
		array1.set(3d,2,1,2,2);
		array2.set(2d,1,2,3);
		//array2.set(1d,1,2,3);
		
		DoubleArray product = array1.at(-1,-1,2,2).matrixMultiply(array2.at(-1,-1,3));
		
		assertEquals(2,product.size().length);
		assertEquals(3,product.size()[0]);
		assertEquals(4,product.size()[1]);
		assertEquals(6d,product.get(2,2),ALLOWED_ERROR);
		
		assertEquals(6d,product.sum(),ALLOWED_ERROR);
	}
	
	@Test
	public void testAdd() throws MultiDimensionalArrayException
	{
		DoubleArray array1 = new DoubleArray(_executor,3,3,4,5);
		DoubleArray array2 = new DoubleArray(_executor, 3,4,5);
		
		DoubleArray subArray = array1.at(-1,1,-1,-1);
		
		subArray.set(3d,2,2,2);
		array2.set(2d,2,2,2);
		array2.set(1d,1,2,3);
		
		DoubleArray result = subArray.add(array2);
		
		assertEquals(5d, result.get(2,2,2),1e-15);
		assertEquals(1d, result.get(1,2,3),1e-15);
		assertEquals(0d, result.get(2,2,1),1e-15);
		
		result = subArray.add(7d);
		
		assertEquals(10d, result.get(2,2,2),1e-15);
		assertEquals(7d, result.get(1,2,3),1e-15);
		
		result = subArray.add(0d);
		
		assertEquals(3d, result.get(2,2,2),1e-15);
		assertEquals(0d, result.get(1,2,3),1e-15);
	}
	

	@Test
	public void testFillDimensions() throws MultiDimensionalArrayException
	{
		double[] inputData = new double[]{1.2,1.3,1.4,1.5};

		DoubleArray inputArray = new DoubleArray(inputData,_executor);
		DoubleArray array = new DoubleArray(_executor, 4,3);
		
		array.fillDimensions(inputArray,0);
		
		for (int i = 0; i < inputData.length; i++)
		{
			assertEquals(inputData[i], array.get(i,i % 3), ALLOWED_ERROR);
		}
		
		DoubleArray threeD = new DoubleArray(_executor, 3,6,5,2);
		
		
		// Now we fill along two dimensions which have different sizes than source, and are
		// not consecutive in the target
		threeD.at(1,-1,-1,-1).fillDimensions(array, 0,2);
		
		assertEquals(1.4d,threeD.get(1,3,3,1), 1e-15);
		assertEquals(1.4d,threeD.get(1,3,2,1), 1e-15);
		assertEquals(1.4d,threeD.get(1,4,3,0), 1e-15);
		assertEquals(1.5d,threeD.get(1,4,0,1), 1e-15);
		assertEquals(1.2d,threeD.get(1,0,0,1), 1e-15);
	}
	
	@Test
	public void testAddSubArray() throws MultiDimensionalArrayException
	{
		double[] inputData = new double[]{1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9};

		DoubleArray inputArray = new DoubleArray(_executor, 2,2,2);
		inputArray.fillDimensions(new DoubleArray(inputData,_executor), 0,1,2);
		
		
		DoubleArray sum = inputArray.at(0,-1,-1).add(inputArray.at(1,-1,-1));
		
		assertEquals(2.8,sum.get(0,0),1e-15);
		assertEquals(3.0,sum.get(0,1),1e-15);
		assertEquals(3.2,sum.get(1,0),1e-15);
		assertEquals(3.4,sum.get(1,1),1e-15);
		
	}
	
//	
//	@Test
//	public void testMatrixMultiply() throws MultiDimensionalArrayException
//	{
//		DoubleArray A = new DoubleArray(3,2);
//		DoubleArray B = new DoubleArray(2,4);
//		
//		A.fillDimensions(new DoubleArray(new double[]{1d,2d,3d,4d,5d,6d}), 0,1);
//		B.fillDimensions(new DoubleArray(new double[]{7d,8d,9d,10d,11d,12d,13d,14d}), 0,1);
//		
//		DoubleArray result = A.matrixMultiply(B);
//		
//		int[] size = result.size();
//		
//		assertEquals(2, size.length);
//		assertEquals(3, size[0]);
//		assertEquals(4, size[1]);
//		
//		assertEquals(29d,result.get(0,0),1e-15);
//		assertEquals(65d,result.get(1,0),1e-15);
//		assertEquals(72d,result.get(1,1),1e-15);
//		assertEquals(123d,result.get(2,2),1e-15);
//	}
//	
//	@Test
//	public void testMatrixMultiplyTransposed() throws MultiDimensionalArrayException
//	{
//		DoubleArray A = new DoubleArray(3,2);
//		DoubleArray B = new DoubleArray(2,4);
//		
//		// Sets the whole matrix to minus inf
//		B.setMatching(0d, double.NEGATIVE_INFINITY);
//		A.fillAt(new DoubleArray(new double[]{1,1}),0);
//		DoubleArray nonTransposed = (DoubleArray) A.matrixMultiply(B, 0,1);
//		
//		assertTrue(double.isInfinite(nonTransposed.get(0,0)));
//		assertTrue(double.isNaN(nonTransposed.get(1,1)));
//		
//		DoubleArray transposed = (DoubleArray) A.matrixMultiply(B, new ITransformer<double>()
//		{
//			
//			@Override
//			public double transform(double value_)
//			{
//				return double.isNaN(value_) ? 0d : value_;
//			}
//		},0,1);
//		
//		assertTrue(double.isInfinite(transposed.get(0,0)));
//		assertEquals(0d,transposed.get(1,1), ALLOWED_ERROR);
//	}
//	
//	@Test
//	public void testTranspose() throws MultiDimensionalArrayException
//	{
//		DoubleArray A = new DoubleArray(3,2);
//		
//		A.fillDimensions(new DoubleArray(new double[]{1d,2d,3d,4d,5d,6d}), 0,1);
//		
//		A.transpose(0,1);
//		
//		int[] size = A.size();
//		
//		assertEquals(2, size.length);
//		assertEquals(2, size[0]);
//		assertEquals(3, size[1]);
//		
//		assertEquals(3d,A.get(0,1),1e-15);
//		assertEquals(6d,A.get(1,2),1e-15);
//		
//		assertTrue(A.isTransposed());
//		
//		A.set(17d,1,2);
//		
//		assertEquals(17d,A.get(1,2),1e-15);
//		
//		A.transpose(0, 1);
//		assertTrue(!A.isTransposed());
//		assertEquals(17d,A.get(2,1),1e-15);		
//	}
//	
//	@Test
//	public void testMax() throws MultiDimensionalArrayException
//	{
//		DoubleArray threeD = new DoubleArray(6,5,2);
//		
//		for(int i = 0; i < 60;i++)
//		{
//			threeD.set(-Math.pow(i/10-i%10,2)+i/10, i);
//		}
//		
//		Pair<DoubleArray, IMultiDimensionalArray<Integer>> maxRes = threeD.max(1);
//				
//		DoubleArray maxValues = maxRes.getLeft();
//		IMultiDimensionalArray<Integer> maxIndices = maxRes.getRight();
//		
//		int[] size = maxValues.size();
//		assertEquals(2, size.length);
//		assertEquals(2, maxIndices.size().length);
//	
//		// Check that the actual dimension sizes are correct
//		assertEquals(6,size[0]);
//		assertEquals(2,size[1]);
//		
//		assertEquals(0d,maxValues.get(0,0),ALLOWED_ERROR);
//		assertEquals(2d,maxValues.get(2,0),ALLOWED_ERROR);
//	
//		assertEquals(0,(int)maxIndices.get(0,0));
//		assertEquals(1,(int)maxIndices.get(2,0));
//	
//		maxRes = threeD.max(1,2);
//		
//		maxValues = maxRes.getLeft();
//		maxIndices = maxRes.getRight();
//		
//		size = maxValues.size();
//		assertEquals(1, size.length);
//		assertEquals(2, maxIndices.size().length);
//		
//		assertEquals(0d,maxValues.get(0),ALLOWED_ERROR);
//		assertEquals(2d,maxValues.get(2),ALLOWED_ERROR);
//	
//		assertEquals(0,(int)maxIndices.get(0,0));
//		assertEquals(0,(int)maxIndices.get(0,1));
//		
//		assertEquals(1,(int)maxIndices.get(2,0));
//		assertEquals(0,(int)maxIndices.get(2,1));
//	}
//	
//	@Test
//	public void testFillAt() throws MultiDimensionalArrayException
//	{
//		DoubleArray threeD = new DoubleArray(6,5,2);
//		
//		threeD.fillAt(new DoubleArray(new double[]{1.1,1.2,1.3,1.4,1.5}),3,-1,1);
//		
//		assertEquals(1.4, threeD.get(3,3,1),ALLOWED_ERROR);
//		assertEquals(1.2, threeD.get(3,1,1),ALLOWED_ERROR);
//		assertEquals(0d, threeD.get(2,3,1),ALLOWED_ERROR);
//		
//	}
//	
//	@Test
//	public void testSetMatching() throws MultiDimensionalArrayException
//	{
//		DoubleArray threeD = new DoubleArray(6,5,2);
//
//		// Fill some values
//		threeD.fillAt(new DoubleArray(new double[]{1.1,1.2,1.3,1.4,1.5}),3,-1,1);
//		
//		// Now set those matching 0 to -1
//		threeD.setMatching(0d, -1d);
//		
//		assertEquals(1.4, threeD.get(3,3,1),ALLOWED_ERROR);
//		assertEquals(1.2, threeD.get(3,1,1),ALLOWED_ERROR);
//		assertEquals(-1d, threeD.get(2,3,1),ALLOWED_ERROR);
//
//		// Checks that null blocks are matched with zero
//		DoubleArray oneD = new DoubleArray(3, 10);
//		oneD.set(1.2, 0);
//		oneD.set(1.3,9);
//		
//		oneD.setMatching(0d, 1d);
//		
//		assertEquals(1.2,oneD.get(0),ALLOWED_ERROR);
//		assertEquals(1.3,oneD.get(9),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(2),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(3),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(4),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(5),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(6),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(7),ALLOWED_ERROR);
//		assertEquals(1d,oneD.get(8),ALLOWED_ERROR);
//	}
//	
	@Test
	public void testSum() throws MultiDimensionalArrayException
	{
		DoubleArray threeD = new DoubleArray(_executor, 3,2,2);

		// Fill some values
		threeD.fillDimensions(new DoubleArray(new double[]{1d,2d,3d,4d,5d,6d,7d,8d,9d,10d,11d,12d},_executor),0,1,2);

		double sumFull = threeD.across(0,1,2).sum().get(0);
		assertEquals(12d*13d/2d,sumFull, ALLOWED_ERROR);
		
		DoubleArray subSum = threeD.at(1,-1,-1).across(0,1).sum();
		
		assertEquals(1, subSum.size().length);
		assertEquals(1, subSum.size()[0]);
		assertEquals(26d, subSum.get(0),ALLOWED_ERROR);
	}
}
