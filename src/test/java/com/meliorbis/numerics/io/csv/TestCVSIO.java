package com.meliorbis.numerics.io.csv;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.SettableIndexedIterator;
import com.meliorbis.numerics.generic.impl.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleSettableIndexedIterator;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;
import com.meliorbis.utils.Utils;

import org.junit.Assert;

/**
 * Created by toby on 07/03/2014.
 */
public class TestCVSIO extends NumericsTestBase
{
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @SuppressWarnings("unchecked")
	@Test
    public void testReadWrite() throws IOException
    {
        final DoubleArray<?> array = _numerics.getArrayFactory().newArray(new int[]{2, 4, 6});
        final DoubleArray<?> array2 = _numerics.getArrayFactory().newArray(new int[]{1, 1000});
        final IntegerArray integerArray = _numerics.newIntArray(9, 3, 7, 5);

        array.fill(Utils.sequence(1d, (double) array.numberOfElements(), array.numberOfElements()));

        final Random random = new Random(1027);

        final DoubleSettableIndexedIterator a2Iter = array2.iterator();

        while(a2Iter.hasNext())
        {
            a2Iter.nextDouble();

            a2Iter.set(random.nextDouble());
        }

        final SettableIndexedIterator<Integer> intIter = integerArray.iterator();

        while(intIter.hasNext())
        {
            intIter.next();
            intIter.set(random.nextInt());
        }


        final File tempDir = folder.newFolder("test");

        final CSVWriter writer = new CSVWriter(tempDir);

        final HashMap<String, MultiDimensionalArray<? extends Number, ?>> arrays =
                new HashMap<String, MultiDimensionalArray<? extends Number, ?>>();

        arrays.put("a1",array);
        arrays.put("a2", array2);
        arrays.put("int",integerArray);


        writer.writeArrays(arrays);

        writer.close();

        final CSVReader reader = new CSVReader(tempDir);

        DoubleArray<?> readA1 = (DoubleArray<?>) reader.<Double>getArray("a1");

        ArrayAssert.assertEquals(array, readA1, 1e-15);

        DoubleArray<?> readA2 = (DoubleArray<?>) reader.<Double>getArray("a2");

        ArrayAssert.assertEquals(array2,readA2, 1e-15);

        MultiDimensionalArray<Integer, ?> readInt = reader.getArray("int");

        ArrayAssert.assertEquals(integerArray,readInt);


        final Map<String,MultiDimensionalArray<? extends Number, ?>> readArrays = reader.getArrays();

        Assert.assertEquals(3, readArrays.size());

        readA1 = (DoubleArray<?>) readArrays.get("a1");

        ArrayAssert.assertEquals(array,readA1, 1e-15);

        readA2 = (DoubleArray<?>) readArrays.get("a2");

        ArrayAssert.assertEquals(array2,readA2, 1e-15);

        readInt = (MultiDimensionalArray<Integer, ?>) readArrays.get("int");

        ArrayAssert.assertEquals(integerArray,readInt);
    }

    @Test
    public void testStructure() throws IOException
    {
        final DoubleArray<?> array = _numerics.getArrayFactory().newArray(new int[]{2, 4, 6});
        final DoubleArray<?> array2 = _numerics.getArrayFactory().newArray(new int[]{1, 1000});
        final IntegerArray integerArray = _numerics.newIntArray(9, 3, 7, 5);

        array.fill(Utils.sequence(1d, (double)array.numberOfElements(), array.numberOfElements()));

        final Random random = new Random(1027);

        final DoubleSettableIndexedIterator a2Iter = array2.iterator();

        while(a2Iter.hasNext())
        {
            a2Iter.nextDouble();

            a2Iter.set(random.nextDouble());
        }

        final SettableIndexedIterator<Integer> intIter = integerArray.iterator();

        while(intIter.hasNext())
        {
            intIter.next();
            intIter.set(random.nextInt());
        }

        // Check that the full range is available
        integerArray.set(Integer.MAX_VALUE, 0);
        integerArray.set(Integer.MIN_VALUE, 1);

        final File tempFile = folder.newFolder("test");

        final CSVWriter writer = new CSVWriter(tempFile);

        final HashMap<String, MultiDimensionalArray<? extends Number, ?>> arrays =
                new HashMap<String, MultiDimensionalArray<? extends Number, ?>>();

        arrays.put("a1",array);
        arrays.put("a2", array2);
        arrays.put("int",integerArray);


        writer.writeStructure("myStruct", arrays);

        writer.close();

        final CSVReader reader = new CSVReader(tempFile);

        final Map<String,MultiDimensionalArray<? extends Number, ?>> readArrays = reader.getStruct("myStruct");

        Assert.assertEquals(3, readArrays.size());

        DoubleArray<?> readA1 = (DoubleArray<?>) readArrays.get("a1");

        ArrayAssert.assertEquals(array,readA1, 1e-15);

        DoubleArray<?> readA2 = (DoubleArray<?>) readArrays.get("a2");

        ArrayAssert.assertEquals(array2,readA2, 1e-15);

        @SuppressWarnings("unchecked")
		MultiDimensionalArray<Integer, ?> readInt = (MultiDimensionalArray<Integer, ?>) readArrays.get("int");

        ArrayAssert.assertEquals(integerArray,readInt);


    }
}
