package com.meliorbis.numerics.markov;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.meliorbis.numerics.NumericsException;
import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleUnaryOp;
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;

public class RouwenhorstTest extends NumericsTestBase {
	protected static final double ALLOWED_ERROR = 1e-15;
	private DiscreteStochasticProcessFactory _factory;

	@Before
	public void setUp() {
		_factory = new DiscreteStochasticProcessFactory();		
	}

	@Test
	public void testCreateSymmetricTransition()
			throws MultiDimensionalArrayException {
		DoubleArray<?> symTrans = _factory
				.createSymmetricTransition(2, 0);

		DoubleArray<?> expected = _numerics.getArrayFactory()
				.newArray(2, 2);

		expected.fill(new double[]{.5, .5, .5, .5});

		ArrayAssert.assertEquals(expected, symTrans, ALLOWED_ERROR);

		symTrans = _factory.createSymmetricTransition(2, .4);

		expected.fill(new double[]{.7, .3, .3, .7});

		ArrayAssert.assertEquals(expected, symTrans, ALLOWED_ERROR);
		
		symTrans = _factory.createSymmetricTransition(5, .97);
		
		double minRowSum = symTrans.across(1).sum().across(0).min().get(0);
		double maxRowSum = symTrans.across(1).sum().across(0).max().get(0);
		
		assertEquals(1d, minRowSum, ALLOWED_ERROR);
		assertEquals(1d, maxRowSum, ALLOWED_ERROR);
	}

	@Test
	public void testCreateTransition() throws MultiDimensionalArrayException {

		DoubleArray<?> transitionMatrix = _factory
				.createTransitionMatrix(2, .5, .5);

		transitionMatrix.map(new DoubleUnaryOp<RuntimeException>()
        {

            @Override
            public double perform(double input_)
            {
                assertEquals(0.5, input_, ALLOWED_ERROR);
                return 0d;
            }
        });

		transitionMatrix = _factory.createTransitionMatrix(2, .8, .8);

		DoubleArray<?> expected = _numerics.getArrayFactory()
				.newArray(2, 2);

		expected.fill(new double[]{.8, .2, .2, .8});

		ArrayAssert.assertEquals(expected, transitionMatrix, ALLOWED_ERROR);

		transitionMatrix = _factory.createTransitionMatrix(2, .2, .8);

		expected.fill(new double[]{.2, .8, .2, .8});

		ArrayAssert.assertEquals(expected, transitionMatrix, ALLOWED_ERROR);

		transitionMatrix = _factory.createTransitionMatrix(3, .2, .8);

		expected = _numerics.getArrayFactory().newArray(3, 3);

		expected.fill(new double[] { .04, .32, .64, .04, .32, .64, .04, .32,
				.64 });

		ArrayAssert.assertEquals(expected, transitionMatrix, ALLOWED_ERROR);
	}

	@Test
	public void testStatesAndTransition() throws MultiDimensionalArrayException, NumericsException {
		DiscreteMarkovProcess markovProcess = _factory
				.rouwenhorst(21, .979, .0072);

		DoubleArray<?> states = markovProcess.getLevels();
		DoubleArray<?> transition = markovProcess.getTransitionProbabilities();

		DoubleArray<?> kopeckyTransition = _numerics.readCSV(ClassLoader.getSystemResourceAsStream("rouwenhorstTransKopecky.csv"));
		DoubleArray<?> kopeckyGrid = _numerics.readCSV(ClassLoader.getSystemResourceAsStream("rouwenhorstGridKopecky.csv"));
		
		assertEquals(0d,states.add(kopeckyGrid.transpose(0,1).multiply(-1d)).max(),ALLOWED_ERROR);
		assertEquals(0d,DoubleArrayFunctions.maximumRelativeDifference(transition, kopeckyTransition),ALLOWED_ERROR);		
	}
}
