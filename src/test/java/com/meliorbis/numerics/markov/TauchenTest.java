package com.meliorbis.numerics.markov;

import org.junit.Before;
import org.junit.Test;

import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;
import com.meliorbis.utils.Utils;

import org.junit.Assert;

/**
 * Tests creation of discrete markov processes using the tauchen method
 */
public class TauchenTest extends NumericsTestBase
{
    private static final double ALLOWED_ERROR = 1e-15;
    private DiscreteStochasticProcessFactory _factory;

    @Before
    public void setUp()
    {
        _factory = new DiscreteStochasticProcessFactory();
    }
    @Test
    public void testTauchen()
    {
        final DiscreteMarkovProcess tauchen = _factory.tauchen(5, 0.9, 0.01, 3);

        Assert.assertEquals(5, tauchen.getLevels().numberOfElements());
        Assert.assertEquals(25, tauchen.getTransitionProbabilities().numberOfElements());
        Assert.assertEquals(2, tauchen.getTransitionProbabilities().numberOfDimensions());

        // Check the width of the levels
        Assert.assertEquals(-3d*0.01, tauchen.getLevels().first(), ALLOWED_ERROR);
        Assert.assertEquals(3d*0.01, tauchen.getLevels().last(), ALLOWED_ERROR);

        // The middle level should be 0
        Assert.assertEquals(0d, tauchen.getLevels().get(2), ALLOWED_ERROR);

        // The sum across targets should all be 1
        final DoubleArray<?> totalProbs = tauchen.getTransitionProbabilities().across(1).sum();

        ArrayAssert.assertEquals(_numerics.getArrayFactory().newArray(Utils.repeatArray(1d,5)),totalProbs,
                ALLOWED_ERROR);
    }
}
