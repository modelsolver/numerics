package com.meliorbis.numerics.performance;

import com.meliorbis.numerics.test.NumericsTestBase;

public class PerfTest extends NumericsTestBase
{

//	private static final int ARRAY_SIZE = 500000;
//	@Before
//	public void setUp() 
//	{
//		super.setUp();
//	}
//
//	@Test
//	public void testMap()
//	{
//	
//		IDoubleArray<?> array1 = createArray(ARRAY_SIZE);
//		IDoubleArray<?> array2 = createArray(ARRAY_SIZE);
//		
//		
//		
//		for(int i = 0; i< ARRAY_SIZE; i++)
//		{
//			array1.set(Math.random(), i);
//			array2.set(Math.random(), i);
//		}
//		
//		for(int i = 0; i < 1; i++)
//		{
//			array1.multiply(array2);
//		}
//		
//		Timer timer = new Timer();
//		
//		Stoppable stoppable = timer.start("map");
//
//		for(int i = 0; i < 5; i++){
//			array1.multiply(array2);
//		}
//		
//		stoppable.stop();
//		assert(true);
//	}
//	
//	@Test
//	public void testMapAcross()
//	{
//	
//		IDoubleArray<?> array1 = createArray(ARRAY_SIZE,2);
//		IDoubleArray<?> array2 = createArray(2);
//		array2.set(2, 0);
//		array2.set(3, 1);
//		
//		
//		for(int i = 0; i< array1.numberOfElements(); i++)
//		{
//			array1.set(Math.random(), i);
//		}
//		
//		IDoubleArray<?> product = null;
//		
//		for(int i = 0; i < 5; i++)
//		{
//			
//			product = array1.across(1).multiply(array2);
//		}
//		
//		for(int i = 0; i < array1.size()[0];i++)
//		{
//			
//			for(int j = 0; j < array1.size()[1];j++)
//			{
//				assertEquals(array1.get(i,j)*array2.get(j),product.get(i,j), 1e-15);
//			}
//			
//		}
//		
//		Timer timer = new Timer();
//		
//		Stoppable stoppable = timer.start("mapAcross");
//
//		for(int i = 0; i < 2; i++){
//			array1.across(1).multiply(array2);
//		}
//		
//		stoppable.stop();
//		assert(true);
//
//	}
//	
//	@Test
//	public void testMapAcrossCount()
//	{
//	
//		IDoubleArray<?> array1 = createArray(ARRAY_SIZE,2);
//		IDoubleArray<?> array2 = createArray(2);
//		array2.set(2, 0);
//		array2.set(3, 1);
//		
//		
//		for(int i = 0; i< array1.numberOfElements(); i++)
//		{
//			array1.set(Math.random(), i);
//		}
//		
//		IDoubleArray<?> product = null;
//		
//		for(int i = 0; i < 5; i++)
//		{
//			
//			product = array1.across(1).multiply(array2);
//		}
//		
//		for(int i = 0; i < array1.size()[0];i++)
//		{
//			
//			for(int j = 0; j < array1.size()[1];j++)
//			{
//				assertEquals(array1.get(i,j)*array2.get(j),product.get(i,j), 1e-15);
//			}
//			
//		}
//		
//		Timer timer = new Timer();
//		
//		Stoppable stoppable = timer.start("mapAcross");
//
//		final AtomicInteger counter = new AtomicInteger(0);
//		
//		for(int i = 0; i < 2; i++){
//			array1.across(1).with(array2).map(new DoubleBinaryOp<RuntimeException>()
//			{
//
//				@Override
//				public double perform(double... inputs_) throws RuntimeException
//				{
//					return perform(inputs_[0], inputs_[1]);
//				}
//
//				@Override
//				public Double perform(Double... inputs_) throws RuntimeException
//				{
//					throw new UnsupportedOperationException();
//				}
//
//				@Override
//				public Double perform(Double left_, Double right_) throws RuntimeException
//				{
//					throw new UnsupportedOperationException();
//				}
//
//				@Override
//				public double perform(double left_, double right_) throws RuntimeException
//				{
//					counter.incrementAndGet();
//					double r = 0;
//					
//					for(int j = 0; j < 10000; j++)
//					{
//						r += left_ * right_*j;
//					}
//					
//					return r;
//				}
//			});
//		}
//		
//		stoppable.stop();
//		assert(true);
//		System.out.println(counter);
//	}
//	
//	@Test
//	public void testReduceAcross()
//	{
//	
//		IDoubleArray<?> array1 = createArray(ARRAY_SIZE*50,5);
//		//IDoubleArray<?> array2 = createArray(ARRAY_SIZE);
//		
//		
//		for(int i = 0; i< array1.numberOfElements(); i++)
//		{
//			array1.set(Math.random(), i);
//		}
//		
//		IDoubleArray<?> sumArray = null;
//		
//		for(int i = 0; i < 1; i++)
//		{
//			sumArray = array1.across(1).sum();
//		}
//		
//		for(int i = 0; i < array1.size()[0];i++)
//		{
//			double sum = 0;
//			
//			for(int j = 0; j < array1.size()[1];j++)
//			{
//				sum += array1.get(i,j);
//			}
//			
//			assertEquals(sum,sumArray.get(i), 1e-15);
//		}
//		
//		Timer timer = new Timer();
//		
//		Stoppable stoppable = timer.start("reduceAcross");
//
//		for(int i = 0; i < 1; i++){
//			array1.across(1).sum();
//		}
//		
//		stoppable.stop();
//		assert(true);
//	}
//	
//	@Test
//	public void testRawDouble() throws InterruptedException
//	{
//		double[] array1 = new double[ARRAY_SIZE];
//		double[] array2 = new double[ARRAY_SIZE];
//		double[] array3 = new double[ARRAY_SIZE*2];
//		
//		for (int i = 0; i < array2.length; i++)
//		{
//			array1[i] = Math.random();
//			array2[i] = Math.random();
//		}
//		
//		for(int j = 1; j < 5; j++) {
//			for (int i = 0; i < array2.length; i++)
//			{
//				array3[i] = array1[i]*array2[i];
//			}
//		}
//		
//		Timer timer = new Timer();
//		
//		Stoppable stoppable = timer.start("raw");
//		for(int j = 0; j < 10000; j++) {
//			final int offset = (j%2)*array1.length;
//			for (int i = 0; i < array2.length; i++)
//			{
//				array3[i+ offset] = mult(array1[i], array2[i]);
//			}
//		}
//		stoppable.stop();
//		
//		ExecutorService threadPool = Executors.newFixedThreadPool(2);
//		
//		List<Callable<Object>> callables = new ArrayList<Callable<Object>>();
//		for(int t = 0; t < 2; t++)
//		{
//			final int offset = t*array1.length;
//			
//			callables.add(new Callable()
//			{
//				@Override
//				public Object call() throws Exception
//				{
//					for(int j = 1; j < 5000; j++) {
//						for (int i = 0; i < array2.length; i++)
//						{
//							array3[i+offset] = mult(array1[i], array2[i]);
//						}
//					}
//					return null;
//				}
//			});
//		}
//		stoppable = timer.start("rawThreaded");
//		threadPool.invokeAll(callables);
//		stoppable.stop();
//	}
//	
//	public double mult(double a_, double b_)
//	{
//		return a_ * b_;
//	}

}
