package com.meliorbis.utils;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import com.meliorbis.utils.Timer.Stoppable;

public class TimerTest
{
	@Test
	public void testTimesMap()
	{
		Timer.clearMap();
		
		Timer timer = new Timer(true);
		
		Stoppable foo = timer.start("foo");
		foo.stop();
		
		Stoppable bar = timer.start("bar");
		bar.stop();

		foo = timer.start("foo");
		foo.stop();
		
		Map<String, long[]> timesMap = Timer.getTimesMap();
		
		assertEquals(2,timesMap.size());
		
		long[] barTimes = timesMap.get("bar");
		
		assertNotNull(barTimes);
		assertEquals(2,barTimes.length);
		assertEquals(1,barTimes[1]);
		
		long[] fooTimes = timesMap.get("foo");
		
		assertNotNull(fooTimes);
		assertEquals(2,fooTimes.length);
		assertEquals(2,fooTimes[1]);
	}

}
