package com.meliorbis.utils;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.NumericsTestBase;

public class UtilsTestCase extends NumericsTestBase
{
    @Test
    public void testMax()
    {

        final Pair<Integer, Integer> max = Utils.max(new int[]{1, 4, 7, 5, 3, 5});

        assertEquals(7, (int) max.getLeft());
        assertEquals(2, (int) max.getRight());
    }

    @Test
    public void testSequence()
    {
        final int[] sequence = Utils.sequence(3, 7);
        assertArrayEquals(new int[]{3,4,5,6}, sequence);

        final int[] reverse = Utils.sequence(7,3);
        assertArrayEquals(new int[]{7,6,5,4}, reverse);

    }
    @Test
    public void testRepeatArray()
    {

        final double[] doubles = Utils.repeatArray(1.2, 9);

        assertEquals(9, doubles.length);
        
        for (int i = 0; i < doubles.length; i++)
		{
			assertEquals(1.2,doubles[i], 1e-15);
		}
    }

    @Test
    public void testToIntArray()
    {

        final int[] ints = Utils.toIntArray(Arrays.asList(Integer.valueOf(7), Integer.valueOf(104)));

        assertEquals(2, ints.length);
        assertArrayEquals(new int[]{7, 104}, ints);
    }

    @Test
    public void testDrawRandom()
    {
        final DoubleArray<?> dist = _numerics.getArrayFactory().newArray(new double[]{.2, .4, .3, .1});

        final double[] values = new double[]{.3,.6,.95,.1};

        Random random = new Random(){
            int index = 0;
            @Override
            public double nextDouble()
            {
                return values[index++];
            }
        };

        int[] index = Utils.drawRandomState(dist, random);

        assertEquals(1,index[0]);

        index = Utils.drawRandomState(dist, random);

        assertEquals(1,index[0]);

        index = Utils.drawRandomState(dist, random);

        assertEquals(3,index[0]);

        index = Utils.drawRandomState(dist, random);

        assertEquals(0,index[0]);
    }

    @Test
    public void testCombine()
    {
        int[] first = new int[]{1, 4, 9};
        int[] second = new int[]{16};
        int[] third = new int[]{25, 36};

        int[] combined = Utils.combine(first, second, third);

        assertArrayEquals(new int[]{1, 4, 9, 16, 25, 36}, combined);
    }


    @Test
    public void testExtract()
    {
        int[] values = new int[]{1, 4, 9, 16, 25, 36};
        int[] indices = new int[]{2, 4, 5};

        int[] extracted = Utils.extract(values, indices);

        assertArrayEquals(new int[]{9, 25, 36}, extracted);
    }

    @Test
    public void testAddDimensionsToList()
    {
        ArrayList<Integer> ints = new ArrayList<Integer>();

        ints.add(2);
        ints.add(5);

        final DoubleArray<?> array1 = _numerics.getArrayFactory().newArray(4,3,7,8);
        final DoubleArray<?> array2 = _numerics.getArrayFactory().newArray(2);

        Utils.addLengthsToList(ints, Arrays.asList(array1, array2));
        assertEquals(4, ints.size());
        assertArrayEquals(new Integer[]{2, 5, 4, 2}, ints.toArray());
    }
}
